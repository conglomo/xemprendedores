//libreria con funciones para repoblar automaticamente inputs.
var countDinamico1 = 0;
var countDinamico2 = 0;
var countDinamico3 = 0;
var countDinamico4 = 0;
var subpro_incre = 0;
var local = "";
var seccion ="";
var log_seccion = false;
var log_sucursal = false;
var log_cargo = false;
var value_radio;
var select_complejidad ="";
var instrumentoplus = "";
function getCheckedRadioValue() {
   var rads = document.getElementsByName("frustrado"),
       i;
   for (i=0; i < rads.length; i++)
      if (rads[i].checked)
          return rads[i].value;
   return null; // or undefined, or your preferred default for none checked
}

function getcasos_sub(click_id,values){
	var sub_p = click_id;
	var casos_sub =  "#sel_casos"+countDinamico4;
        var dato = values;
        console.log(dato);
        //alert("estamos en function");                
        if((dato > 0) || (dato != "")){
            
        var dato = values;     
            $.ajax({
                type:"post",
                dataType:"html",
                url: base_url+"/administrador/ajax_idCasos",
                data: "casos="+dato,
                success: function(msg){
                    // alert(msg);
                    $(casos_sub).empty().removeAttr("disabled").append(msg);                            
                    $(casos_sub).show();       
                }
            });
        }else{
            alert("Debe seleccionar un Caso");                                                  
            $(casos_sub).attr("disabled","disabled");
            $(casos_sub).hide();
        }
    };


$(document).ready(function(){
    
//    countDinamico1 = $(".contacto_tec").attr('id').replace(/tecnica/, '');
//    countDinamico1 = $(".contacto_tec").attr('id').replace(/tecnica/, '');
//    console.log(countDinamico1);
    base_img = ((location.href).substr(0, (location.href).lastIndexOf('index.php')));  
    site_url = ((location.href).substr(0, (location.href).lastIndexOf('index.php')))+"index.php/";
    value_radio= getCheckedRadioValue();
     $("#select_complejidad").change(function () {
       select_complejidad = $(this).val(); 
    });
    
    if(value_radio=="Si" && countDinamico1 == 0 && countDinamico2 == 0 && select_complejidad!=''){
		$('#guardar_problematica').attr('disabled', false);
		$('#guardar_problematica').css('color', '#fff');   
    }
    
    $("#radio_si").click(function() {
    value_radio = $(this).val();
    if(select_complejidad!=''){
        // console.log(select_complejidad);
		$('#guardar_problematica').attr('disabled', false);
		$('#guardar_problematica').css('color', '#fff');
    }else{
		$('#guardar_problematica').attr('disabled', true);
		$('#guardar_problematica').css('color', '#ccc');   
    }
    });
    $("#radio_no").click(function() {
    value_radio = $(this).val();
     if(countDinamico1 > 0 && countDinamico2 > 0 && select_complejidad!='') {
    $('#guardar_problematica').attr('disabled', false);
    $('#guardar_problematica').css('color', '#fff');
    }else{
    $('#guardar_problematica').attr('disabled', true);
    $('#guardar_problematica').css('color', '#ccc');
    }
    });


  /*####################Estado Salud con Licencia Prolongada######################*/
  
  if ( $('#complejidad').val() != 'prolongado' && $('#casotipo').val() != 'Salud' ) {
		$('#iniciolbl').hide();
		$('#iniciofld').hide();
		$('#estadolbl').hide();
		$('#estadofld').hide();	
	}	
    
    
    if(countDinamico1 == 0 || countDinamico2 == 0 && value_radio=="No") {
    	$('#guardar_problematica').attr('disabled', true);
    	$('#guardar_problematica').css('color', '#ccc');
    }     
    
//---------------Start  Agregar y quitar tecnica   -------------------------------------//
    
    $('#addtecnica').click(function(){
        //event.preventDefault();
        countDinamico1++;
        if(countDinamico1 > 0 && countDinamico2 > 0 && value_radio=="No") {
        	$('#guardar_problematica').attr('disabled', false);
        	$('#guardar_problematica').css('color', '#fff');
        }else if(value_radio=="Si"){
				$('#guardar_problematica').attr('disabled', false);
				$('#guardar_problematica').css('color', '#fff');   
		}else{
			$('#guardar_problematica').attr('disabled', true);
			$('#guardar_problematica').css('color', '#ccc');
        }
        
        $("#ajaxs").append("<div id='base"+countDinamico1+"'></div>");
        $("#base"+countDinamico1).append("<img src='"+base_img+"img/loading/6.gif' style='display:none' alt='Cargando' id='imgtecnica"+countDinamico1+"'>");
        $("#base"+countDinamico1).append("<select name='tecnica"+countDinamico1+"' id='tecnica"+countDinamico1+"'></select>");       
            $.ajax({
                type:"post",
                dataType:"html",                
                url: base_url+"/fichas/ajax_tecnica",
                beforeSend: function(){
                    $("#imgtecnica"+countDinamico1).show();
                    $("#tecnica"+countDinamico1).hide();
                },
                success: function(msgd){
                    $("#tecnica"+countDinamico1).show();
                    $("#tecnica"+countDinamico1).empty().removeAttr("disabled").append(msgd);
                    $("#imgtecnica"+countDinamico1).hide();
                }
            });
    });
    
    $('#deltecnica').click(function(){
        if(countDinamico1>0){
            $("#base"+countDinamico1).remove();
            countDinamico1--;                                
        }else{
            alert("No Existen mas Selectores");
        }
        
        if(countDinamico1 == 0 || countDinamico2 == 0 && value_radio=="No") {
        	$('#guardar_problematica').attr('disabled', true);
        	$('#guardar_problematica').css('color', '#ccc');
        }else if(value_radio=="Si"){
            $('#guardar_problematica').attr('disabled', false);
        	$('#guardar_problematica').css('color', '#fff');
        }
        
    });
      
    //---------------End tecnica  -------------------------------------//
    //
    //
    //
    //---------------Start  Agregar y quitar Intervencion   -------------------------------------//
    
   $('#addintervencion').click(function(){        
        countDinamico2++;
        if(countDinamico1 > 0 && countDinamico2 > 0 && value_radio=="No") {
        	$('#guardar_problematica').attr('disabled', false);
        	$('#guardar_problematica').css('color', '#fff');
        }else if(value_radio=="Si"){
			$('#guardar_problematica').attr('disabled', false);
        	$('#guardar_problematica').css('color', '#fff');
		}else{
            $('#guardar_problematica').attr('disabled', true);
        	$('#guardar_problematica').css('color', '#ccc');
        }
        
        $("#ajaxs2").append("<div id='base2"+countDinamico2+"'></div>");
        $("#base2"+countDinamico2).append("<img src='"+base_img+"img/loading/6.gif' style='display:none' alt='Cargando' id='imgintervencion"+countDinamico2+"'>");
        //$("#base2"+countDinamico2).append("<select name='intervencion"+countDinamico2+"' id='intervencion"+countDinamico2+"'></select>");
        $("#base2"+countDinamico2).append("<div id='div_intervencion"+countDinamico2+"'><select name='intervencion"+countDinamico2+"' id='intervencion"+countDinamico2+"' onChange='crea_div2(this.id)' ></select></div>");              
            $.ajax({
                type:"post",
                dataType:"html",                
                url: base_url+"/fichas/ajax_intervencion",
                beforeSend: function(){
                    $("#imgintervencion"+countDinamico2).show();
                    $("#intervencion"+countDinamico2).hide();
                },
                success: function(msgd){
                    $("#intervencion"+countDinamico2).show();
                    $("#intervencion"+countDinamico2).empty().removeAttr("disabled").append(msgd);
                    $("#imgintervencion"+countDinamico2).hide();
                }
            });
    });
    
    
    
    
    $('#delintervencion').click(function(){
        if(countDinamico2>0){
            $("#base2"+countDinamico2).remove();
            countDinamico2--;
        }else{
            alert("No Existen mas Selectores");
        }
        if(countDinamico1 == 0 || countDinamico2 == 0 && value_radio=="No") {
        	$('#guardar_problematica').attr('disabled', true);
        	$('#guardar_problematica').css('color', '#ccc');
        }else if(value_radio=="Si"){
            $('#guardar_problematica').attr('disabled', false);
            $('#guardar_problematica').css('color', '#fff');
        }
    });         
          
    //---------------End  Intervencion   -------------------------------------//
    //
  
    //
    //---------------Start  Agregar y quitar Instrumento   -------------------------------------//
    
       $('#addinstrumento').click(function(){  
        countDinamico3++;
        //alert(base_img);
        $("#ajaxs3").append("<div id='base3"+countDinamico3+"'></div>");
        $("#base3"+countDinamico3).append("<img src='"+base_img+"img/loading/6.gif' style='display:none' alt='Cargando' id='imginstrumento"+countDinamico3+"'>");
        $("#base3"+countDinamico3).append("<div id='div_instrumento"+countDinamico3+"'><select name='instrumento"+countDinamico3+"' id='instrumento"+countDinamico3+"' onChange='crea_div(this.id)' ></select></div>");       
            $.ajax({
                type:"post",
                dataType:"html",                
                url: base_url+"/fichas/ajax_instrumento",
                beforeSend: function(){
                    //alert("antes");
                    $("#imginstrumento"+countDinamico3).show();
                    $("#instrumento"+countDinamico3).hide();
                },
                success: function(msgd){
                	$("#div_instrumento"+countDinamico3).show();
                    $("#instrumento"+countDinamico3).show();
                    $("#instrumento"+countDinamico3).empty().removeAttr("disabled").append(msgd);
                    $("#imginstrumento"+countDinamico3).hide();
                }
            });
    });
    $('#addsubpro').click(function(){  
        countDinamico4++;
        
        //alert(base_img);
        $("#ajaxs4").append("<div id='base4"+countDinamico4+"'></div>");
        $("#base4"+countDinamico4).append("<img src='"+base_img+"img/loading/6.gif' style='display:none' alt='Cargando' id='imgsubpro"+countDinamico4+"'>");
        $("#base4"+countDinamico4).append("<div id='div_subpro"+countDinamico4+"'><select name='subpro"+countDinamico4+"' id='subpro"+countDinamico4+"' onChange='getcasos_sub(this.id,this.value)' ></select></div>");
        $("#base4"+countDinamico4).append("<div id='div_sel_casos"+countDinamico4+"'><select name='sel_casos"+countDinamico4+"' id='sel_casos"+countDinamico4+"' ></select></div>");
        $("#base4"+countDinamico4).append("<div id='div_detalle_casos"+countDinamico4+"'><textarea name='detalle_casos"+countDinamico4+"' id='detalle_casos"+countDinamico4+"' class='st-forminput' style='width:400px'  rows='3' cols='47' maxlength='500'></textarea></div>");    
            $.ajax({
                type:"post",
                dataType:"html",                
                url: base_url+"/fichas/ajax_subpro",
                beforeSend: function(){
                    //alert("antes");
                    $("#imgsubpro"+countDinamico4).show();
                    $("#subpro"+countDinamico4).hide();
                    $("#sel_casos"+countDinamico4).hide();
                },
                success: function(msgd){
                	$("#div_subpro"+countDinamico4).show();
                    $("#subpro"+countDinamico4).show();
                    $("#subpro"+countDinamico4).empty().removeAttr("disabled").append(msgd);
                    $("#sel_casos"+countDinamico4).show();
                    $("#sel_casos"+countDinamico4).empty().removeAttr("disabled");
                    $("#imgsubpro"+countDinamico4).hide();
                    
                }
            });
    });
    
    $('#delsubpro').click(function(){
        if(countDinamico4>0){
            $("#base4"+countDinamico4).remove();
            countDinamico4--;                                
        }else{
            alert("No Existen mas Selectores");
        }
    });
    $('#delinstrumento').click(function(){
        if(countDinamico3>0){
            $("#base3"+countDinamico3).remove();
            countDinamico3--;                                
        }else{
            alert("No Existen mas Selectores");
        }
    });          
    //---------------End Instrumento   -------------------------------------//
    
 $(".eliminar_tecnica").click(function(){
        //sacar padre
        
        var id=$(this).attr('id');
        console.log(id);
        var padre = $("#"+id).parent().attr('id').replace("elementostec","");
        console.log(padre);
        var tecnica = $("#tec"+padre).html();
        console.log(tecnica);
        //var prob_id = $("#prob_id"+)
            $.ajax({
                        type:"post",
                        dataType:"html",                
                        url: base_url+"/fichas/ajax_eliminarTecnica",
                        data: "tecnica="+tecnica,
                        beforeSend: function(){
                        if (!confirm("Está; a punto de Eliminar una Técnica inmediatamente al aceptar ; ¿Desea continuar?")) {
					            return false;
					        }
                        },  
                        success: function(msgd){
                            $("#elementostec"+padre).remove();
                            
                        }
                });
    });

    $(".eliminar_intervencion").click(function(){
        //sacar padre
        
        var id=$(this).attr('id');
        var padre = $("#"+id).parent().attr('id').replace("elementosint","");
        var intervencion = $("#int"+padre).html();
        //alert(tecnica);
        //var prob_id = $("#prob_id"+)
            $.ajax({
                        type:"post",
                        dataType:"html",                
                        url: base_url+"/fichas/ajax_eliminarIntervencion",
                        data: "intervencion="+intervencion,
                        beforeSend: function(){
                            if (!confirm("Está a punto de Eliminar una Intervención inmediatamente al aceptar ; ¿Desea continuar?")) {
					            return false;
					        }
                        },
                        success: function(msgd){
                            $("#elementosint"+padre).remove();
                        }
                });
    });

    $(".eliminar_instrumento").click(function(){
        //sacar padre
        
        var id=$(this).attr('id');
        var padre = $("#"+id).parent().attr('id').replace("elementosins","");
        var instrumento = $("#ins"+padre).html();
        //alert(tecnica);
        //var prob_id = $("#prob_id"+)
            $.ajax({
                        type:"post",
                        dataType:"html",                
                        url: base_url+"/fichas/ajax_eliminarInstrumento",
                        data: "instrumento="+instrumento,
                        beforeSend: function(){
                            if (!confirm("Está a punto de Eliminar un Instrumento inmediatamente al aceptar ; ¿Desea continuar?")) {
					            return false;
					        }
                        },
                        success: function(msgd){
                            $("#elementosins"+padre).remove();
                        }
                });
    });
    //------------- Cargar Secciones --------------------------------------------//
          
    //----------------------VALIDAR SI EXISTEN DATOS PREVIOS DE BASE DATOS-------------//
    
    
        if($("#local").val() != '' && $("#local").length){
            var local = $("#local").val();
            var sucursal = $("#sucursal").val();
            //alert(local);
            //alert(sucursal);
            $.ajax({
                type:"post",
                dataType:"html",
                url: base_url+"/fichas/ajax_sucursal",
                data: "local="+local+"&sucursal="+sucursal,
                success: function(msg){                   
                    $("#sel_sucursal").empty().removeAttr("disabled").append(msg);
                     $("#sel_seccion").empty().attr("disabled", "disabled");                            
                     $("#sel_cargo").empty().attr("disabled", "disabled"); 
                    log_seccion = true;
                    //alert(log_seccion);                   
                        //if sucurlal

                        if($("#sucursal").val() != '' && $("#local").length && log_seccion == true ){                     
                            var local2 = $("#sel_local").val();
                            var sucursal2 = $("#sel_sucursal").val();
                            var seccion = $("#seccion").val();

                            //alert(seccion);
                            $.ajax({
                                type:"post",
                                dataType:"html",
                                url: base_url+"/fichas/ajax_seccion",
                                data: "loc="+local+"&seccion="+seccion,

                                success: function(msg2){
                                    //alert('seccion');
                                     $("#sel_seccion").empty().removeAttr("disabled").append(msg2);
                                     $("#sel_cargo").empty().attr("disabled", "disabled");                            
                                    log_sucursal = true;
                                    //alert("2");                                              
                                    //probar if
                                    if($("#seccion").val() != '' && $("#local").length && log_sucursal == true){                                
                                        //var seccion2 = $("#seccion").val();
                                        var cargo = $("#cargo").val();
                                        //alert(seccion);                
                                        $.ajax({
                                            type:"post",
                                            dataType:"html",
                                            url: base_url+"/fichas/ajax_cargoD",
                                            data: "local="+local+"&secc="+seccion+"&cargo="+cargo,

                                            success: function(msg3){                            

                                                 $("#sel_cargo").empty().removeAttr("disabled").append(msg3);                           
                                                //alert("3");
                                            }
                                        });               
                                    }
                                    
                                    
                                }
                            });

                        }            
                        //fin sucursal
                }
            });


            
        }
        
        


                
    //----------------------FIN DATOS PREVIOS DE BASE DATOS-------------//       
            
    $("#sel_local").change(function(){                
        var dato = $(this).val();
        local = dato;

        if((dato > 0) || (dato != "")){
            //alert(base_url);
            $.ajax({
                type:"post",
                dataType:"html",
                url: base_url+"/fichas/ajax_sucursal",
                data: "local="+local,
                beforeSend: function () {
                    $("#imglocal").show();
                },
                success: function(msg){
                    //alert(msg);
                    $("#sel_sucursal").empty().removeAttr("disabled").append(msg);
                    $("#sel_sucursal").show();
                    $("#sel_seccion").empty().attr("disabled", "disabled");                            
                    $("#sel_cargo").empty().attr("disabled", "disabled");
                    $("#imglocal").hide();
                }
            });
        }else{
            alert("Debe seleccionar un Local");
            $("#sel_sucursal").attr("disabled", "disabled");
            $("#sel_sucursal").hide();
            $("#sel_seccion").attr("disabled", "disabled");
            $("#sel_seccion").hide();
            $("#sel_cargo").attr("disabled", "disabled");                                
            $("#sel_cargo").hide();
        }
    });
    
            
    $("#sel_sucursal").change(function(){                
        var dato = $(this).val(); 
        //alert(dato);
        if((dato > 0) || (dato != "")){
           
            $.ajax({
                type:"post",
                dataType:"html",
                url: base_url+"/fichas/ajax_seccion",
                data: "loc="+local,
                beforeSend: function () {
                    $("#imgsucursal").show();
                },
                success: function(msg2){
                	//alert(msg2);                            
                   $("#sel_seccion").empty().removeAttr("disabled").append(msg2);
                   $("#sel_seccion").show();
                    $("#sel_cargo").empty().attr("disabled", "disabled");                            
                    $("#imgsucursal").hide();        
                }
            });
        }else{
            alert("Debe seleccionar una sucursal");                    
            $("#sel_seccion").attr("disabled", "disabled");
            $("#sel_seccion").hide();
            $("#sel_cargo").attr("disabled", "disabled");                                
            $("#sel_cargo").hide();                                
        }
    });
 
    $("#sel_seccion").change(function(){                
        var dato = $(this).val(); 
        if((dato > 0) || (dato != "")){
            $.ajax({
                type:"post",
                dataType:"html",
                url: base_url+"/fichas/ajax_cargoD",
                data: "local="+local+"&secc="+dato,
                beforeSend: function () {
                    $("#imgseccion").show();
                },
                success: function(msg3){                            
                    $("#sel_cargo").empty().removeAttr("disabled").append(msg3);                           
                    $("#sel_cargo").show();
                    $("#imgseccion").hide();
                }
            });
        }else{
            alert("Debe seleccionar una secci\u00f3n");                    
            $("#sel_cargo").attr("disabled", "disabled");                                
            $("#sel_cargo").hide();                                                 
        }
    });
    
    $('#sel_region_local').change(function() {
		
		$('#loading').hide();
		
		var region = $(this).val();
		var url = $('#regionurl').val();
		
		$('#loading').fadeIn();
		console.log(region);
		console.log(url);
		$.ajax({
				
				url: url,
				type: 'POST',
				data: 'region='+region,
				success: function(nuevo){
				
					console.log(nuevo);
					$('#sel_comuna_local').html(nuevo);
					$('#sel_comuna_local').show();
					$('#loading').hide();
					$('#sel_comuna_local').parent().find('span').html('Seleccione...');
					
				}
		});
	});

//---------------------------------------Actualizar Ficha--------------------------




    
        if($("#local").val() != '' && $("#local").length){
            var local = $("#local").val();
            var sucursal = $("#sucursal").val();
            //alert(local);
            //alert(sucursal);
            $.ajax({
                type:"post",
                dataType:"html",
                url: base_url+"/fichas/ajax_sucursal",
                data: "local="+local+"&sucursal="+sucursal,
                success: function(msg){                   
                    $("#sel_sucursal2").empty().removeAttr("disabled").append(msg);
                     $("#sel_seccion2").empty().attr("disabled", "disabled");                            
                     $("#sel_cargo2").empty().attr("disabled", "disabled"); 
                    log_seccion = true;
                    //alert(log_seccion);                   
                        //if sucurlal

                        if($("#sucursal").val() != '' && $("#local").length && log_seccion == true ){                     
                            var local2 = $("#sel_local2").val();
                            var sucursal2 = $("#sel_sucursal2").val();
                            var seccion = $("#seccion").val();

                            //alert(seccion);
                            $.ajax({
                                type:"post",
                                dataType:"html",
                                url: base_url+"/fichas/ajax_seccion",
                                data: "loc="+local+"&seccion="+seccion,

                                success: function(msg2){
                                    //alert('seccion');
                                     $("#sel_seccion2").empty().removeAttr("disabled").append(msg2);
                                     $("#sel_cargo2").empty().attr("disabled", "disabled");                            
                                    log_sucursal = true;
                                    //alert("2");                                              
                                    //probar if
                                    if($("#seccion").val() != '' && $("#local").length && log_sucursal == true){                                
                                        //var seccion2 = $("#seccion").val();
                                        var cargo = $("#cargo").val();
                                        //alert(seccion);                
                                        $.ajax({
                                            type:"post",
                                            dataType:"html",
                                            url: base_url+"/fichas/ajax_cargoD",
                                            data: "local="+local+"&secc="+seccion+"&cargo="+cargo,

                                            success: function(msg3){                            

                                                 $("#sel_cargo2").empty().removeAttr("disabled").append(msg3);                           
                                                //alert("3");
                                            }
                                        });               
                                    }
                                    
                                    
                                }
                            });

                        }            
                        //fin sucursal
                }
            });


            
        }







$("#sel_local2").change(function(){                
        var dato = $(this).val();
        local = dato;

        if((dato > 0) || (dato != "")){
            //alert(base_url);
            $.ajax({
                type:"post",
                dataType:"html",
                url: base_url+"/fichas/ajax_sucursal",
                data: "local="+local,
                beforeSend: function () {
                    $("#imglocal").show();
                },
                success: function(msg){
                    //alert(msg);
                    $("#sel_sucursal2").empty().removeAttr("disabled").append(msg);
                    $("#sel_sucursal2").show();
                    $("#sel_seccion2").empty().attr("disabled", "disabled");                            
                    $("#sel_cargo2").empty().attr("disabled", "disabled");
                    $("#imglocal").hide();
                }
            });
        }else{
            alert("Debe seleccionar un Local");
            $("#sel_sucursal2").attr("disabled", "disabled");
            $("#sel_sucursal2").hide();
            $("#sel_seccion2").attr("disabled", "disabled");
            $("#sel_seccion2").hide();
            $("#sel_cargo2").attr("disabled", "disabled");                                
            $("#sel_cargo2").hide();
        }
    });
            
    $("#sel_sucursal2").change(function(){                
        var dato = $(this).val(); 
        //alert(dato);
        if((dato > 0) || (dato != "")){
           
            $.ajax({
                type:"post",
                dataType:"html",
                url: base_url+"/fichas/ajax_seccion",
                data: "loc="+local,
                beforeSend: function () {
                    $("#imgsucursal").show();
                },
                success: function(msg2){
                    //alert(msg2);                            
                   $("#sel_seccion2").empty().removeAttr("disabled").append(msg2);
                   $("#sel_seccion2").show();
                   $("#sel_cargo2").empty().attr("disabled", "disabled");                            
                    $("#imgsucursal").hide();        
                }
            });
        }else{
            alert("Debe seleccionar una sucursal");                    
            $("#sel_seccion2").attr("disabled", "disabled");
            $("#sel_seccion2").hide();
            $("#sel_cargo2").attr("disabled", "disabled");                                
            $("#sel_cargo2").hide();                                
        }
    });
 
    $("#sel_seccion2").change(function(){                
        var dato = $(this).val(); 
        if((dato > 0) || (dato != "")){
            $.ajax({
                type:"post",
                dataType:"html",
                url: base_url+"/fichas/ajax_cargoD",
                data: "local="+local+"&secc="+dato,
                beforeSend: function () {
                    $("#imgseccion").show();
                },
                success: function(msg3){                            
                   $("#sel_cargo2").empty().removeAttr("disabled").append(msg3);                           
                    $("#sel_cargo2").show();
                    $("#imgseccion").hide();
                }
            });
        }else{
            alert("Debe seleccionar una secci\u00f3n");                    
            $("#sel_cargo2").attr("disabled", "disabled");                                
            $("#sel_cargo2").hide();                                                 
        }
    });










    //------------------------------------SALUD----------------------------------------------
    
     
    $("#sel_prev_nombre").change(function(){                
        var dato = $(this).val(); 
        if((dato > 0) || (dato != "")){
            $.ajax({
                type:"post",
                dataType:"html",
                url: base_url+"/fichas/ajax_tipo",
                data: "prevision="+dato,
                beforeSend: function () {
                    $("#imgprev").show();
                    
                },
                success: function(msg3){                            
                    $("#sel_prev_tipo").empty().removeAttr("disabled").append(msg3);
                    $("#sel_prev_tipo").show();
                    $("#imgprev").hide();        
                }
            });
        }else{
            $("#sel_prev_tipo").attr('disabled','disabled');
            $("#sel_prev_tipo").hide();
            alert("Seleccione una Previsi\u00f3n");                    
                                                 
        }
    });
    
    $("#sel_local").change(function(){                
        var dato = $(this).val(); 
        if((dato > 0) || (dato != "")){
            $.ajax({
                type:"post",
                dataType:"html",
                url: base_url+"/fichas/ajax_razon_social",
                data: "cliente_id="+dato,
                beforeSend: function () {
                   
                    
                },
                success: function(msg3){                            
                    $("#sel_razon_social").empty().removeAttr("disabled").append(msg3);
                    $("#sel_razon_social").show();
                         
                }
            });
        }else{
            $("#sel_razon_social").attr('disabled','disabled');
            $("#sel_razon_social").hide();
            alert("Seleccione una razon social");                    
                                                 
        }
    });


    $("#sel_caso_tipo").change(function(){                
        var dato = $(this).val();
        //alert("estamos en function");                
        if((dato > 0) || (dato != "")){
            
        var dato = $("#sel_caso_tipo").val();
        var complej = $("#select_complejidad").val();
        
        if(dato =="Salud" && complej=="prolongado"){
            $("#estadostep").show();  
        }
            $.ajax({
                type:"post",
                dataType:"html",
                url: base_url+"/administrador/ajax_idCasos",
                data: "casos="+dato,
                beforeSend: function () {
                    $("#imgcasotipo").show();
                },
                success: function(msg){
                    // alert(msg);
                    $("#sel_casos").empty().removeAttr("disabled").append(msg);                            
                    $("#sel_casos").show();
                    $("#imgcasotipo").hide();        
                }
            });
        }else{
            alert("Debe seleccionar un Caso");                                                  
            $("#sel_casos").attr("disabled","disabled");
            $("#sel_casos").hide();
        }
    });
    
            
    $("#estadostep").hide(); 
    $("#subestadostep").hide();
    
    
    $("#select_complejidad").change(function () {
        if ($("#hide_reservado").is(":hidden")) {                    
            $("#hide_reservado").slideDown("slow");                                                                   
        } 
        var dato = $("#sel_caso_tipo").val();
        var complej = $("#select_complejidad").val();
        
        if(dato =="Salud" && complej=="prolongado"){
            $("#estadostep").show(); 
            
            
        }else{
           $("#estadostep").hide(); 
        }

    }); 
     $("#estadostepa").change(function () {
          var dato = $(this).val();
          if(dato=="No Continúa"){
            $("#subestadostep").show();
              
          }else{
            $('#subestadostep').hide();
          }

    }); 
    
     var datostep= $("#estadostep2").val();
     var datosubstep =$("#subestadostep2").val();
     
     if(datostep=="No Continúa"){
      $("#subestadostep2").show();
     }else{
      $('#subestadostep2').hide();  
     }
//     if(datosubstep ==""){
//         
//     }else{
//         
//     }
     $("#estadostep2").change(function () {
          var dato = $(this).val();
          console.log(dato);
          if(dato=="No Continúa"){
            $("#subestadostep2").show();
              
          }else{
            $('#subestadostep2').hide();
          }

    }); 
   
   
//caja twiter
/*$(document).ready(function(){
            $('textarea[maxlength]').keyup(function(){
                    var max = parseInt($(this).attr('maxlength'));
                    if($(this).val().length > max){
                            $(this).val($(this).val().substr(0, $(this).attr('maxlength')));
                    }
                    $(this).parent().find('.charsRemaining').html('' + (max - $(this).val().length) + '');
                    
            });
    });*/

//caja fin twiter        
//-----------------------------------------Cajas estado---------------------------------------------------
$(".estado").click(function(){
    var id_estado = $(this).attr('id');
    //alert(id_estado);
    if($("#estado"+id_estado).html() == 'Cerrada'){
        if(confirm('Est\u00e1 a punto de Abrir una Problem\u00e1tica Nuevamente, \u00bfDesea Continuar?')){
                
        var ficha = $("#ficha_"+id_estado).html();
        var problematica = $("#problematica_"+id_estado).html();
                $.ajax({
                    type:"post",
                    dataType:"html",
                    url: base_url+"/fichas/ajax_updateEstado",
                    data: "ficha="+ficha+"&problematica="+problematica,
                    success: function(msg){
                        $("#estado"+id_estado).html(msg);
                        $("#"+id_estado).remove();
                       window.location.reload();
                       // console.log(msg);
                        //$("#lock"+id_estado).attr('src',base_img+"/img/icons/sidemenu/lock_open.png");
                        //$("#estatus").attr('title','Caso Abierto');

                    }
                });
         }       
    }else{

           if(confirm('Est\u00e1 a punto de Cerrar una Problem\u00e1tica, \u00bfDesea Continuar?')){
                
        var ficha = $("#ficha_"+id_estado).html();
        var problematica = $("#problematica_"+id_estado).html();
                $.ajax({
                    type:"post",
                    dataType:"html",
                    url: base_url+"/fichas/ajax_updateEstadoCerrar",
                    data: "ficha="+ficha+"&problematica="+problematica,
                    success: function(msg){

                        $("#estado"+id_estado).html(msg);
                        $("#"+id_estado).remove();
                        window.location.reload();
                        //console.log(msg);
                        //$("#lock"+id_estado).attr('src',base_img+"/img/icons/sidemenu/lock_open.png");
                        //$("#estatus").attr('title','Caso Abierto');

                    }
                });
         } 



      // alert('Si desea cerrar Ingrese a Ver Problem\u00e1tica');  
    }       
}); 

//------------------------------------------Fin caja estado-----------------------------------------------

//---------------------------------------edicion contacto----------------------------
$(".contacto_tec").attr("disabled", "disabled");
$(".contacto_int").attr("disabled", "disabled");
$(".contacto_ins").attr("disabled", "disabled");


//---------------------------------------Fin edicion contacto---------------------------------------------------
//---------------------------------------edicion Problematica---------------------------------------------------
    if($("#casohide").val() != ''){
        var caso = $("#casohide").val();
        var caso2 = $("#caso2").val();        
        //alert(caso2);
        $.ajax({
            type:"post",
            dataType:"html",
            url: base_url+"/administrador/ajax_idCasosUpdate",
            data: "caso="+caso+"&caso2="+caso2,

            success: function(msg){
                //alert(msg);
                $("#sel_casos").empty().removeAttr("disabled").append(msg);                           
                            
            }
        });   
    } 
//---------------------------------------Fin edicion Problematica---------------------------------------------------


//esto esta incompleto.
//$("#cerrar_programa_grupal").click(function(){
//    var programa = $("#cerrar_programa_grupal").parent().attr('id');
//    //alert(programa);
//                $.ajax({
//                    type:"post",
//                    dataType:"html",
//                    url: base_url+"/programas/ajax_cerrar_programa_grupal",
//                    data: "id_programa="+programa,
//                    success: function(msg){
//                     console.log(msg);
//
//                    }
//                });    
//    
//});

$(".estadoProg").click(function(){
    
        var campo_busqueda = $('#select1').val();
        var campo_programas = $('#select_programas').val();
        var input_busqueda = $('#textfield13').val();
        console.log(campo_busqueda, campo_programas, input_busqueda);
        
  if(confirm('Est\u00e1 a punto de Abrir un Programa Grupal, \u00bfDesea Continuar?')){
  var programa = $(".estadoProg").parent().attr('id');
  //alert(programa);   
//  var dato_busqueda =$dato_busqueda; 
//   console.log('dato_busqueda');
 
       
    
        $.ajax({
                    type:"post",
                    dataType:"html",
                    url: base_url+"/programas/ajax_abrir_programa_grupal",
                    data: "id_programa="+programa,
                    success: function(msg1){
                        
                    document.forms["form_resultado"].submit();
   
                        
//                        location.reload(true);
//                        history.go(0);
//                    location.reload(true);
//                    console.log(msg); 

//                $.ajax({
//                    type:"post",
//                    dataType:"html",
//                    url: base_url+"/programas/ver_programas",
//                    data: {
//                        "input_busqueda": input_busqueda,
//                          "select_busqueda": campo_busqueda,
//                          "select_programas": campo_programas,
//                            },
//                    
//                    success: function(msg2){
// 
////                        window.location.reload();
//                      console.log(msg2); 
//                    }
//                });  
                    }
                });    
  }
});
$(".estadoProgI").click(function(){
  
  if(confirm('Est\u00e1 a punto de Abrir un Programa Individual, \u00bfDesea Continuar?')){
  var programaI = $(".estadoProgI").attr('id');
  // alert(programaI);   
    
        $.ajax({
                    type:"post",
                    dataType:"html",
                    url: base_url+"/programas/ajax_abrir_programa_individual",
                    data: "id_programa="+programaI,
                    success: function(msg){
                    window.location.reload();
                   // console.log(msg); 
                    }
                });    
  }
});



$(".revisar_rut").click(function(){

    var rut_id = $(this).attr('id');
    alert(rut_id);
    
});


    /*$("#fechas").stop(true,true).delegate( ".datepicker-input", "hover", function(){
        var id = $(this).attr("id");
        $("#"+id).datepicker();
    });*/
        
    $('#estadostepa').change(function(){                                                           
       if($(this).val() == 'No Continua') {
       // $('#hide_frustrado').show();
       alert('Ha Seleccionado No Continúa Deberia cerrar la Problematicas al Finalizar el proceso');
       $("#mensaje_NC").append('<div class="albox warningbox mensaje_NC_warning" id"mensaje_NC_warning">Recuerde Cerrar la Problematica ya que esta <strong>No Continúa</strong><a href="#" class="close tips" title="cerrar">cerrar</a></div>');
        //<div class="albox warningbox">
          //                                  <a href="#" class="close tips" title="cerrar">cerrar</a>
            //                                
       }else{
       	$(".mensaje_NC_warning").remove();
       }                                           
    });


});

