
$(document).ready(function(){
	var totalPoints = 0;
	$("#situacion_economica_ingreso").find('input').each(function(i,n){
		totalPoints += parseInt($(n).val(),10); 
	});
	$("#totalingreso").val(totalPoints);
	totalPoints = 0;
	$("#situacion_economica_egreso").find('input').each(function(i,n){
		totalPoints += parseInt($(n).val(),10); 
	});
	$("#totalegreso").val(totalPoints);
	
	
	$("#saldogastos").val(parseInt($("#totalingreso").val(),10)-parseInt($("#totalegreso").val(),10));
		
	$("#sel_nacionalidad").change(function(){
		if($("#sel_nacionalidad option:selected").text()=="Otra"){
			$("#nacionalidad_otra").css('visibility','visible');
			$("#nacionalidad_otra").focus();
		}else{
			$("#nacionalidad_otra").css('visibility','hidden');
			$("#nacionalidad_otra").val("");
		}
	});

	$("#sel_vivienda").change(function(){
		if($("#sel_vivienda option:selected").text()=="Otro"){
			$("#vivienda_otro").css('visibility','visible');
			$("#vivienda_otro").focus();
		}else{
			$("#vivienda_otro").css('visibility','hidden');
			$("#vivienda_otro").val("");
		}
	});

	$("#sel_plan_colectivo").change(function(){
		if($("#sel_plan_colectivo option:selected").text()=="No"){
			$("#prevision").css('visibility','visible');
			$("#prevision").focus();
		}else{
			$("#prevision").css('visibility','hidden');
			$("#prevision").val("");
		}
	});

	$("#sel_afp").change(function(){
		if($("#sel_afp option:selected").text()=="Jubilado"){
			$("#jubilacion").css('visibility','visible');
			$("#jubilacion").focus();
		}else{
			$("#jubilacion").css('visibility','hidden');
			$("#jubilacion").val("");
		}
	});

	$("#sel_tramo").change(function(){
		if($("#sel_tramo option:selected").text()=="Si"){
			$("#detalle_seguro").css('visibility','visible');
			$("#detalle_seguro").focus();
		}else{
			$("#detalle_seguro").css('visibility','hidden');
			$("#detalle_seguro").val("");
		}
	});
	
	var textogrupo = $("#btn_agregar").val();

	$("#modal_grupo_familiar").hide();

	$("#sueldo_base").keydown(function(event) {
		if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
		} else {
			if (event.keyCode < 95) {
				if (event.keyCode >= 37 && event.keyCode <= 40) {
				} else {
					if (event.keyCode < 48 || event.keyCode > 57) {
						event.preventDefault();
					}
				}
			} else {
				if (event.keyCode < 96 || event.keyCode > 105) {
					event.preventDefault();
				}
			}
		}
	});
	
	$("#situacion_economica input").keydown(function(event) {
		if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
		} else {
			if (event.keyCode < 95) {
				if (event.keyCode >= 37 && event.keyCode <= 40) {
				} else {
					if (event.keyCode < 48 || event.keyCode > 57) {
						event.preventDefault();
					}
				}
			} else {
				if (event.keyCode < 96 || event.keyCode > 105) {
					event.preventDefault();
				}
			}
		}
	});


	$("#situacion_economica input").keydown(function(event) {
		if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
		    } else {
				if (event.keyCode < 95) {
					if (event.keyCode >= 37 && event.keyCode <= 40) {
				} else {
					if (event.keyCode < 48 || event.keyCode > 57) {
						event.preventDefault();
					}
				}
			} else {
				if (event.keyCode < 96 || event.keyCode > 105) {
					event.preventDefault();
				}
			}
		}
	});
   
	$("#situacion_economica input").focus(function(){
		$(this).select();
	});
	   
	$('#situacion_economica_ingreso input').change(function(){
		var totalPoints = 0;
		$("#situacion_economica_ingreso").find('input').each(function(i,n){
			totalPoints += parseInt($(n).val(),10); 
		});
		$("#totalingreso").val(totalPoints);
		var totalPoints2 = parseInt($("#totalegreso").val(),10);
		$("#saldogastos").val(totalPoints-totalPoints2);
	});
	
	$('#situacion_economica_egreso input').change(function(){
		var totalPoints = 0;
		$("#situacion_economica_egreso").find('input').each(function(i,n){
			totalPoints += parseInt($(n).val(),10); 
		});
		$("#totalegreso").val(totalPoints);
		var totalPoints2 = parseInt($("#totalingreso").val(),10);
		$("#saldogastos").val(totalPoints2-totalPoints);
	});
	
	$('#grupofamiliar').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"aoColumns": [
				{ "sTitle": "Id", "sClass": "center" },
				{ "sTitle": "Nombre", "sClass": "center" },
				{ "sTitle": "Parentesco", "sClass": "center"},
				{ "sTitle": "Fecha de Nac.", "sClass": "center"},
				{ "sTitle": "Ocupacion", "sClass": "center" },
				{ "sTitle": "Enfermedad", "sClass": "center" },
				{ "sTitle": "Cronica", "sClass": "center" },
				{ "sTitle": "Tratamiento", "sClass": "center"} 
		    ]
	});
	
	var aPos;
	var aData;
	$("#grupofamiliar tbody tr").live( "click", function() {
		oTable = $('#grupofamiliar').dataTable();
		var data = oTable.fnGetData( this );
		if(data != null){
			if ( $(this).hasClass("row_selected") ) {
				$(this).removeClass("row_selected");
				textogrupo = "Agregar nuevo integrante";
				$("#btn_agregar").val(textogrupo);
			}
			else {
				$("tr.row_selected").removeClass("row_selected");
				$(this).addClass('row_selected');
				textogrupo = "Editar integrante";
				$("#btn_agregar").val(textogrupo);
				aPos = oTable.fnGetPosition( this );
				aData = oTable.fnGetData( aPos );
				console.log(aData);
			}
		}
	});
	
	$("#btn_agregar").click(function(){
		
		
			
		$("#modal-close").click(function(){ $.modal.close(); });

		$("#modal-submit").click(function(){
			var strNombre = $("#modal-nombre").val();
			var strParentesco = $("#modal-parentesco").val();
			var strFNacimiento = $("#modal-fnacimiento").val();
			var strOcupacion = $("#modal-ocupacion").val();
			var strEnfermedad = $("#modal-enfermedad").val();
			var strCronica = $("#modal-cronica option:selected").text();
			var strTratamiento = $("#modal-tratamiento option:selected").text();
			if(strCronica.length>2){strCronica="";}
			if(strTratamiento.length>2){strTratamiento="";}
			if( strNombre == "" || strParentesco == "" || strFNacimiento == "" || strOcupacion == "" ){
				$("#modal-error").text("Debe ingresar valores a los campos correspondientes");
			}else{
				if (!$("#chk-enfermedad").is(":checked")) {
					if( strEnfermedad == "" || strCronica == "" || strTratamiento == "" ){
						$("#modal-error").text("Debe ingresar valores a los campos correspondientes");
						return;
					}
				}
				if($("#btn_agregar").val()=="Editar integrante"){
					
					$('#grupofamiliar').dataTable().fnUpdate( [
						aPos+1,
						strNombre,
						strParentesco,
						strFNacimiento,
						strOcupacion,
						strEnfermedad,
						strCronica,
						strTratamiento], aPos );
					$('#grupofamiliar').dataTable().fnSort( [[0,'asc']] );
					$.modal.close();
				}else{
					var idRow = $('#grupofamiliar').dataTable().fnGetData().length + 1;
					$('#grupofamiliar').dataTable().fnAddData( [
						idRow,
						strNombre,
						strParentesco,
						strFNacimiento,
						strOcupacion,
						strEnfermedad,
						strCronica,
						strTratamiento] );
					$('#grupofamiliar').dataTable().fnSort( [[0,'asc']] );
					$.modal.close();
				}
			}
			
		});
		$("#chk-enfermedad").click(function(){
			if ($(this).is(":checked")) {
				$("#modal-enfermedad").val("sin enfermedad");
				$("#modal-cronica option[value='0']").attr("selected", "selected");
				$("#modal-tratamiento option[value='0']").attr("selected", "selected");
				$("#modal-enfermedad").prop("disabled", true);
				$("#modal-cronica").prop("disabled", true);
				$("#modal-tratamiento").prop("disabled", true);
			}else{
				$("#modal-enfermedad").prop("disabled", false);
				$("#modal-cronica").prop("disabled", false);
				$("#modal-tratamiento").prop("disabled", false);
			}
		});
	});
	
	
	$("form").submit(function(e){
		var data = { familiar:[] };
		datos = $('#grupofamiliar').dataTable().fnGetData();
		for (var i=0;i<datos.length;i++)
		{ 
			data.familiar.push({
				"ficha_id":"",
				"nombre":datos[i][1],
				"parentesco":datos[i][2],
				"fecha_nacimiento":datos[i][3],
				"ocupacion":datos[i][4],
				"enfermedad":datos[i][5],
				"cronica":datos[i][6],
				"tratamiento":datos[i][7]
			});
		}
		
		data = JSON.stringify(data);
		
		$.ajax({
			async:false,    
			dataType: 'json', 
			type: "POST", 
			url: base_url+"/administrador/ajax_grupo_familiar", 
			data: data, 
			success: function(response){
				console.log(response);
			}
		});
	});
});

function fnDeleteRow() {
	var anSelected = $("#grupofamiliar tbody tr.row_selected");
    if ( anSelected.length !== 0 ) {
         $("#grupofamiliar").dataTable().fnDeleteRow( anSelected[0] );
    }
}