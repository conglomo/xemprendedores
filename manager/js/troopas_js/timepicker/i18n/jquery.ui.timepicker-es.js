/* Japanese initialisation for the jQuery time picker plugin. */
/* Written by Bernd Plagge (bplagge@choicenet.ne.jp). */
jQuery(function($){
    $.timepicker.regional['es'] = {
                hourText: 'Hora',
                minuteText: 'Minutos',
                amPmText: ['AM', 'PM'] }
    $.timepicker.setDefaults($.timepicker.regional['es']);
});