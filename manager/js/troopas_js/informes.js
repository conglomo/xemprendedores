$(function() {

    /* Filtro de empresas para locales en selecci�n m�ltiple */

    $('#filtro_cliente').change(function() {

        var cliente = $(this).val();
        var url = $('#url').val();

        if (cliente != '') {

            $.ajax({
                type: "post",
                url: url,
                data: "cliente=" + cliente,
                beforeSend: function() {
                    $("#imglocal").show();
                },
                success: function(msg) {
                    $("#filtro_locales").html(msg);
                    $("#imglocal").hide();
                }
            });

        }
    });

});