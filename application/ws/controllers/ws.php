<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';

class Ws extends REST_Controller
{
	function __construct()
	{
		parent::__construct();
		//$this->load->model('m_species', 'species');
		$this->load->model("site_model");
	}

	function get_post()
	{
		$result = array();
		$name = $this->post('name', true);
		$limit = $this->post('limit', true);
		$query = $this->species->get($name, $limit);

		if (!$query)
		{
			$result['codigo']	= 102;
			$result['mensaje']	= 'No existen valores para esos atributos.';
		}
		else
		{
			$especies = array();

			foreach ($query as $row) 
			{
				$data['id']				= $row->id;
				$data['nombre']			= $row->nombre;
				$data['caracteristica']	= $row->caracteristica;
				$data['tipo']			= $row->tipo;
				$data['familia']		= $row->familia;
				$data['genero']			= $row->genero;
				$data['especie']		= $row->especie;
				$data['variedad']		= $row->variedad;
				$data['forma']			= $row->forma;
				$data['luz']			= $row->luz;
				$data['altura']			= $row->altura;
				$data['diametro']		= $row->diametro;
				$data['buscador']		= $row->nombre.' '.$row->genero.' '.$row->especie;

				$images = array();

				if(isset($row->foto1) && $row->foto1 != 'n_ull')
				{
					$images[] = $row->foto1;
				}
				if(isset($row->foto2) && $row->foto2 != 'n_ull')
				{
					$images[] = $row->foto2;
				}

				$data['fotos'] = $images;

				array_push($especies, $data);
			}

			$result['codigo']	= 100;
			$result['mensaje']	= 'Informacion desplegada correctamente.';
			$result['100']		= $especies;
		}

		$this->response($result, 200);
	}

	function get_localidades_post()
	{
		$result = array();
		$name = $this->post('name', true);
		$limit = $this->post('limit', true);
		$query = $this->site_model->getLocalidades();

		if (!$query)
		{
			$result['codigo']	= 102;
			$result['mensaje']	= 'No existen valores para esos atributos.';
		}
		else
		{
			$especies = array();

			foreach ($query as $row) 
			{
				$data['id_ciudad']		= $row->id_ciudad;
				$data['nombre']			= $row->nombre;
				


				array_push($especies, $data);
			}

			$result['codigo']	= 100;
			$result['mensaje']	= 'Informacion desplegada correctamente.';
			$result['100']		= $especies;
		}

		$this->response($result, 200);
	}

	function get_mensajes_post()
	{
		$result = array();
		$origen = $this->post('input_origen', true);
		$destino = $this->post('input_destino', true);
		$estado = $this->post('input_estado', true);
		$query = $this->site_model->getMensaje($origen, $destino, $estado);

		if (!$query)
		{
			$result['codigo']	= 102;
			$result['mensaje']	= 'No existen valores para esos atributos.';
		}
		else
		{
			$especies = array();

			foreach ($query as $row) 
			{
				$data['id']			= $row->id;
				$data['origen']		= $row->origen;
				$data['destino']	= $row->destino;
				$data['pregunta']	= $row->pregunta;
				$data['estado']		= $row->estado;
				$data['mensaje']	= $row->mensaje;
				$data['tipo']	= $row->tipo;
				


				array_push($especies, $data);
			}

			$result['codigo']	= 100;
			$result['mensaje']	= 'Informacion desplegada correctamente.';
			$result['100']		= $especies;
		}

		$this->response($result, 200);
	}

	function get_Insertmensajes_post()
	{
		$result = array();
		$origen = $this->post('input_origen', true);
		$destino = $this->post('input_destino', true);
		$pregunta = $this->post('input_pregunta', true);
		$estado = $this->post('input_estado', true);
		$mensaje = $this->post('input_mensaje', true);
		$tipo = $this->post('input_tipo', true);

		$query = $this->site_model->getInsertMensaje($origen, $destino, $pregunta, $mensaje, $estado, $tipo);

		if (!$query)
		{
			$result['codigo']	= 102;
			$result['mensaje']	= 'No se ingresaron los datos.';
		}
		else
		{
			$especies = array();
			array_push($especies, $query);

			

			$result['codigo']	= 100;
			$result['mensaje']	= 'Informacion desplegada correctamente.';
			$result['100']		= $especies;
		}

		$this->response($result, 200);
	}

	function get_Updatemensajes_post()
	{
		$result = array();
		//$republicar = $this->post('input_republicar', true);
		$id = $this->post('input_id', true);
		$origen = $this->post('input_origen', true);
		$destino = $this->post('input_destino', true);
		$pregunta = $this->post('input_pregunta', true);
		$estado = $this->post('input_estado', true);
		$mensaje = $this->post('input_mensaje', true);

		$query = $this->site_model->getUpdateMensaje($id, $estado);

		if (!$query)
		{
			$result['codigo']	= 102;
			$result['mensaje']	= 'No se ingresaron los datos.';
		}
		else
		{
			$especies = array();
			array_push($especies, $query);

			

			$result['codigo']	= 100;
			$result['mensaje']	= 'Informacion desplegada correctamente.';
			$result['100']		= $especies;
		}

		$this->response($result, 200);
	}
}