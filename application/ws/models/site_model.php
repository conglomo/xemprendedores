<?php
/**
 * Description of site_model
 *
 * @author nicolas
 */
class site_model extends CI_Model{
    
    function validate_user($username, $password){
        $this->db->where("usr_username", $username);
        $this->db->where("usr_password", sha1($password));
        //$this->db->where("usr_role", "1");
        //$this->db->or_where('usr_role', "3"); 
        $query = $this->db->get("usuarios");
        
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }

    public function getLocalidades(){
        $this->db->select('*');
        $this->db->from('ciudad');
        $query = $this->db->get();
       if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }

    }

    public function getMensaje($origen, $destino){
        $this->db->select('*');
        if ($origen!=0 && $destino!=0) {
            $this->db->where('origen', $origen);
            $this->db->or_where('origen', $destino);
            $this->db->where('destino', $destino);
            $this->db->or_where('destino', $origen);
        }
        if ($destino!=0 && $origen==0) {
            $this->db->where('destino', $destino);
            //$this->db->or_where('destino', $origen);
        }
        if ($origen!=0 && $destino==0) {
            $this->db->where('origen', $origen);
            //$this->db->or_where('origen', $destino);
        }
        
        
        $this->db->order_by('id', 'desc');
        //$this->db->where('estado', $estado);
        $this->db->from('mensaje');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
        
    }

    public function getInsertMensaje($origen, $destino, $pregunta, $mensaje, $estado, $tipo){
        $data = array(


            'origen' => $origen,

            'destino' => $destino,

            'pregunta' => $pregunta,

            'mensaje' => $mensaje,

            'tipo' => $tipo,

            'estado' => $estado

        );

        $this->db->insert('mensaje', $data);

        if ($this->db->affected_rows() > 0) {

            return $data;

        }

        else {

            return false;

        }
        
    }

    public function getUpdateMensaje($id, $estado){
        $data = array(


           

            'estado' => $estado

        );
        $this->db->where('id', $id);
        $this->db->update('mensaje', $data);
        

        if ($this->db->affected_rows() > 0) {

            return $data;

        }

        else {

            return false;

        }
        
    }

    
}

/* End of file site_mdel.php */
/* Location: ./application/models/site_model.php */