<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_main', 'main');
	}

	public function index()
	{
		//$data['plantilla'] = 'main';
		$this->load->view('main');
	}

	public function proyectos()
	{
		//$data['plantilla'] = 'main';
		$this->load->view('proyectos');
	}

	public function contacto()
	{
		//$data['plantilla'] = 'main';
		$this->load->view('contacto');
	}

	public function crear()
	{
		$query = $this->main->obtenerLocalidades();

		$data['ciudades'] = $query;
		$data['plantilla'] = 'form';
		$this->load->view('template', $data);
	}

	public function crear_mensaje()
	{
		$monto_donacion = $this->input->post('monto');
		$materiales_donacion = $this->input->post('materiales');
		$horas_donacion = $this->input->post('horas');
		$profesion = $this->input->post('profesion');
		$idea_innovadora = $this->input->post('idea');

		//js
		$nombre = $this->input->post('nombres');
		$apellidos = $this->input->post('apellidos');
		$rut = $this->input->post('rut');
		$email = $this->input->post('email');
		$telefono = $this->input->post('telefono');

		/*$this->form_validation->set_rules('monto', 'Monto', 'trim');
		$this->form_validation->set_rules('materiales', 'Materiales', 'trim');
		$this->form_validation->set_rules('horas', 'Horas', 'trim');
		$this->form_validation->set_rules('profesion', 'Profesion', 'trim');
		$this->form_validation->set_rules('idea', 'Idea', 'trim');*/

		/*if ($this->form_validation->run() == false)
		{
			
			$data['error'] = true;
			$this->load->view('proyectos');
		}*/
		
			$datos = array(
				'monto_donacion' => $monto_donacion,
				'materiales_donacion' => $materiales_donacion,
				'horas_donacion' => $horas_donacion,
				'profesion' => $profesion,
				'idea_innovacion' => $idea_innovadora,
				'telefono' => $telefono,
				'email' => $email,
				'rut' => $rut,
				'apellido' => $apellidos,
				'nombre' => $nombre,
			);


			$query = $this->main->create($datos);
			
			if ($query)
			{
				$user="XEmprendedores";
				$email2="gahumada@desafiochile.cl, psmith@desafiochile.cl";
				$entrevistado_correo = "gahumada@desafiochile.cl, psmith@desafiochile.cl";
				$email_data = array('nombre' => $nombre . " " . $apellidos, 'rut' => $rut, 'email' => $email, 'telefono' => $telefono, 'monto' => $this->input->post('monto'), 'materiales' => $this->input->post('materiales'), 'horas' => $this->input->post('horas'), 'profesion' => $this->input->post('profesion'), 'idea' => $this->input->post('idea'));

				$email_view = $this->load->view('email_nuevo_evento', $email_data, TRUE);

				$this->sendEmail($email2, $user, $email_view, $entrevistado_correo, $this->input->post('mensaje'));

				$email_data_2 = array('nombre' => $nombre . " " . $apellidos, 'rut' => $rut, 'email' => $email, 'telefono' => $telefono, 'monto' => $this->input->post('monto'), 'materiales' => $this->input->post('materiales'), 'horas' => $this->input->post('horas'), 'profesion' => $this->input->post('profesion'), 'idea' => $this->input->post('idea'));

				$email_view = $this->load->view('confirmacion_donacion', $email_data_2, TRUE);
				$this->sendEmail($email, $user, $email_view, $email, $this->input->post('mensaje'));

				//redirect('gracias#wifi');
			}
		
	}

	public function sendEmail($sender_email = null,$sender_name = null, $content, $email_to, $mensaje)
	{
        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'desarrollo@ibex.cl';
        $config['smtp_pass'] = 'Xebi.2010';
//        $config['smtp_host'] = 'ssl://owa.mutual.cl';
//        $config['smtp_port'] = '587';
//        $config['smtp_user'] = 'gerencialan@mutual.cl';
//        $config['smtp_pass'] = 'Mut$2014';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'utf-8';
        //$config['wordwrap'] = TRUE;
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        
        $this->email->initialize($config);
        
        $this->email->from('desarrollo@ibex.cl', 'XEmprendedores');
        $this->email->to($email_to); 
        /*if ($sender_email != null && $sender_name != null){
            $this->email->reply_to($sender_email, $sender_name);
        }*/
//        $this->email->cc('another@another-example.com'); 
//        $this->email->bcc('them@their-example.com'); 

        $this->email->subject('Nueva donación realizada: ' . $mensaje);
        $this->email->message($content);	

        if (!$this->email->send())
        {
            return false;
        }
        else
        {
            return true;
        }

		//echo $this->email->print_debugger();
    }

	public function condiciones()
	{
		$data['plantilla'] = 'condiciones';
		$this->load->view('template', $data);
	}

	public function exito()
	{
		$data['plantilla'] = 'confirmacion';
		$this->load->view('template', $data);
	}

	public function mensajes($origen, $destino, $dia)
	{
		if ($dia == 0)
		{
			$data['color'] = '#fff';
			$data['background'] = '#000';
			$data['border'] = '1px solid #fff';
			$data['imagen'] = 'noche';
		}
		else if ($dia == 1)
		{
			$data['color'] = '#000';
			$data['background'] = '#fff';
			$data['border'] = '1px solid #000';
			$data['imagen'] = 'dia';
		}

		$this->load->view('mensajes', $data);
	}

	public function obtenerLocalidad($id)
	{
		$localidades = array(
			"Todos" => 0,
			"Chungungo" => 1,
			"El Trapiche" => 2,
			"La Higuera" => 3,
			"Los Hornos" => 4,
			"Los Choros" => 5,
			"Punta de Choros" => 6,
			"Punta Colorada" => 7,
			"Quebrada Honda" => 8,
			"Totoralillo Norte" => 9
		);
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */