<?php if (! defined('BASEPATH')) exit('No direct script access');

class M_main extends CI_Model
{
	function obtenerLocalidades()
	{
		$ch = curl_init("http://hola-hola.cl/ws/index.php/ws/get_localidades/format/json");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "test"); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$ciudades = json_decode(curl_exec($ch));

		if ($ciudades->{'codigo'} == 100)
		{
			return $ciudades->{'100'};
		}
		else
		{
			return false;
		}
	}

	public function create($data){
        
        
        $this->db->insert("formulario", $data);
        
        if($this->db->affected_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

	function crearMensaje($data)
	{
		$ch = curl_init("http://hola-hola.cl/ws/index.php/ws/get_Insertmensajes/format/json");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$ciudades = json_decode(curl_exec($ch));

		if ($ciudades->{'codigo'} == 100)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}