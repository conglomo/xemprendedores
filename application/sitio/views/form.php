<script type="text/javascript">
	$(document).ready(function(){
		$("textarea[maxlength]").keyup(function() {
			var limit   = $(this).attr("maxlength"); // Límite del textarea
			var value   = $(this).val();             // Valor actual del textarea
			var current = value.length;              // Número de caracteres actual
			if (limit < current)
			{
				$(this).val(value.substring(0, limit));
			}
		});
	});
</script>
<article class='content'>
	<div id='contenido' class='width-65'>
		<h2 class='bg-base base'>ESCRIBE UN MENSAJE A LOS VECINOS DE LA HIGUERA</h2>
		<div id='form'>
			<?php if (isset($error) and $error != false) { ?>
				<div class='mtop bg-base' style='padding: 5px 10px; border-radius: 5px;'>Error: Todos los campos son obligatorios</div>
			<?php } ?>
			<form action='<?php echo site_url('crear-mensaje'); ?>' method='POST'>
				<!-- <select id='tipo' class='select mtop'>
					<option>tipo</option>
					<option value='1' selected>mensaje</option>
					<option value='2'>papeleta</option>
					<option value='3'>pizarrra</option>
				</select> -->
				<!-- <div id='opcional'>
					<select id='papeleta' class='select mtop' style='display: none'>
						<option>tipo</option>
					</select>
					<input type='text' id='pizarra' class='mtop' placeholder='Ingrese su Pregunta' style='display: none' />
				</div> -->
				<select name='origen' class='mtop' required>
					<option value='' ng-disabled='!!model'>ORIGEN</option>
					<?php foreach ($ciudades as $origen) { ?>
						<option value='<?php echo $origen->{'id_ciudad'}; ?>' <?php echo ($origen->{'id_ciudad'} == set_value('origen'))?'selected':null;?>><?php echo $origen->{'nombre'}; ?></option>
					<?php } ?>
				</select>
				<select name='destino' class='mtop' required>
					
					<option value='' ng-disabled='!!model'>DESTINO</option>
					<?php foreach ($ciudades as $destino) { ?>
						<option value='<?php echo $destino->{'id_ciudad'}; ?>' <?php echo ($destino->{'id_ciudad'} == set_value('destino'))?'selected':null;?>><?php echo $destino->{'nombre'}; ?></option>
					<?php } ?>
				</select>
				<textarea name='mensaje' class='mtop' placeholder='Escribe aquí tu mensaje (máximo 80 caracteres).' maxlength='80' required><?php echo set_value('mensaje'); ?></textarea>
				<img src='<?php echo base_url('images/hombre.png'); ?>' alt='Mmm...' class='hombre' />
				<span class='mtop display-block width-100 align-center'>
					<a href='<?php echo site_url('/condiciones-de-uso'); ?>' target='_Blank'>
						CONDICIONES DE USO
					</a>
				</span>
				<div class='width-100 align-right'>
					<input type='image' name='enviar' src='<?php echo base_url('images/enviar.png'); ?>' class='enviar' alt='Enviar Mensaje' />
				</div>
			</form>
		</div>
	</div>
	<br class='clear' />
</article>