<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PorEmprendedores.org</title>
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/idangerous.swiper.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/menu_elastic.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/main.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
    
    <!--sidebar menu-->
    <script src="<?php echo base_url(); ?>/js/snap.svg-min.js"></script>
</head>
<body id="home">
    <div id="preloader">
        <div id="status">
            <div id="loader"></div><!--loader-->
        </div><!--status-->
    </div><!--preloader-->
    
    <div class="menu-container hidden-lg">
        <div class="menu-wrap">
            <nav class="menu hidden-lg" id="slide-menu">
                <div class="icon-list">
                    <a class="navbar-brand sidebar-brand" href=""></a>
                    <a href="#"><span class="active-link icon-list-item">Inicio</span></a>
                    <a href="<?php echo base_url(); ?>index.php/main/proyectos"><span class="icon-list-item">Proyecto</span></a>
                    <a href=""><span class="icon-list-item">Noticias</span></a>
                    <a href="<?php echo base_url(); ?>index.php/main/contacto"><span class="icon-list-item">Contacto</span></a>
                </div><!--icon-list-->
            </nav>
            
            <button class="close-button hidden-lg" id="close-button">Cerrar Menu</button>
            <div class="morph-shape" id="morph-shape" data-morph-open="M-1,0h101c0,0,0-1,0,395c0,404,0,405,0,405H-1V0z">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
                    <path d="M-1,0h101c0,0-97.833,153.603-97.833,396.167C2.167,627.579,100,800,100,800H-1V0z"/>
                </svg>
            </div><!--morph-shape-->
        </div><!--menu-wrap-->
    </div><!-- menu-container -->

    <nav class="navbar">
        <div class="container">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header nav-transparent">
                    <button type="button" class="navbar-toggle collapsed menu-button" data-toggle="collapse" id="open-button" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                    <a class="navbar-brand" href="" id="0"></a>
                </div><!--navbar-header-->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right visible-lg" id="nav">
                        <li><a href="#" class="active-link text-white">Inicio</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/main/proyectos" class="text-white">Proyecto</a></li>
                        <li><a href="" class="text-white">Noticias</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/main/contacto" class="text-white">Contacto</a></li>
                        <li id="iconSocialNav">
                            <a href="#" class="icoNav text-white"><img src="img/icons/facebook-head.png" alt=""></a>
                            <a href="#" class="icoNav text-white"><img src="img/icons/twitter-head.png" alt=""></a>
                        </li>
                    </ul>
                </div><!--navbar-collapse -->
            </div><!--container-fluid -->
        </div><!--container-->
    </nav>  
    
    <div class="swiper-container">
        <div class="my-pagination"></div><!--my-pagination-->
	    <div class="swiper-wrapper" id="swiper-wrapper">
	        <div class="swiper-slide header content-wrap">  
                <div id="scene" class="scene">
                    <div class="layer layer-header-parallax header-inner-parallax"></div><!--layer-->
                    <div class="layer layer-header-parallax header-outer-parallax" data-depth="0.05"></div><!--layer-->

                    <img src="img/sections/mancha1.png" class="portfolio-outer-image portfolio-hiking-app-image-mancha">

                    <div class="layer portoflio-outer-layer contNube1" data-depth="0.3">
                        <img src="img/sections/nube1.png" class="portfolio-outer-image portfolio-hiking-app-image-nube1">
                    </div><!--layer-->

                    <!-- <div class="layer portoflio-outer-layer contNube1" data-depth="0.3"> -->
                    <img src="img/sections/grua.png" class="portfolio-outer-image portfolio-hiking-app-image-grua">
                    <!-- </div> --><!--layer-->

                    <div class="layer portoflio-outer-layer contNube2" data-depth="0.2">
                        <img src="img/sections/nube2.png" class="portfolio-outer-image portfolio-hiking-app-image-nube2">
                    </div><!--layer-->
                </div><!--scene-->

                <div class="row">
                    <a id="1" class="scroll-anchor"><img src="img/scroll.png" class="btn-scroll" alt=""></a>
                    <div id="contTitleHome" class="col-sm-8 parallax-enable">
                        <h3 class="header-supertext">LEVANTA UN</h3>
                        
                        <div class="flexslider">        
                            <ul class="slides"> 
                                <li>
                                    <h1 class="flexslider-heading">Emprendedor.</h1>
                                </li>
                                <li>
                                    <h1 class="flexslider-heading">Sueño.</h1>
                                </li>
                                <li>
                                    <h1 class="flexslider-heading">País.</h1>
                                </li>
                            </ul>
                        </div><!--flexslider-->
                        
                        <p class="description">
                            Emprendedores ayudan a emprendedores <b>Colaborando levantamos Chile</b> 
                            <br>
                            Ayudemos a cumplir los sueños de <b>emprendedores</b>
                        </p>

                        <button id="verVideo" class="btn col-xs-4 col-md-2">Ver Video</button>
                    </div><!--col-md-6-->
                </div><!--row-->	  
            </div><!--swiper-slide-->


            <div class="swiper-slide services service-slide">
                <div id="comoFunciona">
                    <div class="service-heading-container">
                        <h1 class="service-heading"><img src="img/sections/como-funciona.png" alt="Como Funciona"></h1>
                    </div><!--service-heading-container--> 

                    <div class="row">
                        <div id="infoServices" class="container">
                            <div class="col-xs-9 col-xs-offset-3">
                                <div class="col-sm-12">
                                    <p class="plus-red">+</p>

                                    <p class="description">Somos una plataforma de Crowdfunding Social. A través de Desafío Levantemos Chile, mas de 40 organizaciones pro emprendimiento tienen la misión es levantar el comercio en zonas de catástrofe. <br>La cual muestra distintos tipos de necesidades por emprendedores que se han visto afectados. Cada miembro tienen un rol importante ya que pueden hacer donaciones de todo tipo: desde una idea, dinero, pasando por productos, servicios y horas hombre, entre otras. Ha sido un trabajo colaborativo donde varias organizaciones han donado horas hombre de desarrollo y el compromiso de todas a ser parte, difundir y cooperar.</p>

                                    <div class="divider-white"></div>
                                </div>

                                <div class="contOpFunciona col-xs-4 text-center vertical-top">
                                    <img src="img/sections/entrar.png" alt="Entra al Proyecto">
                                    <h2 class="services-heading">Entra al Proyecto</h2>
                                </div><!--col-sm-4-->
                                
                                <div class="contOpFunciona col-xs-4 text-center vertical-top">
                                    <img src="img/sections/opciones.png" alt="Elige una opción o más de colaboración">
                                    <h2 class="services-heading">Elige una opción o más de colaboración</h2>
                                </div><!--col-sm-4-->
                                
                                <div class="contOpFunciona col-xs-4 text-center vertical-top">
                                    <img src="img/sections/colabora.png" alt="Colabora">
                                    <h2 class="services-heading">Colabora</h2>
                                </div><!--col-sm-4-->
                            </div>
                        </div><!--container-->
                    </div><!--row-->
                </div>
            </div><!--swiper-slide-->


            <div class="swiper-slide portfolio-slide">
                <div id="multiPlataforma">
                    <div class="portfolio-heading-container">
                        <h1 class="portfolio-heading"><img src="img/sections/multiplataforma.png" alt="Multiplataforma"></h1>
                        <h5 class="portfolio-subtext col-xs-6">Accede desde cualquier dispositivo móvil y podrás colaborar desde la comodidad de tu hogar.</h5>
                    </div><!--portfolio-heading-container--> 

                    <div id="scene2" class="scene">
                        <div class="layer portoflio-outer-layer contImac" data-depth="0.2">
                            <img src="img/sections/imac.png" class="portfolio-outer-image portfolio-hiking-app-image-imac">
                        </div><!--layer-->

                        <div class="layer portoflio-outer-layer contIphone" data-depth="0.3">
                            <img src="img/sections/iphone.png" class="portfolio-outer-image portfolio-hiking-app-image-iphone">
                        </div><!--layer-->
                    </div><!--scene2-->
                </div>
            </div><!--swiper-slide-->

        
            <div class="swiper-slide contact">
                <div id="levanta">
                    <div class="row center-vertically">
                        <div class="container">
                            <div class="col-xs-10 col-md-12 text-center">
                                <h5 class="contact-supertext">No manejamos el destino. El futuro SI.</h5>
                                <h1 class="contact-heading">Levanta un Emprendedor</h1>
                            </div><!--col-xs-10-->
                            
                            <div class="col-xs-12 col-md-12 text-center icon-container">                                
                                <div class="col-xs-1">
                                    <a href="" target="_blank">
                                        <span class="icon contact-icon icon-icon-facebook"></span>
                                    </a>
                                </div><!--col-xs-1-->

                                <div class="col-xs-1">
                                    <a href="" target="_blank">
                                        <span class="icon contact-icon icon-icon-twitter"></span>
                                    </a>
                                </div><!--col-xs-1-->
                            </div><!--col-xs-12-->
                        </div><!--container-->
                    </div><!--row-->
                </div>
            </div><!--swiper-slide-->

            <div class="swiper-slide services service-slide">
                <div id="titleColaboran" class="service-heading-container">
                    <h1 class="service-heading"><img src="<?php echo base_url(); ?>/img/sections/colaboran.png" alt="Colaboran"></h1>
                </div><!--service-heading-container--> 

                <div class="row">
                    <div id="infoColaboradores" class="container">
                        <div class="col-md-9 col-md-offset-3">
                            <img src="img/logos.png" alt="Colaboradores">
                        </div>
                    </div><!--container-->
                </div><!--row-->

                <footer id="footerHome">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <img src="<?php echo base_url(); ?>/img/sections/colabora.png" alt="Colabora">
                                <p>Diseñado con amor por <a href="http://www.wingzoft.com" target="_blank">Wingzoft</a></p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div><!--swiper-slide-->

        </div><!--swiper-wrapper-->
    </div><!--swiper-container-->

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>if (!window.jQuery) { document.write('<script src="js/jquery.min.js"><\/script>'); }</script>

    <!--bootstrap-->
    <script src="<?php echo base_url(); ?>/js/bootstrap.min.js"></script>
    <!--sidebar menu-->
    <script src="<?php echo base_url(); ?>/js/classie.js"></script>
    <script src="<?php echo base_url(); ?>/js/main3.js"></script>
    <!--swiper-->
    <script defer src="<?php echo base_url(); ?>/js/idangerous.swiper.min.js"></script>
    <!--flexslider-->
    <script src="<?php echo base_url(); ?>/js/jquery.flexslider-min.js"></script>
    <!--parallax-->
    <script src="<?php echo base_url(); ?>/js/parallax.js"></script>
    <!--custom-->
    <script src="<?php echo base_url(); ?>/js/main.js"></script>
</body>
</html>