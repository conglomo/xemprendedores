<article class='content'>
	<div id='contenido' class='width-65'>
		<h2 class='bg-base base align-center'>CONDICIONES DE USO</h2>
		<div id='condiciones'>
			<font class='color-base'>“Hola Hola”</font> es una actividad orientada a la construcción de redes entre las comunidades de La Higuera, a través de la publicación en distintos espacios públicos de la comuna, de mensajes provenientes de sus comunidades.<br /><br />
			Como tal, es una herramienta de comunicación con una interfaz de acceso público.<br /><br />
			Al enviar tu mensaje, estarás aceptando las siguientes condiciones de uso:<br /><br />
			<div style='font-family: "Platform-Regular"!important;'>
				• Las opiniones vertidas a través de esta plataforma son de exclusiva responsabilidad de quienes generen cada mensaje.<br />
				• Los mensajes enviados a través de esta plataforma contarán con la autorización para ser publicados exclusivamente en las pantallas que durante las fechas informadas serán instaladas en distintos espacios públicos de la Comuna de La Higuera.<br/>
				• Los mensajes que contengan lenguaje ofensivo, serán filtrados por el equipo de "Hola Hola".
			</div>
		</div>
	</div>
	<br class='clear' />
</article>