<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body style="font: 14px 'Arial', sans-serif; color: #333333">
        <div>
            <div style="background-color: rgb(31,91,97); text-align: right">
                <!-- <img src="cid:1001" alt="aa"/> -->
            </div>
            <div style="margin-left: 10px">
                <p style="text-align: left; margin-top: 50px;"><strong>Estimados,</strong></p>
                <!-- <h1 style="font-size: 24px; font-family: 'Arial', sans-serif;margin-top: 0px;">Nuevo evento creado</h1> -->
                <p>
                    Se ha ingresado una nueva donación:
                    <br/>
                    <br/>
                    <?php if ($monto!=null) { ?>
                       <strong>Monto donación:</strong> <?php echo $monto; ?>
                    <br>
                    <?php } ?>
                    <?php if ($materiales!=null) { ?>
                       <strong>Materiales donados:</strong> <?php echo $materiales; ?>
                    <br>
                    <?php } ?>
                    <?php if ($horas!=null) { ?>
                       <strong>Horas de trabajo donadas:</strong> <?php echo $horas; ?>
                    <br>
                    <?php } ?>
                    <?php if ($profesion!=null) { ?>
                       <strong>Profesión:</strong> <?php echo $profesion; ?>
                    <br>
                    <?php } ?>
                    <?php if ($idea!=null) { ?>
                       <strong>Idea Innovadora:</strong> <?php echo $idea; ?>
                    <br>
                    <?php } ?>
                    
                    <br/>
                    Nombre: <?php echo $nombre; ?>
                    <br/>
                    Rut: <?php echo $rut; ?>
                    <br/>
                    Email: <?php echo $email; ?>
                    <br/>
                    Teléfono: <?php echo $telefono; ?>
                    <br/>


                    Puedes ver esta donación <a href="http://qa.ibex.cl/emprendedores/manager">Aquí</a> 
                    <br/>
                    <!--<br/>Tú código de verifación es:<br/><br/>
                    <strong></strong> -->
                </p>
            </div>
            <div style="font-size: 11px; margin: 20px 0px; text-align: center; background-color: LightGray;">
                Este correo ha sido enviado de forma automatica. Por favor, no respondas a este mensaje. Si tienes alguna duda, favor comunicarse con soporte@ibex.cl.
            </div>
        </div>
    </body>
</html>