<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PorEmprendedores.org</title>
    
    <link rel="stylesheet" href="http://qa.ibex.cl/emprendedores/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://qa.ibex.cl/emprendedores/css/idangerous.swiper.css">
    <link rel="stylesheet" href="http://qa.ibex.cl/emprendedores/css/flexslider.css">
    <link rel="stylesheet" href="http://qa.ibex.cl/emprendedores/css/animate.css">
    <link rel="stylesheet" type="text/css" href="http://qa.ibex.cl/emprendedores/css/menu_elastic.css" />
    <link rel="stylesheet" href="http://qa.ibex.cl/emprendedores/css/main.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
    
    <!--sidebar menu-->
    <script src="js/snap.svg-min.js"></script>
</head>
<body id="home">
    <div id="preloader">
        <div id="status">
            <div id="loader"></div><!--loader-->
        </div><!--status-->
    </div><!--preloader-->
    
    <div class="menu-container hidden-lg">
        <div class="menu-wrap">
            <nav class="menu hidden-lg" id="slide-menu">
                <div class="icon-list">
                    <a class="navbar-brand sidebar-brand" href=""></a>
                    <a href="<?php echo base_url(); ?>"><span class="active-link icon-list-item">Inicio</span></a>
                    <a href="<?php echo base_url(); ?>/index.php/main/proyectos"><span class="icon-list-item">Proyecto</span></a>
                    <a href="#"><span class="icon-list-item">Noticias</span></a>
                    <a href="<?php echo base_url(); ?>/index.php/main/contacto"><span class="icon-list-item">Contacto</span></a>
                </div><!--icon-list-->
            </nav>
            
            <button class="close-button hidden-lg" id="close-button">Cerrar Menu</button>
            <div class="morph-shape" id="morph-shape" data-morph-open="M-1,0h101c0,0,0-1,0,395c0,404,0,405,0,405H-1V0z">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
                    <path d="M-1,0h101c0,0-97.833,153.603-97.833,396.167C2.167,627.579,100,800,100,800H-1V0z"/>
                </svg>
            </div><!--morph-shape-->
        </div><!--menu-wrap-->
    </div><!-- menu-container -->

    <nav id="staticBar" class="navbar">
        <div class="container">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header nav-transparent">
                    <button type="button" class="navbar-toggle collapsed menu-button" data-toggle="collapse" id="open-button" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                    <a class="navbar-brand" href="" id="0"></a>
                </div><!--navbar-header-->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right visible-lg" id="nav">
                        <li><a href="<?php echo base_url(); ?>" class="text-white">Inicio</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/main/proyectos" class="text-white">Proyecto</a></li>
                        <li><a href="#" class="text-white">Noticias</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/main/contacto" class="active-link text-white">Contacto</a></li>
                        <li id="iconSocialNav">
                            <a href="#" class="icoNav text-white"><img src="<?php echo base_url(); ?>/img/icons/facebook-head.png" alt=""></a>
                            <a href="#" class="icoNav text-white"><img src="<?php echo base_url(); ?>/img/icons/twitter-head.png" alt=""></a>
                        </li>
                    </ul>
                </div><!--navbar-collapse -->
            </div><!--container-fluid -->
        </div><!--container-->
    </nav>  
    
    <div class="swiper-container">
        <div class="swiper-wrapper" id="swiper-wrapper">
            <div class="swiper-slide services service-slide">
                <!-- <div id="contact-heading-container" class="service-heading-container">
                    <h1 class="service-heading"><img src="img/sections/contacto.png" alt="Contacto"></h1>
                </div> --><!--service-heading-container-->

                <div class="row">
                    <div id="infoContacto" class="container">
                        <div class="col-sm-6">
                            <h1 class="service-heading"><img src="<?php echo base_url(); ?>/img/sections/contacto.png" alt="Contacto"></h1>

                            <p>¿Quieres saber más de nosotros? Escríbenos a través de nuestro formulario de contacto o hablemos en nuestras redes sociales.</p>

                            <br>

                            <p>Síguenos:</p>

                            <ul id="contactSocial">
                                <li><a href="#" class="icoContact text-white"><img src="<?php echo base_url(); ?>/img/icons/facebook-contact.png" alt=""></a></li>
                                <li><a href="#" class="icoContact text-white"><img src="<?php echo base_url(); ?>/img/icons/twitter-contact.png" alt=""></a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <form action="">
                                <input type="text" id="nombre" class="form-control" placeholder="Nombre y Apellido" />
                                <input type="email" id="email" class="form-control" placeholder="Email" />
                                <input type="text" id="tema" class="form-control" placeholder="Tema" />
                                <textarea name="ayudar" id="" cols="20" rows="7" class="form-control" placeholder="¿Cómo te podemos ayudar?"></textarea>

                                <button type="button" id="sendContact">Enviar</button>
                            </form>
                        </div>
                    </div><!--container-->
                </div><!--row-->

                <footer>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <img src="<?php echo base_url(); ?>/img/sections/colabora.png" alt="Colabora">
                                <p>Diseñado con amor por <a href="http://www.wingzoft.com" target="_blank">Wingzoft</a></p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div><!--swiper-slide-->

        </div><!--swiper-wrapper-->
    </div><!--swiper-container-->

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>if (!window.jQuery) { document.write('<script src="<?php echo base_url(); ?>/js/jquery.min.js"><\/script>'); }</script>

    <!--bootstrap-->
    <script src="<?php echo base_url(); ?>/js/bootstrap.min.js"></script>
    <!--sidebar menu-->
    <script src="<?php echo base_url(); ?>/js/classie.js"></script>
    <script src="<?php echo base_url(); ?>/js/main3.js"></script>
    <!--swiper-->
    <script defer src="<?php echo base_url(); ?>/js/idangerous.swiper.min.js"></script>
    <!--flexslider-->
    <script src="<?php echo base_url(); ?>/js/jquery.flexslider-min.js"></script>
    <!--parallax-->
    <script src="<?php echo base_url(); ?>/js/parallax.js"></script>
    <!--custom-->
    <script src="<?php echo base_url(); ?>/js/main.js"></script>
</body>
</html>