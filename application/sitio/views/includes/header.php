<!DOCTYPE html>
<html lang='es_CL'>
	<head>
		<meta charset='UTF-8' />
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
		<title>Plataforma Emprendedores</title>
		<link rel='profile' href='http://gmpg.org/xfn/11'>
		<link rel='shortcut icon' href='../favicon.ico' />
		<link rel='stylesheet' media='all' href='<?php echo base_url('css/sitio/style.css'); ?>' />

		<script async src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700,600,800,400' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php echo base_url('css/sitio/bootstrap.min.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url('css/sitio/idangerous.swiper.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url('css/sitio/flexslider.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url('css/sitio/animate.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/sitio/menu_elastic.css'); ?>" />
		<link rel="stylesheet" href="<?php echo base_url('css/sitio/main.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url('css/sitio/font-awesome.min.css'); ?>">
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
		
		<!--sidebar menu-->
		<script src="<?php echo base_url('js/snap.svg-min.js'); ?>"></script>
	</head>
	<body>
		<div id='wrapper'>
			<header>
				<div id='header-page' class='content'>
					<ul>
						
					</ul>
				</div>
			</header>
			<div id='menu-movil' class='content'>
				<ul>
					
				</ul>
			</div>