<?php

class Usuarios extends CI_Controller {

    var $table_name = 'usuarios';
    var $helpers = array('form');
    var $libraries = array('form_validation');
    var $models = array('usuario_model', 'area_model', 'cargo_model', 'role_model');
    var $principal_model = 'usuario_model';
    var $view_viewAll = 'usuarios/view_all';
    var $view_view = 'usuarios/view';
    var $view_create = 'usuarios/create';
    var $view_update = 'usuarios/update';
    var $view_form = 'usuarios/_form';
    var $name = 'Usuarios';

    function __construct() {
        parent::__construct();
        $this->load->helper($this->helpers);
        $this->load->library($this->libraries);
        $this->load->model($this->models);
    }

    function view_all() {

        if ($this->session->userdata('usrid')) {

            $get_all = $this->usuario_model->view_all($this->table_name);

            $data = array(
                'view' => $this->view_viewAll,
                'get_all' => $get_all,
            );

            $this->load->view('layouts/template', $data);
        } else {
            redirect("site/logout");
        }
    }

    function view() {

        if ($this->session->userdata('usrid')) {

            $id = $this->uri->segment(3);

            $view = $this->usuario_model->view($this->table_name, $id);

            foreach ($view as $v) {
                $id = $v->usr_id;
                $nombre = $v->usr_nombre;
                $apellidos = $v->usr_apellidos;
                $empresa = $v->usr_empresa;
                $cargo = $v->cargo_nombre_cargo;
                $area = $v->area_nombre_area;
                $role = $v->role_nombre_rol;
                $username = $v->usr_username;
            }

            $data = array(
                'view' => $this->view_view,
                'name' => $this->name,
                'id' => $id,
                'nombre' => $nombre,
                'apellidos' => $apellidos,
                'empresa' => $empresa,
                'cargo' => $cargo,
                'area' => $area,
                'role' => $role,
                'username' => $username,
            );

            $this->load->view('layouts/template', $data);
        } else {
            redirect("site/logout");
        }
    }

    function create() {

        if ($this->session->userdata('usrid')) {

            $this->form_validation->set_rules('input_nombre', 'Nombre', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_apellidos', 'Apellidos', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_email', 'Email', 'trim|required|valid_email');
            
            
            $nombre = $this->input->post('input_nombre');
            $apellidos = $this->input->post('input_apellidos');
            $empresa = $this->input->post('input_empresa');
            $cargo = $this->input->post('input_cargo');
            $area = $this->input->post('input_area');
            $role = $this->input->post('input_role');
            $username = $this->input->post('input_username');
            $password = $this->input->post('input_password');
            $email = $this->input->post('input_email');
            
            $data = array(
                'view' => 'usuarios/_form',
                'nombre' => $nombre,
                'apellidos' => $apellidos,
                'empresa' => $empresa,
                'cargo' => $cargo,
                'area' => $area,
                'role' => $role,
                'username' => $username,
                'areas' => $this->area_model->view_all('area'),
                'cargos' => $this->cargo_model->view_all('cargos'),
                'roles' => $this->role_model->view_all('roles'),
                'email' => $email
            );

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('layouts/template', $data);
            } else {
                $validate_username = $this->usuario_model->validate_username($this->table_name, $username);

                switch ($validate_username) {
                    case 0:
                        $create = $this->usuario_model->create($this->table_name, $nombre, $apellidos, $empresa, $cargo, $area, $role, $username, $password, $email);

                        if ($create == FALSE) {
                            echo '<script>alert("No se ha podido crear el registro");</script>';
                            redirect("usuarios/view_all");
                        } else {
                            echo '<script>alert("Registro creado correctamente");</script>';
                            redirect("usuarios/view_all");
                        }
                        break;
                    case 1:

                        $this->session->set_flashdata("username_exist", "Usuario ya existe");

                        $this->load->view('layouts/template', $data);
                        break;
                }
            }
        } else {
            redirect("site/logout");
        }
    }

    function update() {

        if ($this->session->userdata('usrid')) {

            $id_update = $this->uri->segment(3);
            $view = $this->usuario_model->view($this->table_name, $id_update);

            $this->form_validation->set_rules('input_nombre', 'Nombre', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_apellidos', 'Apellidos', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_empresa', 'Empresa', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_cargo', 'Cargo', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_area', 'Área', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_role', 'Rol', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_username', 'Username', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_email', 'Email', 'trim|required|valid_email');
            
            $id = $this->input->post('input_id');
            $nombre = $this->input->post('input_nombre');
            $apellidos = $this->input->post('input_apellidos');
            $empresa = $this->input->post('input_empresa');
            $cargo = $this->input->post('input_cargo');
            $area = $this->input->post('input_area');
            $role = $this->input->post('input_role');
            $username = $this->input->post('input_username');
            $password = $this->input->post('input_password');
            $email = $this->input->post('input_email');

            if ($this->form_validation->run() == FALSE) {
                foreach ($view as $v) {
                    $id = $v->usr_id;
                    $nombre = $v->usr_nombre;
                    $apellidos = $v->usr_apellidos;
                    $empresa = $v->usr_empresa;
                    $cargo = $v->usr_cargo;
                    $role = $v->usr_role;
                    $area = $v->usr_area;
                    $username = $v->usr_username;
                    $password = $v->usr_password;
                    $email = $v->usr_email;
                }

                $data = array(
                    'view' => $this->view_form,
                    'id' => $id,
                    'nombre' => $nombre,
                    'apellidos' => $apellidos,
                    'empresa' => $empresa,
                    'cargo_id' => $cargo,
                    'role_id' => $role,
                    'area_id' => $area,
                    'username' => $username,
                    'password' => $password,
                    'areas' => $this->area_model->view_all('area'),
                    'cargos' => $this->cargo_model->view_all('cargos'),
                    'roles' => $this->role_model->view_all('roles'),
                    'email' => $email
                );

                $this->load->view('layouts/template', $data);
            } else {
                $update = $this->usuario_model->update($this->table_name, $id, $nombre, $apellidos, $empresa, $cargo, $area, $password, $username, $role, $email);

                if ($update == FALSE) {
                    echo '<script>alert("No se ha realizado la actualización, posiblemente no hayan datos a actualizar, o ha existido un error inesperado");</script>';
                    //$this->view_all();
                    redirect("usuarios/view_all");
                } else {
                    echo '<script>alert("Registro Actualizado correctamente");</script>';
                    //$this->view_all();
                    redirect("usuarios/view_all");
                }
            }
        } else {
            redirect("site/logout");
        }
    }

    function delete() {
        $id = $this->uri->segment(3);

        $delete = $this->usuario_model->delete($this->table_name, $id);
    }

    function generar_pdf() {

        if (!is_dir("./files")) {
            mkdir("./files", 0777);
            mkdir("./files/pdfs", 0777);
        }

        $id = $this->uri->segment(3);
        $usuario = $this->usuario_model->view($this->table_name, $id);

        foreach ($usuario as $user) {
            $nombre = $user->usr_nombre . " " . $user->usr_apellidos;
            $nombre2 = $user->usr_nombre . "_" . $user->usr_apellidos;
            $empresa = $user->usr_empresa;
            $cargo = $user->cargo_nombre_cargo;
            $area = $user->area_nombre_area;
            $rol = $user->role_nombre_rol;
        }

        //Load the library
        $this->load->library('html2pdf');

        //Set folder to save PDF to
        $this->html2pdf->folder('./files/pdfs/');

        //Set Filename
        $this->html2pdf->filename('usuario_perfil.pdf');

        //establecemos el tipo de papel
        $this->html2pdf->paper('a4', 'portrait');

        //datos que queremos enviar a la vista, lo mismo de siempre
        $data = array(
            'title' => 'PDF Created',
            'view' => 'usuarios/view',
            'nombre' => $nombre,
            'empresa' => $empresa,
            'cargo' => $cargo,
            'area' => $area,
            'rol' => $rol,
            'id' => $id
        );

        //Load html view
        $this->html2pdf->html(utf8_decode($this->load->view('usuarios/reporte_pdf', $data, true)));

        //si el pdf se guarda correctamente lo mostramos en pantalla
        if ($this->html2pdf->create('save')) {
            if (is_dir("./files/pdfs")) {            
                $filename =  'usuario_perfil.pdf';
                //rute completa al archivo
                //$route = base_url("files/pdfs/usuario_perfil.pdf");
                $route = "./files/pdfs/" . $filename;
                if (file_exists("./files/pdfs/" . $filename)) {
                    header('Content-type: application/pdf');
                    readfile($route);
                }
            }
        }

        function check_equal_less($second_field, $first_field) {
            if ($second_field <= $first_field) {
                $this->form_validation->set_message('check_equal_less', 'Hora de término no puede ser menor que hora de inicio.');
                return false;
            } else {
                return true;
            }
        }

    }

}