<?php

/**
 * Description of formularios
 *
 * @author nicolas
 */
class Formularios extends CI_Controller {

    var $table_name = 'repin___hecho_reporte';
    var $helpers = array('form');
    var $libraries = array('form_validation', 'phpexcel/PHPExcel');
    var $models = array('formulario_model');
    var $principal_model = 'formulario_model';
    var $view_viewAll = 'formularios/view_all';
    var $view_view = 'formularios/view';
    var $view_create = 'formularios/create';
    var $view_update = 'formularios/update';
    var $view_form = 'formularios/_form';
    var $name = 'Formulario';

    function __construct() {
        parent::__construct();
        $this->load->helper($this->helpers);
        $this->load->library($this->libraries);
        $this->load->model($this->models);
    }

    function view_all() {
        if ($this->session->userdata('usrid')) {

            $get_all = $this->formulario_model->view_all();

            $data = array(
                'view' => $this->view_viewAll,
                'get_all' => $get_all,
            );

            $this->load->view('layouts/template', $data);
        } else {
            redirect("site/logout");
        }
    }
    
    public function hola(){
        $this->output->enable_profiler(TRUE);
        $id = $this->uri->segment(3);
        var_dump($id);
        $view = $this->formulario_model->view($id);
        var_dump($view);
    }

    function view() {
        if ($this->session->userdata('usrid')) {

            $id = $this->uri->segment(3);

            $formulario = $this->formulario_model->view($id);
            $files = $this->formulario_model->getFiles($id);

            $data = array(
                'view' => $this->view_view,
                'formulario' => $formulario,
                'files' => $files
            );

            $this->load->view('layouts/template', $data);
        } else {
            redirect("site/logout");
        }
    }

    function generar_pdf() {

        if (!is_dir("./files")) {
            mkdir("./files", 0777);
            mkdir("./files/pdfs", 0777);
        }

        $id = $this->uri->segment(3);
        $formulario = $this->formulario_model->view($id);
        $files = $this->formulario_model->getFiles($id);

        //Load the library
        $this->load->library('html2pdf');

        //Set folder to save PDF to
        $this->html2pdf->folder('./files/pdfs/');

        //Set Filename
        $this->html2pdf->filename('formularios.pdf');

        //establecemos el tipo de papel
        $this->html2pdf->paper('a4', 'portrait');

        //datos que queremos enviar a la vista, lo mismo de siempre
        $data = array(
            'title' => 'PDF Created',
            'view' => 'formularios/view',
            'formulario' => $formulario,
            'files' => $files
        );

        //Load html view
        $this->html2pdf->html(utf8_decode($this->load->view('formularios/reporte_pdf', $data, true)));

        //si el pdf se guarda correctamente lo mostramos en pantalla
        if ($this->html2pdf->create('save')) {
            if (is_dir("./files/pdfs")) {
                $filename = 'formularios.pdf';
                $route = "./files/pdfs/" . $filename;
                if (file_exists("./files/pdfs/" . $filename)) {
                    header('Content-type: application/pdf');
                    readfile($route);
                } else {
                    echo 'error file_exists';
                }
            }else{
                echo "error is_dir";
            }
        }else{
            echo 'error create_save';
        }
    }

    function check_equal_less($second_field, $first_field) {
        if ($second_field <= $first_field) {
            $this->form_validation->set_message('check_equal_less', 'Hora de término no puede ser menor que hora de inicio.');
            return false;
        } else {
            return true;
        }
    }
    
    function export_excel(){
        // configuramos las propiedades del documento
        $this->phpexcel->getProperties()
                ->setCreator("Arkos Noem Arenom")
                ->setLastModifiedBy("Arkos Noem Arenom")
                ->setTitle("Office 2007 XLSX Test Document")
                ->setSubject("Office 2007 XLSX Test Document")
                ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                ->setKeywords("office 2007 openxml php")
                ->setCategory("Formulario File");
         
         
        // Cabeceras
        $this->phpexcel->setActiveSheetIndex(0);
        $this->phpexcel->getActiveSheet()->getCell('A1')->setValue('ID');
        $this->phpexcel->getActiveSheet()->getCell('B1')->setValue('Creador');
        $this->phpexcel->getActiveSheet()->getCell('C1')->setValue('Fecha Creación');
        $this->phpexcel->getActiveSheet()->getCell('D1')->setValue('Tipo de Evento');
        $this->phpexcel->getActiveSheet()->getCell('E1')->setValue('Tipo de Incidente');
        $this->phpexcel->getActiveSheet()->getCell('F1')->setValue('Fecha Evento');
        $this->phpexcel->getActiveSheet()->getCell('G1')->setValue('Hora Evento');
        $this->phpexcel->getActiveSheet()->getCell('H1')->setValue('Clasificación Incidente');
        $this->phpexcel->getActiveSheet()->getCell('I1')->setValue('Centro de Trabajo');
        $this->phpexcel->getActiveSheet()->getCell('J1')->setValue('Área');
        $this->phpexcel->getActiveSheet()->getCell('K1')->setValue('Lugar Específico');
        $this->phpexcel->getActiveSheet()->getCell('L1')->setValue('Descripción del Incidente');
        $this->phpexcel->getActiveSheet()->getCell('M1')->setValue('Testigo 1');
        $this->phpexcel->getActiveSheet()->getCell('N1')->setValue('Testigo 2');
        $this->phpexcel->getActiveSheet()->getCell('O1')->setValue('Testigo 3');
        $this->phpexcel->getActiveSheet()->getCell('P1')->setValue('Nombre Persona');
        $this->phpexcel->getActiveSheet()->getCell('Q1')->setValue('Rut Persona');
        $this->phpexcel->getActiveSheet()->getCell('R1')->setValue('Sexo');
        $this->phpexcel->getActiveSheet()->getCell('S1')->setValue('Edad');
        $this->phpexcel->getActiveSheet()->getCell('T1')->setValue('BP');
        $this->phpexcel->getActiveSheet()->getCell('U1')->setValue('Razón Social Persona');
        $this->phpexcel->getActiveSheet()->getCell('V1')->setValue('Lesión Aparente');
        $this->phpexcel->getActiveSheet()->getCell('W1')->setValue('Parte Lesionada');
        $this->phpexcel->getActiveSheet()->getCell('X1')->setValue('Área Persona');
        $this->phpexcel->getActiveSheet()->getCell('Y1')->setValue('Jefe Directo Persona');
        $this->phpexcel->getActiveSheet()->getCell('Z1')->setValue('Responsable Área');
        $this->phpexcel->getActiveSheet()->getCell('AA1')->setValue('Turno Persona');
        $this->phpexcel->getActiveSheet()->getCell('AB1')->setValue('Licencias. cursos o habilitaciones específicas requeridas');
        $this->phpexcel->getActiveSheet()->getCell('AC1')->setValue('Proceso');
        $this->phpexcel->getActiveSheet()->getCell('AD1')->setValue('Actividad Genérica');
        $this->phpexcel->getActiveSheet()->getCell('AE1')->setValue('Frecuencia Tarea');
        $this->phpexcel->getActiveSheet()->getCell('AF1')->setValue('Maquinarias');
        $this->phpexcel->getActiveSheet()->getCell('AG1')->setValue('Equipos');
        $this->phpexcel->getActiveSheet()->getCell('AH1')->setValue('Herramientas');
        $this->phpexcel->getActiveSheet()->getCell('AI1')->setValue('Materiales');
        $this->phpexcel->getActiveSheet()->getCell('AJ1')->setValue('Condiciones Climáticas Ambiente');
        $this->phpexcel->getActiveSheet()->getCell('AK1')->setValue('Lugar de Trabajo');
        $this->phpexcel->getActiveSheet()->getCell('AL1')->setValue('Actos');
        $this->phpexcel->getActiveSheet()->getCell('AM1')->setValue('Condiciones');
        $this->phpexcel->getActiveSheet()->getCell('AN1')->setValue('Latitud');
        $this->phpexcel->getActiveSheet()->getCell('AO1')->setValue('Longitud');
        $this->phpexcel->getActiveSheet()->getCell('AP1')->setValue('Dirección');
 
        // Fetching the table data
        $rowNumber =2; //start in cell 1
        $data = $this->formulario_model->view_all_excel();
            foreach ($data as $row) {
                $col = 'A'; // start at column A
                foreach ($row as $cell) {
                    $this->phpexcel->getActiveSheet()->setCellValue($col . $rowNumber, $cell);
                    $col++;
                }
                $rowNumber++;
            }
 
         
        // Renombramos la hoja de trabajo
        $this->phpexcel->getActiveSheet()->setTitle('Formularios');
         
         
        // configuramos el documento para que la hoja
        // de trabajo número 0 sera la primera en mostrarse
        // al abrir el documento
        $this->phpexcel->setActiveSheetIndex(0);
         
         
        // redireccionamos la salida al navegador del cliente (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="formulario.xlsx"');
        header('Cache-Control: max-age=0');
         
        $objWriter = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
        $objWriter->save('php://output');
    }
}

/* End of file formularios.php */
/* Location: ./application/controllers/formularios.php */