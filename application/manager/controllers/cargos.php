<?php

class Cargos extends CI_Controller {

    var $table_name = 'cargos';
    
    var $helpers = array('form');
    var $libraries = array('form_validation');
    var $models = array('cargo_model');
    var $principal_model = 'cargo_model';
    
    var $view_viewAll = 'cargos/view_all';
    var $view_view = 'cargos/view';
    var $view_create = 'cargos/create';
    var $view_update = 'cargos/update';
    var $view_form = 'cargos/_form';
    
    var $name = 'Cargos';
    
    function __construct() {
        parent::__construct();
        $this->load->helper($this->helpers);
        $this->load->library($this->libraries);
        $this->load->model($this->models);
    }

    function view_all() {
        if($this->session->userdata('usrid')){
            
            $get_all = $this->cargo_model->view_all($this->table_name);

            $data = array(
                'view' => $this->view_viewAll,
                'get_all' => $get_all,
            );

            $this->load->view('layouts/template', $data);

        }else{
            redirect("site/logout");
        }
    }
    
    function view() {
        if($this->session->userdata('usrid')){
            
            $id = $this->uri->segment(3);

            $view = $this->cargo_model->view($this->table_name, $id);

            foreach ($view as $v) {
                $id = $v->cargo_id;
                $nombre = $v->cargo_nombre_cargo;
            }

            $data = array(
                'view' => $this->view_view,
                'name' => $this->name,
                'id' => $id,
                'nombre' => $nombre,
            );

            $this->load->view('layouts/template', $data);
            
        }else{
            redirect("site/logout");
        }
    }
    
    function create() {
        if($this->session->userdata('usrid')){
            $this->form_validation->set_rules('input_nombre', 'Nombre', 'trim|required|xss_clean');

            $nombre = $this->input->post('input_nombre');

            $data = array(
                'view' => 'cargos/_form',
            );

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('layouts/template', $data);
            } else {
                $create = $this->cargo_model->create($this->table_name, $nombre);

                if ($create == FALSE) {
                    echo '<script>alert("No se ha podido crear el registro");</script>';
                    $this->view_all();
                } else {
                    echo '<script>alert("Registro creado correctamente");</script>';
                    $this->view_all();
                }
            }

        }else{
            redirect("site/logout");
        }
    }
    
    function update() {        
        if($this->session->userdata('usrid')){
        
            $id_update = $this->uri->segment(3);
        
            $view = $this->cargo_model->view($this->table_name, $id_update);

            $this->form_validation->set_rules('input_nombre', 'Nombre', 'trim|required|xss_clean');

            $id = $this->input->post('input_id');
            $nombre = $this->input->post('input_nombre');

            if ($this->form_validation->run() == FALSE) {
                        
                foreach ($view as $v) {
                    $id = $v->cargo_id;
                    $nombre = $v->cargo_nombre_cargo;
                }

                $data = array(
                    'view' => $this->view_form,
                    'id' => $id,
                    'nombre' => $nombre,
                );

                $this->load->view('layouts/template', $data);

            } else {
                $update = $this->cargo_model->update($this->table_name, $id, $nombre);

                if ($update == FALSE) {
                    echo '<script>alert("No se ha realizado la actualización, posiblemente no hayan datos a actualizar, o ha existido un error inesperado");</script>';
                    $this->view_all();
                } else {
                    echo '<script>alert("Registro Actualizado correctamente");</script>';
                    $this->view_all();
                }
            }
            
        }else{
            redirect("site/logout");
        }
    }

    function delete() {
        $id = $this->uri->segment(3);

        $delete = $this->cargo_model->delete($this->table_name,$id);
    }

    function generar_pdf() {

        $id_evento = $this->uri->segment(3);
        $evento = $this->agenda_model->verEvento($id_evento);

        foreach ($evento as $ev) {
            $id = $ev->agen_id;
            $fecha = $ev->agen_fecha;
            $inicio = $ev->agen_hora_inicio;
            $fin = $ev->agen_hora_fin;
            $contacto = $ev->agen_contacto_area;
            $descripcion = $ev->agen_descripcion;
            $usuario = $ev->usr_nombre;
            $areas = $ev->area_nombre_area;

            if ($ev->agen_comentarios) {
                $comentarios = $ev->agen_comentarios;
            } else {
                $comentarios = "Sin Comentarios";
            }
        }

        //Load the library
        $this->load->library('html2pdf');

        //Set folder to save PDF to
        $this->html2pdf->folder('./files/pdfs/');

        //Set Filename
        $this->html2pdf->filename('test.pdf');

        //establecemos el tipo de papel
        $this->html2pdf->paper('a4', 'portrait');

        //datos que queremos enviar a la vista, lo mismo de siempre
        $data = array(
            'title' => 'PDF Created',
            'view' => 'agenda/ver_evento',
            'evento' => $evento,
            'id' => $id,
            'fecha' => $fecha,
            'inicio' => $inicio,
            'fin' => $fin,
            'contacto' => $contacto,
            'descripcion' => $descripcion,
            'usuario' => $usuario,
            'area' => $areas,
            'comentarios' => $comentarios,
        );

        //Load html view
        $this->html2pdf->html($this->load->view('agenda/reporte_pdf', $data, true));

        //si el pdf se guarda correctamente lo mostramos en pantalla
        if ($this->html2pdf->create('save')) {
            if (is_dir("./files/pdfs")) {
                $filename = "test.pdf";
                $route = base_url("files/pdfs/test.pdf");
                if (file_exists("./files/pdfs/" . $filename)) {
                    header('Content-type: application/pdf');
                    readfile($route);
                }
            }
        }

        function check_equal_less($second_field, $first_field) {
            if ($second_field <= $first_field) {
                $this->form_validation->set_message('check_equal_less', 'Hora de término no puede ser menor que hora de inicio.');
                return false;
            } else {
                return true;
            }
        }

    }

}

/* End of file cargos.php */
/* Location: ./application/controllers/cargos.php */