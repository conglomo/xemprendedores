<?php

class Documentos extends CI_Controller {

    var $table_name = 'documentos';
    var $helpers = array('form');
    var $libraries = array('form_validation');
    var $models = array('documentos_model');
    var $principal_model = 'documentos_model';
    var $view_viewAll = 'documentos/view_all';
    var $view_view = 'documentos/view';
    var $view_create = 'documentos/create';
    var $view_update = 'documentos/update';
    var $view_form = 'documentos/_form';
    var $name = 'Documentos';

    function __construct() {
        parent::__construct();
        $this->load->helper($this->helpers);
        $this->load->library($this->libraries);
        $this->load->model($this->models);
    }

    function view_all() {
        if($this->session->userdata('usrid')){
            
            $get_all = $this->documentos_model->view_all($this->table_name);

            $data = array(
                'view' => $this->view_viewAll,
                'get_all' => $get_all,
            );

            $this->load->view('layouts/template', $data);
            
        }else{
            redirect("site/logout");
        }
    }

    function view() {
        if($this->session->userdata('usrid')){
            
            $id = $this->uri->segment(3);

            $view = $this->documentos_model->view($this->table_name, $id);

            foreach ($view as $v) {
                $id = $v->doc_id;
                $nombre = $v->doc_nombre;
                $descripcion = $v->doc_descripcion;
            }

            $data = array(
                'view' => $this->view_view,
                'name' => $this->name,
                'id' => $id,
                'nombre' => $nombre,
                'descripcion' => $descripcion
            );

            $this->load->view('layouts/template', $data);
        
        }else{
            redirect("site/logout");
        }
    }

    function create() {
        if($this->session->userdata('usrid')){
            
            $this->form_validation->set_rules('input_nombre', 'Nombre', 'trim|required|xss_clean');
            //$this->form_validation->set_rules('input_titulo', 'Título', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_descripcion', 'Descripción', 'trim|required|xss_clean');

            $nombre = $this->input->post('input_nombre');
            $descripcion = $this->input->post('input_descripcion');
            $file = $this->input->post('userfile');

            $data = array(
                'view' => 'documentos/_form',
            );

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('layouts/template', $data);
            } else {
                $upload_doc = $this->do_upload($file);
                if ($upload_doc == false) {
                    echo '<script>alert("Error al subir imagen");</script>';
                    //echo $upload_doc;
                    //echo "error al subir imagen";
                } else {
                    $create = $this->documentos_model->create($this->table_name, $nombre, $descripcion, base_url("documentos/biblioteca/"). "/" . $upload_doc["file_name"]);

                    if ($create == FALSE) {
                        echo '<script>alert("No se ha podido crear el registro");</script>';
                        //$this->view_all();
                        redirect("documentos/view_all");
                    } else {
                        $data = array(
                            $message => '<script>alert("Registro creado correctamente");</script>',
                        );
                        //$this->view_all();
                        redirect("documentos/view_all", $data);
                    }
                }
            }
        
        }else{
            redirect("site/logout");
        }
    }
    
    function update() {        
        if($this->session->userdata('usrid')){
        
            $id = $this->input->post('input_id');
            $nombre = $this->input->post('input_nombre');
            $descripcion = $this->input->post('input_descripcion');
            $file = $this->input->post('userfile');
            
            $id_update = $this->uri->segment(3);
            
            $view = $this->documentos_model->view($this->table_name, $id_update);
            $this->form_validation->set_rules('input_nombre', 'Nombre', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_descripcion', 'Descripción', 'trim|required|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                foreach ($view as $v) {
                    $id = $v->doc_id;
                    $nombre = $v->doc_nombre;
                    $descripcion = $v->doc_descripcion;
                    $file = $v->doc_path;
                }
                
                $data = array(
                    'view' => $this->view_form,
                    'id' => $id,
                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                    'file' => $file
                );

                $this->load->view('layouts/template', $data);

            } else {
                if (!empty($_FILES['userfile']['name'])){
                    $upload_doc = $this->do_upload($file);
                    if ($upload_doc == false){
                        echo '<script>alert("Error al subir archivo.");</script>';
                        redirect(current_url());
                    }
                    else {
                        $update = $this->documentos_model->update($this->table_name, $id, $nombre, $descripcion, base_url("documentos/biblioteca/"). "/" . $upload_doc["file_name"]);
                    }
                }
                else {
                    $update = $this->documentos_model->update($this->table_name, $id, $nombre, $descripcion, null);
                }
                //$update = $this->documentos_model->update($this->table_name, $id, $nombre, $descripcion);

                if ($update == FALSE) {
                    echo utf8_decode('<script>alert("No se ha realizado la actualización, posiblemente no hayan datos a actualizar, o ha existido un error inesperado");</script>');
                    ///$this->view_all();
                    redirect("documentos/view_all");
                } else {
                    echo '<script>alert("Registro actualizado correctamente!");</script>';
                    //$this->view_all();
                    redirect("documentos/view_all");
                }
            }
        
        }else{
            redirect("site/logout");
        }
    }

    function delete() {
        $id = $this->uri->segment(3);

        $delete = $this->documentos_model->delete($this->table_name, $id);
    }

    private function do_upload($file) {

        //$file = $this->input->post('userfile');

        if (!is_dir("./documentos/")) {
            mkdir("./documentos/", 0777, TRUE);
        }

        if (!is_dir("./documentos/biblioteca")) {
            mkdir("./documentos/biblioteca", 0777, TRUE);
        }

        $config['upload_path'] = './documentos/biblioteca';
        $config['allowed_types'] = 'pdf|doc|docx';
        $config['max_size'] = '10240';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);
        //$this->upload->initialize($config);

        if (!$this->upload->do_upload()) {
            echo $this->upload->display_errors();
            return false;
        } else {
            //$data = array('upload_data' => $this->upload->data());
            $data = $this->upload->data();
            //$filepath = $data["full_path"];
            //print_r($data);

            return $data;
            //$this->load->view('upload_success', $data);
        }
    }

}

/* End of file documentos.php */
/* Location: ./application/controllers/documentos.php */