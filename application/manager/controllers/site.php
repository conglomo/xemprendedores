<?php

/**
 * Description of site
 *
 * @author nicolas
 */
class site extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper("form");
        $this->load->library(array("form_validation", "session"));
        $this->load->model("site_model");
        $this->load->model('m_main', 'main');
        $this->load->model("agenda_model");
    }

    function index() {
        
        
            $this->login();
       
        
    }

    function login() {
        $this->form_validation->set_rules('username', 'Usuario', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Contraseña', 'trim|required|xss_clean');

        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/head');
            $this->load->view("site/login");
            $this->load->view("layouts/footer");
        } else {
            $validate = $this->site_model->validate_user($username, $password);
            
            if ($validate == FALSE) {
                $this->session->set_flashdata('error_validate', 'Usuario y/o contraseña incorrectos.');
                redirect("site/login");
            } else {
                foreach($validate as $val_user){  
                    $this->session->set_userdata("usrid", $val_user->usr_id);
                    $this->session->set_userdata("usrrole", $val_user->usr_role);
                    $this->session->set_userdata("usrnombre", $val_user->usr_nombre);
                }
                //echo $this->db->last_query();
                
                    redirect("site/inicio");
                
            }
        }
    }
    
    function logout(){
        $this->session->sess_destroy();
        redirect("site/login");
    }

    function inicio() {
       

        

        $formularios = $this->site_model->ver_formularios();
        $data = array(
            'formularios' => $formularios,
            
        );
        
        
        
        $this->load->view('layouts/head');
        $this->load->view("site/menu", $data);
        $this->load->view("layouts/footer");
        
    }

     function ver_formulario() {
       

        $id = $this->uri->segment(3);

        $formulario = $this->site_model->verFormulario($id);
        $data = array(
            'formulario' => $formulario,
            
        );
        
        
        
        $this->load->view('layouts/head');
        $this->load->view("site/formulario", $data);
        $this->load->view("layouts/footer");
        
    }

    function papeleta() {
       

        

        $query = $this->main->obtenerLocalidades();
        
        if ($query != false)
        {
            $data['ciudades'] = $query;
        }
        else
        {

        }

        
        
        
        $this->load->view('layouts/head');
        $this->load->view("site/menu_papeleta", $data);
        $this->load->view("layouts/footer");
        
    }

    function pizarra() {
       

        

        $query = $this->main->obtenerLocalidades();
        
        if ($query != false)
        {
            $data['ciudades'] = $query;
        }
        else
        {

        }

        
        
        
        $this->load->view('layouts/head');
        $this->load->view("site/menu_pizarra", $data);
        $this->load->view("layouts/footer");
        
    }

    function administracion() {

        $query = $this->main->obtenerLocalidades();

        //$query2 = $this->main->obtenerMensajes();
        
        if ($query != false)
        {
            $data['ciudades'] = $query;
            //$data['mensajes'] = $query2;
        }
        else
        {

        }
 
        $this->load->view('layouts/head');
        $this->load->view("site/admin", $data);
        $this->load->view("layouts/footer");
        
    }

    public function crear_mensaje()
    {
        $tipo = $this->input->post('tipo');
        $pregunta = $this->input->post('pregunta1');
        $origen = $this->input->post('origen');
        $destino = $this->input->post('destino');
        $tipo= 0;

        if ($pregunta!=0 || $pregunta!= NULL) {

            $destino=$origen; 
            $tipo= 1;
        }
        

        $datos = array(
            'input_origen' => $this->input->post('origen'),
            'input_destino' => $destino,
            'input_mensaje' => $this->input->post('mensaje'),
            'input_pregunta' => $pregunta,
            'input_tipo' => $tipo,
            'input_estado' => 0
        );

        $query = $this->main->crearMensaje($datos);

        if ($query)
        {
            
            $this->administracion();
        } else {
                    echo '<script>alert("Registro Creado incorrectamente");</script>';
                    $this->administracion();
                }
        
        
    }

    public function publicar_mensaje()
    {
        
        $id_mensaje = $this->uri->segment(3);
        $estado = $this->uri->segment(4);

        $datos = array(
            
            'input_id' => $id_mensaje,
            'input_estado' => $estado
        );

        $query = $this->main->publicarMensajes($datos);

        if ($query)
        {
            //echo '<script>alert("Registro Actualizado correctamente");</script>';
            //$this->administracion();
            redirect("site/mensajes/0/0/5");
        } else {
                    echo '<script>alert("Registro Actualizado incorrectamente");</script>';
                    redirect("site/mensajes/0/0/5");
                }
        
        
    }

    public function republicar()
    {
        $origen = $this->uri->segment(3);
        $destino = $this->uri->segment(4);
        $mensaje = $this->uri->segment(5);
        $pregunta = $this->uri->segment(6);

        $datos = array(
            'input_origen' => $origen,
            'input_destino' => $destino,
            'input_mensaje' => $mensaje,
            'input_pregunta' => $pregunta,
            'input_estado' => 1
        );

        $query = $this->main->crearMensaje($datos);

        if ($query)
        {
            //echo '<script>alert("Mensaje Republicado  correctamente");</script>';
            redirect("site/mensajes/0/0/5");
        } else {
                    echo '<script>alert("Mensaje Republicado incorrectamente");</script>';
                    redirect("site/mensajes/0/0/5");
                }
        
        
    }

    public function ver_mensajes()
    {
       
       
        
       
        
         $datos = array(
            'input_origen' => $this->input->post('origen'),
            'input_destino' => $this->input->post('destino')
            
        );

        $origen = $this->input->post('origen');    
        $destino = $this->input->post('destino');
        $tipo_vista = $this->input->post('tipo_vista');
        $query = $this->main->obtenerMensajes($datos);

        if ($query)
        {
            redirect("site/mensajes/" . $origen . "/" . $destino . "/" . $this->input->post('tipo_vista'));
        }else{
            echo '<script>alert("No existen mensajes entre estas ciudades");</script>';
            $this->administracion();
        }
        
        
    }

    function mensajes() {

        $query1 = $this->main->obtenerLocalidades();
        $origen = $this->uri->segment(3);
        $destino = $this->uri->segment(4);
        $tipo_vista = $this->uri->segment(5);
        $datos = array(
            'input_origen' => $origen,
            'input_destino' => $destino
            
        );    
            

        $query = $this->main->obtenerMensajes($datos);

        //$query2 = $this->main->obtenerMensajes();
        
        if ($query != false)
        {
            $data['mensajes'] = $query;
            $data['ciudades'] = $query1;
            $data['origen'] = $origen;
            $data['destino'] = $destino;
        }
        else
        {

        }
 
        //$this->load->view('layouts/head');
        //$this->load->view("site/ver_mensajes", $data);
        if ($tipo_vista==1) {
            $this->load->view('layouts/head');
            $this->load->view("site/ver_mensajes", $data);
            $this->load->view("layouts/footer");
        }
        if ($tipo_vista==2) {
            $this->load->view("site/ver_mensajes_led", $data);
        }
        if ($tipo_vista==3) {
            $data['color'] = '#fff';
            $data['background'] = '#000';
            $data['border'] = '2px solid #fff';
            $data['imagen'] = 'noche';
            $this->load->view("site/mensajes", $data);
        }
        if ($tipo_vista==4) {
            $data['color'] = '#000';
            $data['background'] = '#fff';
            $data['border'] = '2px solid #000';
            $data['imagen'] = 'dia';
            $this->load->view("site/mensajes", $data);
        }

        if ($tipo_vista==5) {
            $this->load->view('layouts/head');
            $this->load->view("site/no_enviados", $data);
            $this->load->view("layouts/footer");
        }

        if ($tipo_vista==6) {
            $data['color'] = '#fff';
            $data['background'] = '#000';
            $data['border'] = '1px solid #fff';
            $data['imagen'] = 'noche';
            $this->load->view("site/pizarra_papeleta", $data);
        }
        if ($tipo_vista==7) {
            $data['color'] = '#000';
            $data['background'] = '#fff';
            $data['border'] = '1px solid #000';
            $data['imagen'] = 'dia';
            $this->load->view("site/pizarra_papeleta", $data);
        }
        
        //$this->load->view("site/mensajes", $data);
        //$this->load->view("layouts/footer");
        
    }
    
    

}

/* End of file site.php */
/* Location: ./application/controllers/site.php */
