<?php

/**
 * Description of pec
 *
 * @author Tavor
 */

class pec extends CI_Controller {
    
    var $helpers = array('form');
    var $libraries = array('form_validation');
    var $models = array('pec_model');
    
    function __construct() {
        parent::__construct();
        $this->load->helper($this->helpers);
        $this->load->library($this->libraries);
        $this->load->model($this->models);
    }
    
    function view_all() {
        if($this->session->userdata('usrid')){
            
            $get_all = $this->pec_model->getAll();

            $data = array(
                'view' => 'pec/view_all',
                'get_all' => $get_all
            );

            $this->load->view('layouts/template', $data);
            
        }
        else{
            redirect("site/logout");
        }
    }
    
    public function delete() {
        $id = $this->uri->segment(3);
        $delete = $this->pec_model->delete($id);
    }
    
    public function delete_comment() {
        $id = $this->uri->segment(3);
        $delete = $this->pec_model->delete_comment($id);
    }
    
    
    public function create() {
        if($this->session->userdata('usrid')){
            
            $this->form_validation->set_rules('input_nombre', 'Nombre', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_descripcion', 'Descripción', 'trim|required|xss_clean');
            //$this->form_validation->set_rules('userfile', 'Archivo', 'required');
            
            $nombre = $this->input->post('input_nombre');
            $descripcion = $this->input->post('input_descripcion');
            $file = $this->input->post('userfile');
            
            $data = array(
                'view' => 'pec/_form',
            );

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('layouts/template', $data);
            } else {
                //var_dump($file);
                $upload_doc = $this->do_upload($file);
                //var_dump($upload_doc);
                if ($upload_doc == false) {
                    echo '<script>alert("Error al subir archivo.");</script>';
                    //echo $upload_doc;
                    //echo "error al subir doc";
                    //redirect("pec/create", $data);
                } else {
                    $create = $this->pec_model->create($nombre, $descripcion, base_url("documentos/pec/"). "/" . $upload_doc["file_name"]);

                    if ($create == FALSE) {
                        echo '<script>alert("No se ha podido crear el registro");</script>';
                        //$this->view_all();
                        redirect("pec/view_all");
                    } else {
                        $data = array(
                            $message => '<script>alert("Registro creado correctamente");</script>',
                        );
                        //$this->view_all();
                        redirect("pec/view_all", $data);
                    }
                }
            }
        
        } else {
            redirect("site/logout");
        }
    }
    
    public function do_upload($file) {

        //$file = $this->input->post('userfile');

        if (!is_dir("./documentos/")) {
            mkdir("./documentos/", 0777, TRUE);
        }

        if (!is_dir("./documentos/pec")) {
            mkdir("./documentos/pec", 0777, TRUE);
        }

        $config['upload_path'] = './documentos/pec';
        $config['allowed_types'] = 'pdf|doc|docx';
        $config['max_size'] = '10240';
        $config['encrypt_name'] = true;
        //$config['overrite'] = true;
        $this->load->library('upload', $config);
        //$this->upload->initialize($config);

        if (!$this->upload->do_upload()) {
            echo $this->upload->display_errors();
            return false;
        } else {
            //$data = array('upload_data' => $this->upload->data());
            $data = $this->upload->data();
            //$filepath = $data["full_path"];
            //print_r($data);

            return $data;
            //$this->load->view('upload_success', $data);
        }
    }
    
    public function ver_comentarios(){
        if($this->session->userdata('usrid')){
            
            $this->form_validation->set_rules('input_comentario', 'Comentario', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_titulo', 'Título', 'trim|required|xss_clean');
            
            $comentario = $this->input->post('input_comentario');
            $titulo = $this->input->post('input_titulo');
            
            $id = $this->uri->segment(3);
            $comments = $this->pec_model->getComments($id);

            $data = array(
                'view' => 'pec/view_comments',
                'name' => 'PEC',
                'id_pec' => $id,
                'comments' => $comments
            );
            
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('layouts/template', $data);
            }
            else {
                $newComment = $this->pec_model->addComment($id, $titulo, $comentario);
                if ($newComment == true){
                    $this->session->set_flashdata('result_ok', 'Comentario agregado con éxito.');
                }
                else {
                    $this->session->set_flashdata('result_failed', 'Error al agregar comentario.');
                }
                redirect(current_url());
            }

            //$this->load->view('layouts/template', $data);
        
        }else{
            redirect("site/logout");
        }
    }
    
    public function update() {        
        if($this->session->userdata('usrid')){
        
            $id = $this->input->post('input_id');
            $nombre = $this->input->post('input_nombre');
            $descripcion = $this->input->post('input_descripcion');
            $file = $this->input->post('userfile');
            
            $id_update = $this->uri->segment(3);
            
            $view = $this->pec_model->view($id_update);
            $this->form_validation->set_rules('input_nombre', 'Nombre', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_descripcion', 'Descripción', 'trim|required|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                foreach ($view as $v) {
                    $id = $v->id_pec;
                    $nombre = $v->nombre;
                    $descripcion = $v->descripcion;
                    $file = $v->doc_path;
                }
                
                $data = array(
                    'view' => 'pec/_form',
                    'id' => $id,
                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                    'file' => $file
                );

                $this->load->view('layouts/template', $data);

            } else {
                if (!empty($_FILES['userfile']['name'])){
                    $upload_doc = $this->do_upload($file);
                    if ($upload_doc != false){
                        $update = $this->pec_model->update($id, $nombre, $descripcion, base_url("documentos/pec/"). "/" . $upload_doc["file_name"]);
                    }
                    else {
                        echo '<script>alert("Error al subir archivo.");</script>';
                        redirect(current_url());
                    }
                }
                else {
                    $update = $this->pec_model->update($id, $nombre, $descripcion, null);
                }
                
                if ($update == FALSE) {
                    echo utf8_decode('<script>alert("No se ha realizado la actualización, posiblemente no hayan datos a actualizar, o ha existido un error inesperado");</script>');
                    redirect("pec/view_all");
                    //$this->view_all();
                } else {
                    echo '<script>alert("Registro Actualizado correctamente");</script>';
                    redirect("pec/view_all");
                    //$this->view_all();
                }
            }
        
        }else{
            redirect("site/logout");
        }
    }
    
    public function update_comment() {        
        if($this->session->userdata('usrid')){
        
            $id_update = $this->uri->segment(3);
            
            $this->form_validation->set_rules('input_titulo', 'Nombre', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_comentario', 'Descripción', 'trim|required|xss_clean');
            
            $id_comentario = $this->input->post('input_id_comentario');
            $titulo = $this->input->post('input_titulo');
            $descripcion = $this->input->post('input_comentario');
            $id_pec = $this->input->post('input_id_pec');
            
            $comment = $this->pec_model->getComment($id_update);
            if ($this->form_validation->run() == FALSE) {

                $data = array(
                    'view' => 'pec/comment_form',
                    'id_comentario' => $comment->id_comentario,
                    'id_pec' => $comment->id_pec,
                    'titulo' => $comment->titulo,
                    'comentario' => $comment->comentario
                );

                $this->load->view('layouts/template', $data);

            } else {
                $update = $this->pec_model->update_comment($id_comentario, $titulo, $descripcion);
                if ($update == FALSE) {
                    //echo utf8_decode('<script>alert("No se ha realizado la actualización, posiblemente no hayan datos a actualizar, o ha existido un error inesperado.");</script>');
                    $this->session->set_flashdata('result_failed', 'Error al editar comentario.');
                    
                    redirect("pec/ver_comentarios/" . $id_pec );
                } else {
                    $this->session->set_flashdata('result_ok', 'Comentario editado con éxito.');
                    //echo '<script>alert("Registro actualizado correctamente.");</script>';
                    redirect("pec/ver_comentarios/" . $id_pec );
                }
            }
        
        } else {
            redirect("site/logout");
        }
    }
    
    
}
