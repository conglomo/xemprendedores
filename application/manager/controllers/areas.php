<?php
/**
 * Description of areas
 *
 * @author nicolas
 */
class Areas extends CI_Controller {

    var $table_name = 'area';
    var $helpers = array('form');
    var $libraries = array('form_validation');
    var $models = array('area_model');
    var $principal_model = 'area_model';
    var $view_viewAll = 'areas/view_all';
    var $view_view = 'areas/view';
    var $view_create = 'areas/create';
    var $view_update = 'areas/update';
    var $view_form = 'areas/_form';
    var $name = 'Areas';

    function __construct() {
        parent::__construct();
        $this->load->helper($this->helpers);
        $this->load->library($this->libraries);
        $this->load->model($this->models);
    }

    function view_all() {
        if($this->session->userdata('usrid')){

            $get_all = $this->area_model->view_all($this->table_name);

            $data = array(
                'view' => $this->view_viewAll,
                'get_all' => $get_all,
            );

            $this->load->view('layouts/template', $data);
        
        }else{
            redirect("site/logout");
        }
        
    }

    function view() {
        if($this->session->userdata('usrid')){
            
            $id = $this->uri->segment(3);

            $view = $this->area_model->view($this->table_name, $id);

            foreach ($view as $v) {
                $id = $v->area_id;
                $nombre = $v->area_nombre_area;
            }

            $data = array(
                'view' => $this->view_view,
                'name' => $this->name,
                'id' => $id,
                'nombre' => $nombre,
            );

            $this->load->view('layouts/template', $data);
        
        }else{
            redirect("site/logout");
        }
    }

    function create() {
        if($this->session->userdata('usrid')){
            
            $this->form_validation->set_rules('input_nombre', 'Nombre', 'trim|required|xss_clean');

            $nombre = $this->input->post('input_nombre');

            $data = array(
                'view' => 'areas/_form',
            );

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('layouts/template', $data);
            } else {
                $create = $this->area_model->create($this->table_name, $nombre);

                if ($create == FALSE) {
                    echo '<script>alert("No se ha podido crear el registro");</script>';
                    redirect("areas/view_all");
                } else {
                    echo '<script>alert("Registro creado correctamente");</script>';
                    redirect("areas/view_all");
                }
            }
        
        }else{
            redirect("site/logout");
        }
    }

    function update() {
        if($this->session->userdata('usrid')){

            $id_update = $this->uri->segment(3);
            $view = $this->area_model->view($this->table_name, $id_update);

            $this->form_validation->set_rules('input_nombre', 'Nombre', 'trim|required|xss_clean');

            $id = $this->input->post('input_id');
            $nombre = $this->input->post('input_nombre');

            if ($this->form_validation->run() == FALSE) {
                foreach ($view as $v) {
                    $id = $v->area_id;
                    $nombre = $v->area_nombre_area;
                }

                $data = array(
                    'view' => $this->view_form,
                    'id' => $id,
                    'nombre' => $nombre,
                );

                $this->load->view('layouts/template', $data);
            } else {
                $update = $this->area_model->update($this->table_name, $id, $nombre);

                if ($update == FALSE) {
                    echo '<script>alert("No se ha realizado la actualización, posiblemente no hayan datos a actualizar, o ha existido un error inesperado");</script>';
                    $this->view_all();
                } else {
                    echo '<script>alert("Registro Actualizado correctamente");</script>';
                    $this->view_all();
                }
            }
        
        }else{
            redirect("site/logout");
        }
    }

    function delete() {
        $id = $this->uri->segment(3);

        $delete = $this->area_model->delete($this->table_name, $id);
    }
}

/* End of file areas.php */
/* Location: ./application/controllers/areas.php */