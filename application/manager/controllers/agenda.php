<?php

class Agenda extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper("form");
        $this->load->library("form_validation");
        $this->load->model("agenda_model");
        $this->load->model("usuario_model");
        $this->load->model("area_model");
    }

    function crear_evento() {
        if($this->session->userdata('usrid')){
            $this->form_validation->set_rules('input_fecha', 'Fecha', 'trim|required|xss_clean|callback_date_valid[' 
                    . $this->input->post('input_fecha') . ']');
            $this->form_validation->set_rules('input_inicio', 'Hora de Inicio', 'trim|required|xss_clean|min_length[5]|max_length[5]|callback_validate_time[' 
                    . $this->input->post('input_inicio') . ']');
            $this->form_validation->set_rules('input_fin', 'Hora de Término', 'trim|required|xss_clean|callback_check_equal_less[' 
                    . $this->input->post('input_inicio') . ']|callback_validate_time['  . $this->input->post('input_fin') . ']');
            $this->form_validation->set_rules('input_descripcion', 'Descripción', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_asignado', 'Usuario Asignado', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_entrevistado', 'Entrevistado', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_correo_entrevistado', 'Correo entrevistado', 'trim|valid_email|xss_clean');
            $this->form_validation->set_rules('area_entrevistado', 'Área de entrevistado', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_correo_entrevistado', 'Correo entrevistado', 'trim|valid_email|xss_clean');
            $this->form_validation->set_rules('input_actividad_terreno', 'Actividad en terreno', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_tipo_actividad', 'Tipo de actividad', 'trim|required|xss_clean');
            $this->form_validation->set_message('date_valid', 'Fecha inválida.');
            $fecha = $this->input->post('input_fecha');
            $inicio = $this->input->post('input_inicio');
            $fin = $this->input->post('input_fin');
            $desc = $this->input->post('input_descripcion');
            $user = $this->input->post('input_asignado');
            $entrevistado = $this->input->post('input_entrevistado');
            $entrevistado_correo = $this->input->post('input_correo_entrevistado');
            $area = $this->input->post('area_entrevistado');
            $creador = $this->session->userdata('usrid');
            $actividad_terreno = $this->input->post('input_actividad_terreno');
            $tipo_actividad = $this->input->post('input_tipo_actividad');
            
            $areas = $this->area_model->view_all('area');
            $tipos_actividad = $this->agenda_model->getTiposActividad();
            
            if ($this->session->userdata('usrrole') == 2){
                $user_id = $this->session->userdata('usrid');
                $usuarios = $this->usuario_model->view('usuarios', $user_id);
            }
            else {
                $usuarios = $this->usuario_model->view_all('usuarios');
            }

            $data = array(
                'view' => 'agenda/crear_evento',
                'areas' => $areas,
                'usuarios' => $usuarios,
                'tipos_actividad' => $tipos_actividad
            );
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('layouts/template', $data);
            } else {
                //Cambiar formato de fecha (d-m-Y -> Y-m-d)
                $nueva_fecha = date("Y-m-d", strtotime($fecha));
                $agenda_insert = $this->agenda_model->insertarEvento($nueva_fecha, $inicio, $fin, $desc, $user, $entrevistado, $entrevistado_correo, $area, $creador, $actividad_terreno, $tipo_actividad);
                $email_creador = $this->usuario_model->getUserEmail($user);
                $user_info = $this->usuario_model->getUserInfo($user);
                $user_full_name = $user_info->usr_nombre . ' ' . $user_info->usr_apellidos;
                $nombre_area = $this->area_model->getAreaName($area);
                $email_data = array('entrevistado' => $entrevistado, 'creador' => $user_full_name, 'fecha' => $fecha, 'hora_inicio' => $inicio,
                    'hora_termino' => $fin, 'area' => $nombre_area->area_nombre_area, 'descripcion' => $desc);
                $email_view = $this->load->view('agenda/email_nuevo_evento', $email_data, TRUE);
                $this->sendEmail($email_creador->usr_email, $user, $email_view, $entrevistado_correo);
//                if ($agenda_insert == FALSE) {
//                    redirect("agenda/ver_eventos");
//                } else {
//                    redirect("agenda/ver_eventos");
//                }
                redirect("agenda/ver_eventos");
            }

        }
        else {
            redirect("site/logout");
        }
    }
    
    public function test(){
        $query = $this->usuario_model->getUserInfo($this->session->userdata('usrid'));
        echo $query->usr_nombre;
        echo $query->usr_apellidos;
        //var_dump($creador_full_name);
    }

    function editar_evento() {

        if($this->session->userdata('usrid')){

            $id_evento = $this->uri->segment(3);
            $evento = $this->agenda_model->verEvento($id_evento);
            $this->form_validation->set_rules('input_fecha', 'Fecha', 'trim|required|xss_clean|callback_date_valid[' 
                    . $this->input->post('input_fecha') . ']');
            $this->form_validation->set_rules('input_inicio', 'Hora de Inicio', 'trim|required|xss_clean|min_length[5]|max_length[5]|callback_validate_time[' 
                    . $this->input->post('input_inicio') . ']');
            $this->form_validation->set_rules('input_fin', 'Hora de Término', 'trim|required|xss_clean|callback_check_equal_less[' 
                    . $this->input->post('input_inicio') . ']|callback_validate_time['  . $this->input->post('input_fin') . ']');
            //$this->form_validation->set_rules('input_inicio', 'Hora de Inicio', 'trim|required|xss_clean');
            //$this->form_validation->set_rules('input_fin', 'Hora de Término', 'trim|required|xss_clean|callback_check_equal_less[' . $this->input->post('input_inicio') . ']');
            $this->form_validation->set_rules('input_descripcion', 'Descripción', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_asignado', 'Usuario Asignado', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_entrevistado', 'Entrevistado', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_correo_entrevistado', 'Correo entrevistado', 'trim|valid_email|xss_clean');
            $this->form_validation->set_rules('area_entrevistado', 'Área de entrevistado', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_estado', 'Estado', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_actividad_terreno', 'Actividad en terreno', 'trim|required|xss_clean');
            $this->form_validation->set_rules('input_tipo_actividad', 'Tipo de actividad', 'trim|required|xss_clean');
            
            $id = $this->input->post('input_id');
            $fecha = $this->input->post('input_fecha');
            $inicio = $this->input->post('input_inicio');
            $fin = $this->input->post('input_fin');
            $desc = $this->input->post('input_descripcion');
            $user = $this->input->post('input_asignado');
            $entrevistado = $this->input->post('input_entrevistado');
            $entrevistado_correo = $this->input->post('input_correo_entrevistado');
            $area = $this->input->post('area_entrevistado');
            $estado = $this->input->post('input_estado');
            $actividad_terreno = $this->input->post('input_actividad_terreno');
            $tipo_actividad = $this->input->post('input_tipo_actividad');

            if ($this->form_validation->run() == FALSE) {
                $areas = $this->area_model->view_all('area');
                $tipos_actividad = $this->agenda_model->getTiposActividad();
                
                if ($this->session->userdata('usrrole') == 2){
                    $user_id = $this->session->userdata('usrid');
                    $usuarios = $this->usuario_model->view('usuarios', $user_id);
                }
                else {
                    $usuarios = $this->usuario_model->view_all('usuarios');
                }

                foreach ($evento as $ev) {
                    $id = $this->uri->segment(3);
                    $fecha = $ev->agen_fecha;
                    $inicio = $ev->agen_hora_inicio;
                    $fin = $ev->agen_hora_fin;
                    $contacto = $ev->agen_contacto_area;
                    $contacto_correo = $ev->agen_contacto_correo;
                    $descripcion = $ev->agen_descripcion;
                    $usuario = $ev->usr_nombre;
                    $area_id = $ev->agen_area;
                    $usr_id = $ev->agen_usuario;
                    $comentarios = $ev->agen_comentarios;
                    $estado = $ev->agen_estado;
                    $actividad_terreno = $ev->agen_actividad_terreno;
                    $tipo_actividad = $ev->nombre_actividad;
                }

                $data = array(
                    'view' => 'agenda/crear_evento',
                    'areas' => $areas,
                    'usuarios' => $usuarios,
                    'id' => $id,
                    'fecha' => $fecha,
                    'inicio' => $inicio,
                    'fin' => $fin,
                    'contacto' => $contacto,
                    'contacto_correo' => $contacto_correo,
                    'descripcion' => $descripcion,
                    'usuario' => $usuario,
                    'comentarios' => $comentarios,
                    'area_id' => $area_id,
                    'usr_id' => $usr_id,
                    'estado' => $estado,
                    'actividad_terreno' => $actividad_terreno,
                    'tipo_actividad' => $tipo_actividad,
                    'tipos_actividad' => $tipos_actividad
                );
                $this->load->view('layouts/template', $data);
            } else {
                $eventos = $this->agenda_model->verTodosEventos();

                $dato = array(
                    'view' => 'agenda/ver_eventos',
                    'eventos' => $eventos,
                );
                $nueva_fecha = date("Y-m-d", strtotime($fecha));
                $agenda_insert = $this->agenda_model->editarEvento($id, $nueva_fecha, $inicio, $fin, $desc, $user, $entrevistado, $entrevistado_correo, $area, $estado, $actividad_terreno, $tipo_actividad);

                if ($agenda_insert == FALSE) {
                    echo '<script>alert("No se ha realizado la actualización, posiblemente no hayan datos a actualizar, o ha existido un error inesperado");</script>';
                    $this->ver_eventos();
                } else {
                    echo '<script>alert("Registro Actualizado correctamente");</script>';
                    $this->ver_eventos();
                }
            }

        }else{
            redirect("site/logout");
        }
    }

    function ver_evento() {
        if($this->session->userdata('usrid')){
            $id_evento = $this->uri->segment(3);

            $evento = $this->agenda_model->verEvento($id_evento);
            
            $entrevistados = $this->agenda_model->getEntrevistados($id_evento);

            foreach ($evento as $ev) {
                $id = $ev->agen_id;
                $fecha = $ev->agen_fecha;
                $inicio = $ev->agen_hora_inicio;
                $fin = $ev->agen_hora_fin;
                $estado = $ev->agen_estado;
                $contacto = $ev->agen_contacto_area;
                $contacto_correo = $ev->agen_contacto_correo;
                $descripcion = $ev->agen_descripcion;
                $usuario = $ev->usr_nombre;
                $areas = $ev->area_nombre_area;
                $padre = $ev->agen_padre;
                $actividad_terreno = $ev->agen_actividad_terreno;
                $tipo_actividad = $ev->nombre_actividad;

                if ($ev->agen_comentarios) {
                    $comentarios = $ev->agen_comentarios;
                } else {
                    $comentarios = "Sin Comentarios";
                }
            }

            $data = array(
                'view' => 'agenda/ver_evento',
                'even' => $evento,
                'id' => $id,
                'fecha' => $fecha,
                'inicio' => $inicio,
                'fin' => $fin,
                'estado' => $estado,
                'contacto' => $contacto,
                'contacto_correo' => $contacto_correo,
                'descripcion' => $descripcion,
                'usuario' => $usuario,
                'area' => $areas,
                'comentarios' => $comentarios,
                'padre' => $padre,
                'actividad_terreno' => $actividad_terreno,
                'tipo_actividad' => $tipo_actividad,
                'entrevistados' => $entrevistados
            );

            $this->load->view('layouts/template', $data);
        }else{
            redirect("site/logout");
        }
    }

    function ver_eventos() {
        if($this->session->userdata('usrid')){
            if($this->session->userdata('usrrole') == 1 || $this->session->userdata('usrrole') == 3){
                $eventos = $this->agenda_model->verTodosEventos();
            }
            else {
                $user_id = $this->session->userdata('usrid');
                $eventos = $this->agenda_model->verEventosEmpleado($user_id);
            }

            $data = array(
                'view' => 'agenda/ver_eventos',
                'eventos' => $eventos,
            );

            $this->load->view('layouts/template', $data);
        }else{
            redirect("site/logout");
        }
    }

    function eliminar_evento() {
        $id_evento = $this->uri->segment(3);

        $eliminar_evento = $this->agenda_model->eliminarEvento($id_evento);
    }

    function generar_pdf() {

        $id_evento = $this->uri->segment(3);
        $evento = $this->agenda_model->verEvento($id_evento);
        foreach ($evento as $ev) {
            $id = $ev->agen_id;
            $fecha = $ev->agen_fecha;
            $inicio = $ev->agen_hora_inicio;
            $fin = $ev->agen_hora_fin;
            $contacto = $ev->agen_contacto_area;
            $descripcion = $ev->agen_descripcion;
            $usuario = $ev->usr_nombre;
            $areas = $ev->area_nombre_area;
            //$contacto_nombre = $ev->agen_nombre_contacto;
            //$contacto_mail = $ev->agen_contacto_mail;
            //$contacto_numero = $ev->agen_contacto_numero;

            if ($ev->agen_comentarios) {
                $comentarios = $ev->agen_comentarios;
            } else {
                $comentarios = "Sin Comentarios";
            }
        }
        $this->load->helper('file');
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./files/pdfs/');
        //Set Filename
        $this->html2pdf->filename('test.pdf');

        //establecemos el tipo de papel
        $this->html2pdf->paper('a4', 'portrait');

        //datos que queremos enviar a la vista, lo mismo de siempre
        $data = array(
            'title' => 'PDF Created',
            'view' => 'agenda/ver_evento',
            'evento' => $evento,
            'id' => $id,
            'fecha' => $fecha,
            'inicio' => $inicio,
            'fin' => $fin,
            'contacto' => $contacto,
            'descripcion' => $descripcion,
            'usuario' => $usuario,
            'area' => $areas,
            'comentarios' => $comentarios,
            'contacto_nombre' => $contacto_nombre,
            'contacto_mail' => $contacto_mail,
            'contacto_numero' => $contacto_numero
        );

        //Load html view
        $this->html2pdf->html(utf8_decode($this->load->view('agenda/reporte_pdf', $data, true)));
        
        //si el pdf se guarda correctamente lo mostramos en pantalla
        if ($this->html2pdf->create('save')) {
            if (is_dir("./files/pdfs")) {
                $filename = "test.pdf";
                //$route = base_url("files/pdfs/test.pdf");
                $route = "./files/pdfs/" . $filename;
                if (file_exists("./files/pdfs/" . $filename)) {
                    header('Content-type: application/pdf');
                    readfile($route);
                }
            }
        }
        
    }

    function check_equal_less($second_field, $first_field) {
        if ($second_field <= $first_field) {
            $this->form_validation->set_message('check_equal_less', 'Hora de término no puede ser menor que hora de inicio.');
            return false;
        } else {
            return true;
        }
    }
    
    function date_valid($date){
        $day = (int) substr($date, 0, 2);
        $month = (int) substr($date, 3, 2);
        $year = (int) substr($date, 6, 4);
        return checkdate($month, $day, $year);
    }
    
    public function validate_time($str){
        //Assume $str SHOULD be entered as HH:MM
        list($hh, $mm) = array_pad(explode(':', $str, 2),2,null);
        if (empty($hh) || empty($mm)){
            $this->form_validation->set_message('validate_time', 'Hora inválida.');
            return FALSE;
        }
        else if (!is_numeric($hh) || !is_numeric($mm)){
            $this->form_validation->set_message('validate_time', 'Hora no numérica.');
            return FALSE;
        }
        else if ((int) $hh > 24 || (int) $mm > 59)
        {
            $this->form_validation->set_message('validate_time', 'Hora inválida.');
            return FALSE;
        }
        else if (mktime((int) $hh, (int) $mm) === FALSE)
        {
            $this->form_validation->set_message('validate_time', 'Hora inválida.');
            return FALSE;
        }
        return TRUE;
    }
    
    public function sendEmail($sender_email = null,$sender_name = null, $content, $email_to){
        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'desarrollo@ibex.cl';
        $config['smtp_pass'] = 'Xebi.2010';
//        $config['smtp_host'] = 'ssl://owa.mutual.cl';
//        $config['smtp_port'] = '587';
//        $config['smtp_user'] = 'gerencialan@mutual.cl';
//        $config['smtp_pass'] = 'Mut$2014';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'utf-8';
        //$config['wordwrap'] = TRUE;
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        
        $this->email->initialize($config);
        
        $this->email->from('desarrollo@ibex.cl', 'HSEC - LAN');
        $this->email->to($email_to); 
        if ($sender_email != null && $sender_name != null){
            $this->email->reply_to($sender_email, $sender_name);
        }
//        $this->email->cc('another@another-example.com'); 
//        $this->email->bcc('them@their-example.com'); 

        $this->email->subject('Nuevo evento asignado');
        $this->email->message($content);	

        if (!$this->email->send()){
            return false;
        }
        else {
            return true;
        }
        //echo $this->email->print_debugger();
    }

}