<?php
/* @var $this roles */
/* @var $model role_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= $name . " #" . $id; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Nombre</th>
                                    <td><?= $nombre; ?></td>
                                </tr>
                                <tr>
                                    <th>Apellidos</th>
                                    <td><?= $apellidos; ?></td>
                                </tr>
                                <tr>
                                    <th>Empresa</th>
                                    <td><?= $empresa; ?></td>
                                </tr>
                                <tr>
                                    <th>Cargo</th>
                                    <td><?= $cargo; ?></td>
                                </tr>
                                <tr>
                                    <th>Área</th>
                                    <td><?= $area; ?></td>
                                </tr>
                                <tr>
                                    <th>Rol</th>
                                    <td><?= $role; ?></td>
                                </tr>
                                <tr>
                                    <th>Username</th>
                                    <td><?= $username; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
