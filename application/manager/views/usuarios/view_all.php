<?php
/* @var $this usuarios */
/* @var $model usuario_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Usuarios</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="<?= site_url("usuarios/create"); ?>">
                <button type="button" class="btn btn-info">Crear Nuevo Usuario</button>
            </a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="user-table" width="100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Empresa</th>
                        <th>Cargo</th>
                        <th>Área</th>
                        <th>Rol</th>
                        <th>Username</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($get_all as $formulario) {
                        echo '<tr>';
                        echo '<td>' . $formulario->usr_nombre . ' ' .$formulario->usr_apellidos . '</td>';
                        echo '<td>' . $formulario->usr_empresa . '</td>';
                        echo '<td>' . $formulario->cargo_nombre_cargo . '</td>';
                        echo '<td>' . $formulario->area_nombre_area . '</td>';
                        echo '<td>' . $formulario->role_nombre_rol . '</td>';
                        echo '<td>' . $formulario->usr_username . '</td>';
                        echo '<td>
                                <a href="' . site_url("usuarios/view/" . $formulario->usr_id) . '" title="Ver"><i class="fa fa-search fa-fw"></i></a>
                                <a href="' . site_url("usuarios/update/" . $formulario->usr_id) . '" title="Editar"><i class="fa fa-edit fa-fw"></i></a>
                                <a href="' . site_url("usuarios/delete/" . $formulario->usr_id) . '" title="Eliminar" class="deleteRow"><i class="fa fa-times fa-fw"></i></a>
                                <a href="' . site_url("usuarios/generar_pdf/" . $formulario->usr_id) . '" title="Reporte PDF" target="_blank"><i class="fa fa-file-o fa-fw"></i></a>
                              </td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
                <!--<tfoot>
                    <tr>
                        <th><input type="text" name="search_id" value="Buscar..." class="search_init"/></th>
                        <th><input type="text" name="search_nombre" value="Buscar..." class="search_init" /></th>
                    </tr>
                </tfoot> -->
            </table>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(document).ready(function() {

        $(".deleteRow").click(function(event) {
            var href = $(this).attr("href");
            var btn = this;

            $.ajax({
                type: "GET",
                url: href,
                beforeSend: function() {
                    if (!confirm("Está a punto de eliminar un registro ; ¿Desea continuar?")) {
                        return false;
                    }
                },
                success: function(msgd) {
                    $(btn).closest('tr').fadeOut("slow");
                }
            });
            event.preventDefault();
        });
    });
</script>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    $('#user-table').dataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
        },
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "http://cdnjs.cloudflare.com/ajax/libs/datatables-tabletools/2.1.5/swf/copy_csv_xls_pdf.swf",
            "aButtons":[
                {"sExtends": "xls","sButtonText": "Exportar a Excel", "sFileName": "usuarios.xls", "mColumns": [0, 1, 3, 4,5]},
                {"sExtends": "pdf","sButtonText": "Exportar a PDF", "sFileName": "usuarios.pdf", "mColumns": [0, 1, 3, 4,5]},
                {"sExtends": "print","sButtonText": "Imprimir",  "mColumns": [0, 1, 3, 4,5]}
            ]
        }
    });
} );
</script>