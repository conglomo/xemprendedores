<?php
/* @var $this usuarios */
/* @var $model usuario_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <?= ($this->router->method == "create") ? '<h1 class="page-header">Crear</h1>' : '<h1 class="page-header">Editar</h1>'; ?>
        </div>
    </div>

    <?php $username_exist = $this->session->flashdata("username_exist"); ?>
    <?php if (($username_exist != "" || $username_exist != FALSE || $username_exist != NULL)) { ?>
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span>
                <?= (isset($username_exist)) ? $username_exist : ""; ?>
            </span>
        </div>
    <?php } ?>

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= ($this->router->method == "create") ? 'Registrar nuevo usuario en el sistema' : 'Editar usuario del sistema'; ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?= ($this->router->method == "create") ? form_open("usuarios/create") : form_open("usuarios/update"); ?>
                        <div class="col-lg-6">
                            <?php if (validation_errors() != FALSE) { ?>
                                <div class="alert alert-danger">
                                    <?= validation_errors(); ?>
                                </div>  
                            <?php } ?>

                            <input type='text' hidden id="input_id" name="input_id" value="<?= isset($id) ? $id : ''; ?>"/>

                            <div class="form-group">
                                <label>Nombre:</label>
                                <input class="form-control" placeholder="Nombre" id="input_nombre" name="input_nombre" 
                                       value="<?= isset($nombre) ? $nombre : ''; ?>">
                            </div>
                            <div class="form-group">
                                <label>Apellidos:</label>
                                <input class="form-control" placeholder="Apellidos" id="input_apellidos" name="input_apellidos" 
                                       value="<?= isset($apellidos) ? $apellidos : ''; ?>">
                            </div>
                            <div class="form-group">
                                <label>Empresa:</label>
                                <select class="form-control" id="input_empresa" name="input_empresa">
                                    <option value="mutual" <?= (isset($empresa) && $empresa == 'mutual') ? 'selected' : ''; ?>>Mutual</option>
                                    <option value="lan" <?= (isset($empresa) && $empresa == 'lan') ? 'selected' : ''; ?>>Lan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Cargo</label>
                                <select class="form-control" id="input_cargo" name="input_cargo">
                                    <?php for ($i = 0; $i < sizeof($cargos); $i++): ?>
                                        <option value="<?= $cargos[$i]->cargo_id; ?>" <?= (isset($cargo_id) && $cargo_id == $cargos[$i]->cargo_id) ? "selected" : ""; ?>><?= $cargos[$i]->cargo_nombre_cargo; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Área</label>
                                <select class="form-control" id="input_area" name="input_area">
                                    <?php for ($i = 0; $i < sizeof($areas); $i++): ?>
                                        <option value="<?= $areas[$i]->area_id; ?>" <?= (isset($area_id) && $area_id == $areas[$i]->area_id) ? "selected" : ""; ?>><?= $areas[$i]->area_nombre_area; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Rol</label>
                                <select class="form-control" id="input_role" name="input_role">
                                    <?php for ($i = 0; $i < sizeof($roles); $i++): ?>
                                        <option value="<?= $roles[$i]->role_id; ?>" <?= (isset($role_id) && $role_id == $roles[$i]->role_id) ? "selected" : ""; ?>><?= $roles[$i]->role_nombre_rol; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Username:</label>
                                <input class="form-control" placeholder="Usuario" id="input_username" name="input_username" 
                                       value="<?= isset($username) ? $username : ''; ?>">
                            </div>
                            <div class="form-group">
                                <label>Password:</label>
                                <input type="password" class="form-control" placeholder="Contraseña" id="input_password" name="input_password" value="">
                            </div>
                            <div class="form-group">
                                <label>Email:</label>
                                <input class="form-control" placeholder="Email" id="input_email" name="input_email" value="<?= isset($email) ? $email : ''; ?>">
                            </div>
                            <button type="submit" class="btn btn-default">
                                <?= ($this->router->method == "create") ? "Crear" : "Editar"; ?>
                            </button>
                            <button type="reset" class="btn btn-default">Limpiar Formulario</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>