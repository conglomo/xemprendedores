<?php
/* @var $this documentos */
/* @var $model documentos_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Documentos</h1>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="example">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($get_all as $formulario) {
                        echo '<tr>';
                        echo '<td>' . $formulario->doc_nombre . '</td>';
                        echo '<td>' . $formulario->doc_descripcion . '</td>';
                        echo '<td>
                                <a href="' . $formulario->doc_path . '" title="Ver" target="_blank"><i class="fa fa-search fa-fw"></i></a>
                                <a href="' . site_url("documentos/update/" . $formulario->doc_id) . '" title="Editar"><i class="fa fa-edit fa-fw"></i></a>
                                <a href="' . site_url("documentos/delete/" . $formulario->doc_id) . '" title="Eliminar" class="deleteRow"><i class="fa fa-times fa-fw"></i></a>
                              </td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
                <!--<tfoot>
                    <tr>
                        <th><input type="text" name="search_id" value="Buscar..." class="search_init"/></th>
                        <th><input type="text" name="search_nombre" value="Buscar..." class="search_init" /></th>
                    </tr>
                </tfoot> -->
            </table>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(document).ready(function() {

        $(".deleteRow").click(function(event) {
            var href = $(this).attr("href")
            var btn = this;

            $.ajax({
                type: "GET",
                url: href,
                beforeSend: function() {
                    if (!confirm("Está a punto de Eliminar un Instrumento inmediatamente al aceptar ; ¿Desea continuar?")) {
                        return false;
                    }
                },
                success: function(msgd) {
                        $(btn).closest('tr').fadeOut("slow");
                }
            });
            event.preventDefault();
        })
    });
</script>
