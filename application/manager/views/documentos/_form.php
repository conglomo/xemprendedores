﻿<?php
/* @var $this documentos */
/* @var $model documento_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <?= ($this->router->method == "create") ? '<h1 class="page-header">Subir</h1>' : '<h1 class="page-header">Editar</h1>'; ?>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= ($this->router->method == "create") ? 'Registrar nuevo documento en el sistema' : 'Editar documento del sistema'; ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?= ($this->router->method == "create") ? form_open_multipart("documentos/create") : form_open_multipart("documentos/update/" . $id); ?>
                        <div class="col-lg-6">
                            <?php if (validation_errors() != FALSE) { ?>
                                <div class="alert alert-danger">
                                    <?= validation_errors(); ?>
                                </div>  
                            <?php } ?>

                            <div class="form-group">
                                <label>Nombre:</label>
                                <input class="form-control" placeholder="Nombre del documento" id="input_nombre" name="input_nombre" 
                                       value="<?= isset($nombre) ? $nombre : ''; ?>" required>
                            </div>
                            <div class="form-group">
                                <label>Descripción:</label>
                                <input class="form-control" placeholder="Descripción del documento" id="input_descripcion" name="input_descripcion" 
                                       value="<?= isset($descripcion) ? $descripcion : ''; ?>" required>
                            </div>
                            
                            <div class="form-group">
                                <label>Archivo:</label>
                                <?php if($this->router->fetch_method() == 'update'){ ?>
                                <b><p><a href="<?= isset($file) ? $file : ''; ?>" target="_blank">Ver actual</a></p></b>
                                <input type="file" name="userfile" id="userfile" accept=".pdf">
                                <p>PDF, máximo 10Mb.</p>
                                <?php } else { ?>
                                <input type="file" name="userfile" id="userfile" accept=".pdf" required>
                                <p>PDF, máximo 10Mb.</p>
                                <?php } ?>
                            </div>
                            <?php if($this->router->fetch_method() == 'update'){ ?>
                            <input type="hidden" name="input_id" id ="input_id" value="<?= isset($id) ? $id : ''; ?>">
                            <?php } ?>
                            <button type="submit" class="btn btn-default">
                                <?= ($this->router->method == "create") ? "Guardar" : "Guardar Cambios"; ?>
                            </button>
                            <button type="reset" class="btn btn-default">Limpiar Formulario</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>