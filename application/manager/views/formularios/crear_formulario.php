﻿<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <?= ($this->router->method == "crear_evento") ? '<h1 class="page-header">Crear Actividad</h1>' : '<h1 class="page-header">Editar Actividad</h1>'; ?>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= ($this->router->method == "crear_evento") ? 'Registrar nueva actividad en agenda' : 'Editar actividad en agenda'; ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?= ($this->router->method == "crear_evento") ? form_open("agenda/crear_evento") : form_open("agenda/editar_evento"); ?>
                        <div class="col-lg-6">
                            <?php if (validation_errors() != FALSE) { ?>
                                <div class="alert alert-danger">
                                    <?= validation_errors(); ?>
                                </div>  
                            <?php } ?>
                            
                            <input type='text' hidden id="input_id" name="input_id" value="<?= isset($id) ? $id : ''; ?>"/>
                            
                            <div class="form-group">
                                <label>*Fecha:</label> 
                                <div class='input-group date' id='fechaPicker'>
                                    <input type='text' class="form-control" id="input_fecha" name="input_fecha" value="<?= isset($fecha) ? $fecha : ''; ?>" required/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>*Hora de Inicio:</label>
                                <div class='input-group date' id='inicioPicker'>
                                    <input type="text" class="form-control" id="timepicker" name="input_inicio" value="<?= isset($inicio) ? $inicio : ''; ?>" required>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>*Hora de Término:</label>
                                <div class='input-group date' id='finPicker'>
                                    <input type="text" class="form-control" id="input_fin" name="input_fin" value="<?= isset($fin) ? $fin : ''; ?>" required>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label>Tipo de actividad: </label>
                                
                                <select class="form-control" id="input_tipo_actividad" name="input_tipo_actividad">
                                    <?php for ($i = 0; $i < sizeof($tipos_actividad); $i++): ?>
                                        <option value="<?= $tipos_actividad[$i]->id_tipo_actividad ?>" <?= (isset($tipo_actividad) && $tipo_actividad == $tipos_actividad[$i]->id_tipo_actividad) ? "selected" : ""; ?>><?= $tipos_actividad[$i]->nombre_actividad; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>*Descripción (máximo 255 caracteres):</label>
                                <!--<input class="form-control" placeholder="Descripción de la entrevista" id="input_descripcion" name="input_descripcion" 
                                       value="<?//= isset($descripcion) ? $descripcion : ''; ?>"> -->
                                <textarea class="form-control" name="input_descripcion" id="input_descripcion" placeholder="Descripción de la entrevista" rows="2" maxlength="255" required><?= isset($descripcion) ? $descripcion : ''; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label>*Usuario Asignado: </label>
                                
                                <select class="form-control" id="input_asignado" name="input_asignado">
                                    
                                    <?php for ($i = 0; $i < sizeof($usuarios); $i++): ?>
                                        <option value="<?= $usuarios[$i]->usr_id ?>" <?= (isset($usr_id) && $usr_id == $usuarios[$i]->usr_id) ? "selected" : ""; ?>><?= $usuarios[$i]->usr_nombre . ' ' . $usuarios[$i]->usr_apellidos; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <?php if ($this->router->method == "editar_evento"){ ?>
                            <div class="form-group">
                                <label>Estado: </label>
                                <select class="form-control" id="input_estado" name="input_estado">
                                    <option value="Pendiente" <?= ($estado == 'Pendiente') ? 'selected' : '' ?>>Pendiente</option>
                                    <option value="Realizada" <?= ($estado == 'Realizada') ? 'selected' : '' ?>>Realizada</option>
                                    <option value="No Realizada" <?= ($estado == 'No Realizada') ? 'selected' : '' ?>>No Realizada</option>
                                    <option value="Suprimida" <?= ($estado == 'Suprimida') ? 'selected' : '' ?>>Suprimida</option>
                                </select>
                            </div>
                             <?php } ?>
                            <div class="form-group">
                                <label>Actividad en terreno: </label>
                                <select class="form-control" id="input_actividad_terreno" name="input_actividad_terreno">
                                    <option value="1" <?= (isset($actividad_terreno) && $actividad_terreno == 1) ? 'selected' : '' ?>>Sï</option>
                                    <option value="0" <?= (isset($actividad_terreno) && $actividad_terreno == 0) ? 'selected' : '' ?>>No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>*Entrevistado:</label>
                                <input class="form-control" placeholder="Nombre del entrevistado" id="input_entrevistado" name="input_entrevistado" value="<?= isset($contacto) ? $contacto : ''; ?>" required maxlength="150">
                            </div>
                            <div class="form-group">
                                <label>*Correo entrevistado:</label>
                                <input class="form-control" placeholder="Correo del entrevistado" id="input_correo_entrevistado" name="input_correo_entrevistado" value="<?= isset($contacto_correo) ? $contacto_correo : ''; ?>" maxlength="90">
                            </div>
                            <div class="form-group">
                                <label>*Área del entrevistado</label>
                                <select class="form-control" id="area_entrevistado" name="area_entrevistado">
                                    <?php for ($i = 0; $i < sizeof($areas); $i++): ?>
                                        <option value="<?= $areas[$i]->area_id; ?>" <?= (isset($area_id) && $area_id == $areas[$i]->area_id) ? "selected" : ""; ?>><?= $areas[$i]->area_nombre_area; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <label>*Todos los campos son obligatorios.</label>
                            <br>
                            <button type="submit" class="btn btn-default">
                                <?= ($this->router->method == "crear_evento") ? "Crear Evento" : "Editar Evento"; ?>
                            </button>
                            <button type="reset" class="btn btn-default">Limpiar Formulario</button>
                            <?= form_close(); ?>
                        </div>           
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>