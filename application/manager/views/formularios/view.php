<?php
/* @var $this formularios */
/* @var $model formulario_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Formulario #<?= $formulario->rir_id; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Creador</th>
                                    <td><?= !empty($formulario->usr_nombre) ? $formulario->usr_nombre : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Fecha Creación</th>
                                    <td><?= !empty($formulario->rir_fecha_informe) ? $formulario->rir_fecha_informe : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Tipo de Evento</th>
                                    <td><?= !empty($formulario->rig_tipo_evento) ? $formulario->rig_tipo_evento : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Tipo de Incidente</th>
                                    <td><?= !empty($formulario->rig_tipo_incidente) ? $formulario->rig_tipo_incidente : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Fecha Evento</th>
                                    <td><?= !empty($formulario->rig_fecha_evento) ? $formulario->rig_fecha_evento : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Hora Evento</th>
                                    <td><?= !empty($formulario->rig_hora_evento) ? $formulario->rig_hora_evento : 'N/A' ; ?></td>
                                </tr>
                                <tr>
                                    <th>Clasificación Incidente</th>
                                    <td><?= !empty($formulario->descripcion_incidente) ? $formulario->descripcion_incidente : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Centro de Trabajo</th>
                                    <td><?= !empty($formulario->nombre_centro_trabajo) ? $formulario->nombre_centro_trabajo : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Área</th>
                                    <td><?= !empty($formulario->nombre_area) ? $formulario->nombre_area : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Lugar Específico</th>
                                    <td><?= !empty($formulario->rig_lugar_especifico) ? $formulario->rig_lugar_especifico : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Descripción del Incidente</th>
                                    <td><?= !empty($formulario->rig_descripcion) ? $formulario->rig_descripcion : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Testigo 1</th>
                                    <td><?= !empty($formulario->rig_testigo_1) ? $formulario->rig_testigo_1 : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Testigo 2</th>
                                    <td><?= !empty($formulario->rig_testigo_2) ? $formulario->rig_testigo_2 : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Testigo 3</th>
                                    <td><?= !empty($formulario->rig_testigo_3) ? $formulario->rig_testigo_3 : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Nombre Persona</th>
                                    <td><?= !empty($formulario->rip_nombre_completo) ? $formulario->rip_nombre_completo : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Rut Persona</th>
                                    <td><?= !empty($formulario->rip_rut) ? $formulario->rip_rut : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Sexo</th>
                                    <td><?= !empty($formulario->rip_sexo) ? $formulario->rip_sexo : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Edad</th>
                                    <td><?= !empty($formulario->rip_edad) ? $formulario->rip_edad : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>BP</th>
                                    <td><?= !empty($formulario->rip_bp) ? $formulario->rip_bp : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Razón Social Persona</th>
                                    <td><?= !empty($formulario->razon_social_persona) ? $formulario->razon_social_persona : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Lesión Aparente</th>
                                    <td><?= !empty($formulario->lesion_persona) ? $formulario->lesion_persona : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Parte Lesionada</th>
                                    <td><?= !empty($formulario->parte_lesionada_persona) ? $formulario->parte_lesionada_persona : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Área Persona</th>
                                    <td><?= !empty($formulario->area_persona) ? $formulario->area_persona : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Jefe Directo Persona</th>
                                    <td><?= !empty($formulario->rip_jefe_directo) ? $formulario->rip_jefe_directo : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Responsable Área</th>
                                    <td><?= !empty($formulario->rip_responsable_area) ? $formulario->rip_responsable_area : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Turno Persona</th>
                                    <td><?= !empty($formulario->turno_persona) ? $formulario->turno_persona : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Licencias. cursos o habilitaciones específicas requeridas</th>
                                    <td><?= !empty($formulario->rip_otros) ? $formulario->rip_otros : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Proceso</th>
                                    <td><?= !empty($formulario->rie_proceso) ? $formulario->rie_proceso : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Actividad Genérica</th>
                                    <td><?= !empty($formulario->rie_actividad_generica) ? $formulario->rie_actividad_generica : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Tarea Específica</th>
                                    <td><?= !empty($formulario->rie_tarea_especifica) ? $formulario->rie_tarea_especifica : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Frecuencia Tarea</th>
                                    <td><?= !empty($formulario->rie_frecuencia_tarea) ? $formulario->rie_frecuencia_tarea : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Estándar Aplicable</th>
                                    <td><?= !empty($formulario->rie_estandar) ? $formulario->rie_estandar : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Maquinarias</th>
                                    <td><?= !empty($formulario->rie_maquinarias) ? $formulario->rie_maquinarias : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Equipos</th>
                                    <td><?= !empty($formulario->rie_equipos) ? $formulario->rie_equipos : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Herramientas</th>
                                    <td><?= !empty($formulario->rie_herramientas) ? $formulario->rie_herramientas : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Materiales</th>
                                    <td><?= !empty($formulario->rie_materiales) ? $formulario->rie_materiales : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Condiciones Climáticas Ambiente</th>
                                    <td><?= !empty($formulario->descripcion_condicion_climatica) ? $formulario->descripcion_condicion_climatica : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Lugar de Trabajo</th>
                                    <td><?= !empty($formulario->centro_trabajo_clima) ? $formulario->centro_trabajo_clima : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Actos</th>
                                    <td><?= !empty($formulario->actos) ? $formulario->actos : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Condiciones</th>
                                    <td><?= !empty($formulario->condiciones) ? $formulario->condiciones : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Latitud</th>
                                    <td><?= !empty($formulario->rig_latitude) ? $formulario->rig_latitude : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Longitud</th>
                                    <td><?= !empty($formulario->rig_longitude) ? $formulario->rig_longitude : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Dirección</th>
                                    <td><?= !empty($formulario->rig_direccion) ? $formulario->rig_direccion : 'N/A'; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php if (!empty($files)){
                        echo "<p><b>Archivos adjuntos</b></p>";
                    } ?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <?php if (empty($files)) {
                                        echo '<p><b>No hay archivos adjuntos para este formulario.</b></p>';
                                    }
                                else {
                                    echo '<thead>';
                                    echo '<tr>';
                                    echo '<th>ID</th>';
                                    echo '<th>Tipo</th>';
                                    echo '<th>Acciones</th>';
                                    echo '</tr>';
                                    echo '</thead>';
                                    echo '<tbody>';
                                
                                    foreach ($files as $file) {
                                        echo '<tr>';
                                        echo '<td><b>' . $file->multi_id . '</b></td>';
                                        echo '<td><b>' . $file->multi_tipo . '</b></td>';
                                        echo '<td>
                                             <a href="' . str_replace("/intranet","", base_url()) . "multimedia/" . $formulario->rir_id . 
                                                '/' . $file->multi_path . '" title="Ver" target="_blank"><i class="fa fa-search fa-fw"></i></a>
                                            </td>';
                                        echo '</tr>';
                                     }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>