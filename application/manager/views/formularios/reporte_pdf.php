<style>
    .table-bordered th,
    .table-bordered td {
        border: 1px solid #ddd !important;
    }
    table {
        max-width: 100%;
        background-color: transparent;
    }
    th {
        text-align: left;
    }
    .table {
        width: 100%;
        margin-bottom: 20px;
    }
    .table > thead > tr > th,
    .table > tbody > tr > th,
    .table > tfoot > tr > th,
    .table > thead > tr > td,
    .table > tbody > tr > td,
    .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .table > thead > tr > th {
        vertical-align: bottom;
        border-bottom: 2px solid #ddd;
    }
    .table > caption + thead > tr:first-child > th,
    .table > colgroup + thead > tr:first-child > th,
    .table > thead:first-child > tr:first-child > th,
    .table > caption + thead > tr:first-child > td,
    .table > colgroup + thead > tr:first-child > td,
    .table > thead:first-child > tr:first-child > td {
        border-top: 0;
    }
    .table > tbody + tbody {
        border-top: 2px solid #ddd;
    }
    .table .table {
        background-color: #fff;
    }
    .table-condensed > thead > tr > th,
    .table-condensed > tbody > tr > th,
    .table-condensed > tfoot > tr > th,
    .table-condensed > thead > tr > td,
    .table-condensed > tbody > tr > td,
    .table-condensed > tfoot > tr > td {
        padding: 5px;
    }
    .table-bordered {
        border: 1px solid #ddd;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > tbody > tr > th,
    .table-bordered > tfoot > tr > th,
    .table-bordered > thead > tr > td,
    .table-bordered > tbody > tr > td,
    .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > thead > tr > td {
        border-bottom-width: 2px;
    }
    .table-striped > tbody > tr:nth-child(odd) > td,
    .table-striped > tbody > tr:nth-child(odd) > th {
        background-color: #f9f9f9;
    }
    .table-hover > tbody > tr:hover > td,
    .table-hover > tbody > tr:hover > th {
        background-color: #f5f5f5;
    }
    table col[class*="col-"] {
        position: static;
        display: table-column;
        float: none;
    }
    table td[class*="col-"],
    table th[class*="col-"] {
        position: static;
        display: table-cell;
        float: none;
    }
    .table > thead > tr > td.active,
    .table > tbody > tr > td.active,
    .table > tfoot > tr > td.active,
    .table > thead > tr > th.active,
    .table > tbody > tr > th.active,
    .table > tfoot > tr > th.active,
    .table > thead > tr.active > td,
    .table > tbody > tr.active > td,
    .table > tfoot > tr.active > td,
    .table > thead > tr.active > th,
    .table > tbody > tr.active > th,
    .table > tfoot > tr.active > th {
        background-color: #f5f5f5;
    }
    .table-hover > tbody > tr > td.active:hover,
    .table-hover > tbody > tr > th.active:hover,
    .table-hover > tbody > tr.active:hover > td,
    .table-hover > tbody > tr.active:hover > th {
        background-color: #e8e8e8;
    }
    .table > thead > tr > td.success,
    .table > tbody > tr > td.success,
    .table > tfoot > tr > td.success,
    .table > thead > tr > th.success,
    .table > tbody > tr > th.success,
    .table > tfoot > tr > th.success,
    .table > thead > tr.success > td,
    .table > tbody > tr.success > td,
    .table > tfoot > tr.success > td,
    .table > thead > tr.success > th,
    .table > tbody > tr.success > th,
    .table > tfoot > tr.success > th {
        background-color: #dff0d8;
    }
    .table-hover > tbody > tr > td.success:hover,
    .table-hover > tbody > tr > th.success:hover,
    .table-hover > tbody > tr.success:hover > td,
    .table-hover > tbody > tr.success:hover > th {
        background-color: #d0e9c6;
    }
    .table > thead > tr > td.info,
    .table > tbody > tr > td.info,
    .table > tfoot > tr > td.info,
    .table > thead > tr > th.info,
    .table > tbody > tr > th.info,
    .table > tfoot > tr > th.info,
    .table > thead > tr.info > td,
    .table > tbody > tr.info > td,
    .table > tfoot > tr.info > td,
    .table > thead > tr.info > th,
    .table > tbody > tr.info > th,
    .table > tfoot > tr.info > th {
        background-color: #d9edf7;
    }
    .table-hover > tbody > tr > td.info:hover,
    .table-hover > tbody > tr > th.info:hover,
    .table-hover > tbody > tr.info:hover > td,
    .table-hover > tbody > tr.info:hover > th {
        background-color: #c4e3f3;
    }
    .table > thead > tr > td.warning,
    .table > tbody > tr > td.warning,
    .table > tfoot > tr > td.warning,
    .table > thead > tr > th.warning,
    .table > tbody > tr > th.warning,
    .table > tfoot > tr > th.warning,
    .table > thead > tr.warning > td,
    .table > tbody > tr.warning > td,
    .table > tfoot > tr.warning > td,
    .table > thead > tr.warning > th,
    .table > tbody > tr.warning > th,
    .table > tfoot > tr.warning > th {
        background-color: #fcf8e3;
    }
    .table-hover > tbody > tr > td.warning:hover,
    .table-hover > tbody > tr > th.warning:hover,
    .table-hover > tbody > tr.warning:hover > td,
    .table-hover > tbody > tr.warning:hover > th {
        background-color: #faf2cc;
    }
    .table > thead > tr > td.danger,
    .table > tbody > tr > td.danger,
    .table > tfoot > tr > td.danger,
    .table > thead > tr > th.danger,
    .table > tbody > tr > th.danger,
    .table > tfoot > tr > th.danger,
    .table > thead > tr.danger > td,
    .table > tbody > tr.danger > td,
    .table > tfoot > tr.danger > td,
    .table > thead > tr.danger > th,
    .table > tbody > tr.danger > th,
    .table > tfoot > tr.danger > th {
        background-color: #f2dede;
    }
    .table-hover > tbody > tr > td.danger:hover,
    .table-hover > tbody > tr > th.danger:hover,
    .table-hover > tbody > tr.danger:hover > td,
    .table-hover > tbody > tr.danger:hover > th {
        background-color: #ebcccc;
    }
    @media (max-width: 767px) {
        .table-responsive {
            width: 100%;
            margin-bottom: 15px;
            overflow-x: scroll;
            overflow-y: hidden;
            -webkit-overflow-scrolling: touch;
            -ms-overflow-style: -ms-autohiding-scrollbar;
            border: 1px solid #ddd;
        }
        .table-responsive > .table {
            margin-bottom: 0;
        }
        .table-responsive > .table > thead > tr > th,
        .table-responsive > .table > tbody > tr > th,
        .table-responsive > .table > tfoot > tr > th,
        .table-responsive > .table > thead > tr > td,
        .table-responsive > .table > tbody > tr > td,
        .table-responsive > .table > tfoot > tr > td {
            white-space: nowrap;
        }
        .table-responsive > .table-bordered {
            border: 0;
        }
        .table-responsive > .table-bordered > thead > tr > th:first-child,
        .table-responsive > .table-bordered > tbody > tr > th:first-child,
        .table-responsive > .table-bordered > tfoot > tr > th:first-child,
        .table-responsive > .table-bordered > thead > tr > td:first-child,
        .table-responsive > .table-bordered > tbody > tr > td:first-child,
        .table-responsive > .table-bordered > tfoot > tr > td:first-child {
            border-left: 0;
        }
        .table-responsive > .table-bordered > thead > tr > th:last-child,
        .table-responsive > .table-bordered > tbody > tr > th:last-child,
        .table-responsive > .table-bordered > tfoot > tr > th:last-child,
        .table-responsive > .table-bordered > thead > tr > td:last-child,
        .table-responsive > .table-bordered > tbody > tr > td:last-child,
        .table-responsive > .table-bordered > tfoot > tr > td:last-child {
            border-right: 0;
        }
        .table-responsive > .table-bordered > tbody > tr:last-child > th,
        .table-responsive > .table-bordered > tfoot > tr:last-child > th,
        .table-responsive > .table-bordered > tbody > tr:last-child > td,
        .table-responsive > .table-bordered > tfoot > tr:last-child > td {
            border-bottom: 0;
        }
    }
    .panel > .table-bordered,
    .panel > .table-responsive > .table-bordered {
        border: 0;
    }
    .panel > .table-bordered > thead > tr > th:first-child,
    .panel > .table-responsive > .table-bordered > thead > tr > th:first-child,
    .panel > .table-bordered > tbody > tr > th:first-child,
    .panel > .table-responsive > .table-bordered > tbody > tr > th:first-child,
    .panel > .table-bordered > tfoot > tr > th:first-child,
    .panel > .table-responsive > .table-bordered > tfoot > tr > th:first-child,
    .panel > .table-bordered > thead > tr > td:first-child,
    .panel > .table-responsive > .table-bordered > thead > tr > td:first-child,
    .panel > .table-bordered > tbody > tr > td:first-child,
    .panel > .table-responsive > .table-bordered > tbody > tr > td:first-child,
    .panel > .table-bordered > tfoot > tr > td:first-child,
    .panel > .table-responsive > .table-bordered > tfoot > tr > td:first-child {
        border-left: 0;
    }
    .panel > .table-bordered > thead > tr > th:last-child,
    .panel > .table-responsive > .table-bordered > thead > tr > th:last-child,
    .panel > .table-bordered > tbody > tr > th:last-child,
    .panel > .table-responsive > .table-bordered > tbody > tr > th:last-child,
    .panel > .table-bordered > tfoot > tr > th:last-child,
    .panel > .table-responsive > .table-bordered > tfoot > tr > th:last-child,
    .panel > .table-bordered > thead > tr > td:last-child,
    .panel > .table-responsive > .table-bordered > thead > tr > td:last-child,
    .panel > .table-bordered > tbody > tr > td:last-child,
    .panel > .table-responsive > .table-bordered > tbody > tr > td:last-child,
    .panel > .table-bordered > tfoot > tr > td:last-child,
    .panel > .table-responsive > .table-bordered > tfoot > tr > td:last-child {
        border-right: 0;
    }
    .panel > .table-bordered > thead > tr:first-child > td,
    .panel > .table-responsive > .table-bordered > thead > tr:first-child > td,
    .panel > .table-bordered > tbody > tr:first-child > td,
    .panel > .table-responsive > .table-bordered > tbody > tr:first-child > td,
    .panel > .table-bordered > thead > tr:first-child > th,
    .panel > .table-responsive > .table-bordered > thead > tr:first-child > th,
    .panel > .table-bordered > tbody > tr:first-child > th,
    .panel > .table-responsive > .table-bordered > tbody > tr:first-child > th {
        border-bottom: 0;
    }
    .panel > .table-bordered > tbody > tr:last-child > td,
    .panel > .table-responsive > .table-bordered > tbody > tr:last-child > td,
    .panel > .table-bordered > tfoot > tr:last-child > td,
    .panel > .table-responsive > .table-bordered > tfoot > tr:last-child > td,
    .panel > .table-bordered > tbody > tr:last-child > th,
    .panel > .table-responsive > .table-bordered > tbody > tr:last-child > th,
    .panel > .table-bordered > tfoot > tr:last-child > th,
    .panel > .table-responsive > .table-bordered > tfoot > tr:last-child > th {
        border-bottom: 0;
    }
    .panel > .table-responsive {
        margin-bottom: 0;
        border: 0;
    }
    /* DataTables Overrides */

    table.dataTable thead .sorting,
    table.dataTable thead .sorting_asc:after,
    table.dataTable thead .sorting_desc,
    table.dataTable thead .sorting_asc_disabled,
    table.dataTable thead .sorting_desc_disabled {
        background: transparent;
    }

    table.dataTable thead .sorting_asc:after {
        content: "\f0de";
        float: right;
        font-family: fontawesome;
    }

    table.dataTable thead .sorting_desc:after {
        content: "\f0dd";
        float: right;
        font-family: fontawesome;
    }

    table.dataTable thead .sorting:after {
        content: "\f0dc";
        float: right;
        font-family: fontawesome;
        color: rgba(50,50,50,.5);
    }
</style>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Formulario #<?= $formulario->rir_id; ?></h1>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <!-- <tr>
                                    <th style="text-align: left">Fecha Informe</th>
                                    <td><?//= $fecha_informe; ?></td>
                                </tr> -->
                                <tr>
                                    <th>Creador</th>
                                    <td><?= !empty($formulario->usr_nombre) ? $formulario->usr_nombre : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Fecha Creación</th>
                                    <td><?= !empty($formulario->rir_fecha_informe) ? $formulario->rir_fecha_informe : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Tipo de Evento</th>
                                    <td><?= !empty($formulario->rig_tipo_evento) ? $formulario->rig_tipo_evento : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Tipo de Incidente</th>
                                    <td><?= !empty($formulario->rig_tipo_incidente) ? $formulario->rig_tipo_incidente : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Fecha Evento</th>
                                    <td><?= !empty($formulario->rig_fecha_evento) ? $formulario->rig_fecha_evento : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Hora Evento</th>
                                    <td><?= !empty($formulario->rig_hora_evento) ? $formulario->rig_hora_evento : 'N/A' ; ?></td>
                                </tr>
                                <tr>
                                    <th>Clasificación Incidente</th>
                                    <td><?= !empty($formulario->rig_descripcion) ? $formulario->rig_descripcion : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Centro de Trabajo</th>
                                    <td><?= !empty($formulario->nombre_centro_trabajo) ? $formulario->nombre_centro_trabajo : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Área</th>
                                    <td><?= !empty($formulario->nombre_area) ? $formulario->nombre_area : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Lugar Específico</th>
                                    <td><?= !empty($formulario->rig_lugar_especifico) ? $formulario->rig_lugar_especifico : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Descripción del Incidente</th>
                                    <td><?= !empty($formulario->rig_descripcion) ? $formulario->rig_descripcion : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Testigo 1</th>
                                    <td><?= !empty($formulario->rig_testigo_1) ? $formulario->rig_testigo_1 : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Testigo 2</th>
                                    <td><?= !empty($formulario->rig_testigo_2) ? $formulario->rig_testigo_2 : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Testigo 3</th>
                                    <td><?= !empty($formulario->rig_testigo_3) ? $formulario->rig_testigo_3 : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Nombre Persona</th>
                                    <td><?= !empty($formulario->rip_nombre_completo) ? $formulario->rip_nombre_completo : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Rut Persona</th>
                                    <td><?= !empty($formulario->rip_rut) ? $formulario->rip_rut : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Sexo</th>
                                    <td><?= !empty($formulario->rip_sexo) ? $formulario->rip_sexo : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Edad</th>
                                    <td><?= !empty($formulario->rip_edad) ? $formulario->rip_edad : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>BP</th>
                                    <td><?= !empty($formulario->rip_bp) ? $formulario->rip_bp : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Razón Social Persona</th>
                                    <td><?= !empty($formulario->razon_social_persona) ? $formulario->razon_social_persona : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Lesión Aparente</th>
                                    <td><?= !empty($formulario->lesion_persona) ? $formulario->lesion_persona : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Parte Lesionada</th>
                                    <td><?= !empty($formulario->parte_lesionada_persona) ? $formulario->parte_lesionada_persona : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Área Persona</th>
                                    <td><?= !empty($formulario->area_persona) ? $formulario->area_persona : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Jefe Directo Persona</th>
                                    <td><?= !empty($formulario->rip_jefe_directo) ? $formulario->rip_jefe_directo : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Responsable Área</th>
                                    <td><?= !empty($formulario->rip_responsable_area) ? $formulario->rip_responsable_area : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Turno Persona</th>
                                    <td><?= !empty($formulario->turno_persona) ? $formulario->turno_persona : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Licencias. cursos o habilitaciones específicas requeridas</th>
                                    <td><?= !empty($formulario->rip_otros) ? $formulario->rip_otros : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Proceso</th>
                                    <td><?= !empty($formulario->rie_proceso) ? $formulario->rie_proceso : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Actividad Genérica</th>
                                    <td><?= !empty($formulario->rie_actividad_generica) ? $formulario->rie_actividad_generica : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Tarea Específica</th>
                                    <td><?= !empty($formulario->rie_tarea_especifica) ? $formulario->rie_tarea_especifica : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Frecuencia Tarea</th>
                                    <td><?= !empty($formulario->rie_frecuencia_tarea) ? $formulario->rie_frecuencia_tarea : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Estándar Aplicable</th>
                                    <td><?= !empty($formulario->rie_estandar) ? $formulario->rie_estandar : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Maquinarias</th>
                                    <td><?= !empty($formulario->rie_maquinarias) ? $formulario->rie_maquinarias : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Equipos</th>
                                    <td><?= !empty($formulario->rie_equipos) ? $formulario->rie_equipos : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Herramientas</th>
                                    <td><?= !empty($formulario->rie_herramientas) ? $formulario->rie_herramientas : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Materiales</th>
                                    <td><?= !empty($formulario->rie_materiales) ? $formulario->rie_materiales : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Condiciones Climáticas Ambiente</th>
                                    <td><?= !empty($formulario->descripcion_condicion_climatica) ? $formulario->descripcion_condicion_climatica : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Lugar de Trabajo</th>
                                    <td><?= !empty($formulario->centro_trabajo_clima) ? $formulario->centro_trabajo_clima : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Actos</th>
                                    <td><?= !empty($formulario->actos) ? $formulario->actos : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Condiciones</th>
                                    <td><?= !empty($formulario->condiciones) ? $formulario->condiciones : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Latitud</th>
                                    <td><?= !empty($formulario->rig_latitude) ? $formulario->rig_latitude : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Longitud</th>
                                    <td><?= !empty($formulario->rig_longitude) ? $formulario->rig_longitude : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Dirección</th>
                                    <td><?= !empty($formulario->rig_direccion) ? $formulario->rig_direccion : 'N/A'; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php if (!empty($files)){
                        echo "<p><b>Archivos adjuntos</b></p>";
                    } ?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <?php if (empty($files)) {
                                        echo '<p><b>No hay archivos adjuntos para este formulario.</b></p>';
                                    }
                                else {
                                    echo '<thead>';
                                    echo '<tr>';
                                    echo '<th>ID</th>';
                                    echo '<th>Tipo</th>';
                                    echo '<th>Link</th>';
                                    echo '</tr>';
                                    echo '</thead>';
                                    echo '<tbody>';
                                
                                    foreach ($files as $file) {
                                        echo '<tr>';
                                        echo '<td><b>' . $file->multi_id . '</b></td>';
                                        echo '<td><b>' . $file->multi_tipo . '</b></td>';
                                        echo '<td><b><a href="' . str_replace("/intranet","", base_url()) . "multimedia/" . $formulario->rir_id . 
                                                '/' . $file->multi_path . '">Ver Archivo</a></b></td>';
                                        echo '</tr>';
                                     }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>