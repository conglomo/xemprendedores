<?php
/* @var $this formularios */
/* @var $model formulario_model */
?>
<div id="page-wrapper">
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Formularios</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="<?= site_url("formularios/export_excel"); ?>">
                <button type="button" class="btn btn-info">Exportar Todo</button>
            </a>
        </div>
        <!-- <div class="col-lg-12">
            <input type="button" value="Imprimir" onclick="window.print();">
        </div> -->
    </div>
    <br>
    <div class="row">
    	<div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="formularios-table">
                <thead>
                    <tr>
                        <th>Fecha Creación</th>
                        <th>Tipo Evento</th>
                        <th>Creador</th>
                        <th>Fecha Evento</th>
                        <!--<th>Dirección</th> -->
                        <th>Accidentado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($get_all as $formulario) {
                        echo '<tr>';
                        echo '<td>' . $formulario->rir_fecha_informe . '</td>';
                        if (!empty($formulario->rig_tipo_evento)){
                            echo '<td>' . $formulario->rig_tipo_evento . '</td>';
                        }
                        else {
                            echo '<td>' . '-' . '</td>';
                        }
                        echo '<td>' . $formulario->usr_nombre . '</td>';
                        echo '<td>' . $formulario->rig_fecha_evento . ' ' . $formulario->rig_hora_evento . '</td>';
                        /*if (!empty($formulario->rig_direccion)){
                            echo '<td>' . $formulario->rig_direccion . '</td>';
                        }
                        else {
                            echo '<td>' . '-' . '</td>';
                        } */
                        echo '<td>' . $formulario->rip_nombre_completo . '</td>';
                        echo '<td>
                                <a href="' . site_url("formularios/view/" . $formulario->rir_id) . '" title="Ver" target="_blank"><i class="fa fa-search fa-fw"></i></a>
                                <a href="' . site_url("formularios/generar_pdf/" . $formulario->rir_id) . '" title="Generar PDF" target="_blank"><i class="fa fa-file-o fa-fw"></i></a>
                              </td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
                <!-- <tfoot>
                    <tr>
                        <th><input type="text" name="search_tipo_informe" id="search_tipo_informe" value="Buscar..." class="search_init"/></th>
                        <th><input type="text" name="search_fecha_informe" id="search_fecha_informe" value="Buscar..." class="search_init"/></th>
                        <th><input type="text" name="search_usuario" value="Buscar..." class="search_init" /></th>
                        <th><input type="text" name="search_fecha_accidente" value="Buscar..." class="search_init" /></th>
                        <th><input type="text" name="search_centro_trabajo" value="Buscar..." class="search_init" /></th>
                        <th><input type="text" name="search_nombre_completo" value="Buscar..." class="search_init" /></th>
                        <th><a href="#" title="Limpiar Búsqueda"><i class="fa fa-eraser fa-fw"></i></a></th>
                    </tr>
                </tfoot> -->
            </table>
            <br>
    	</div>
    </div>
</div>

<script type='text/javascript'>
    $(document).ready(function() {

        $(".deleteRow").click(function(event) {
            var href = $(this).attr("href");
            var btn = this;

            $.ajax({
                type: "GET",
                url: href,
                beforeSend: function() {
                    if (!confirm("Está a punto de eliminar un registro ; ¿Desea continuar?")) {
                        return false;
                    }
                },
                success: function(msgd) {
                    $(btn).closest('tr').fadeOut("slow");
                }
            });
            event.preventDefault();
        });
    });
</script>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    $('#formularios-table').dataTable({
       "bProcessing": true,
       "bSort": false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
        },
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "http://cdnjs.cloudflare.com/ajax/libs/datatables-tabletools/2.1.5/swf/copy_csv_xls_pdf.swf",
            "aButtons":[
                //{"sExtends": "xls","sButtonText": "Exportar a Excel", "sFileName": "usuarios.xls", "mColumns": [0, 1]},
                //{"sExtends": "pdf","sButtonText": "Exportar a PDF", "sFileName": "usuarios.pdf", "mColumns": [0, 1]},
                {"sExtends": "print","sButtonText": "Imprimir",  "mColumns": [0, 1]}
            ]
        },
        //"bPaginate": false,
        //"bJQueryUI": true,
        //"bAutoWidth": false,
        //"aoColumns": [{"sWidth":"10px"},{"sWidth":"20%"},{"sWidth":"20%"},{"sWidth":"20%"},{"sWidth":"20%"},{"sWidth":"20%"},{"sWidth":"20%"}]
    });
} );
</script>