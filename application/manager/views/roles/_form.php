<?php
/* @var $this roles */
/* @var $model role_model */
?>
<?php
/* @var $this roles */
/* @var $model role_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <?= ($this->router->method == "create") ? '<h1 class="page-header">Generar Evento</h1>' : '<h1 class="page-header">Editar Evento</h1>'; ?>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= ($this->router->method == "create") ? 'Registrar nuevo evento en Agenda' : 'Editar evento en Agenda'; ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?= ($this->router->method == "create") ? form_open("roles/create") : form_open("roles/update"); ?>
                        <div class="col-lg-6">
                            <?php if (validation_errors() != FALSE) { ?>
                                <div class="alert alert-danger">
                                    <?= validation_errors(); ?>
                                </div>  
                            <?php } ?>

                            <input type='text' hidden id="input_id" name="input_id" value="<?= isset($id) ? $id : ''; ?>"/>
                            
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input class="form-control" placeholder="Nombre del Rol" id="input_nombre" name="input_nombre" 
                                       value="<?= isset($nombre) ? $nombre : ''; ?>">
                            </div>

                            <button type="submit" class="btn btn-default">
                                <?= ($this->router->method == "create") ? "Crear" : "Editar"; ?>
                            </button>
                            <button type="reset" class="btn btn-default">Limpiar Formulario</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>