<?php
/* @var $this roles */
/* @var $model role_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <?= ($this->router->method == "crear_evento") ? '<h1 class="page-header">Generar Evento</h1>' : '<h1 class="page-header">Editar Evento</h1>'; ?>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= ($this->router->method == "crear_evento") ? 'Registrar nuevo evento en Agenda' : 'Editar evento en Agenda'; ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?= ($this->router->method == "crear_evento") ? form_open("agenda/crear_evento") : form_open("agenda/editar_evento"); ?>
                        <div class="col-lg-6">
                            <?php if (validation_errors() != FALSE) { ?>
                                <div class="alert alert-danger">
                                    <?= validation_errors(); ?>
                                </div>  
                            <?php } ?>
                            
                            <input type='text' hidden id="input_id" name="input_id" value="<?= isset($id) ? $id : ''; ?>"/>
                            
                            <div class="form-group">
                                <label>Fecha:</label> 
                                <div class='input-group date' id='fechaPicker'>
                                    <input type='text' class="form-control" id="input_fecha" name="input_fecha" value="<?= isset($fecha) ? $fecha : ''; ?>"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Hora de Inicio:</label>
                                <div class='input-group date' id='inicioPicker'>
                                    <input type="text" class="form-control" id="timepicker" name="input_inicio" value="<?= isset($inicio) ? substr($inicio,0,-3) : ''; ?>">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Hora de Término:</label>
                                <div class='input-group date' id='finPicker'>
                                    <input type="text" class="form-control" id="input_fin" name="input_fin" value="<?= isset($fin) ? substr($fin,0,-3) : ''; ?>">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Descripción:</label>
                                <input class="form-control" placeholder="Descripción de la entrevista" id="input_descripcion" name="input_descripcion" 
                                       value="<?= isset($descripcion) ? $descripcion : ''; ?>">
                            </div>

                            <div class="form-group">
                                <label>Usuario Asignado: </label>                                
                                <select class="form-control" id="input_asignado" name="input_asignado">
                                    <?php for ($i = 0; $i < sizeof($usuarios); $i++): ?>
                                        <option value="<?= $usuarios[$i]->usr_id ?>" <?= (isset($usr_id) && $usr_id == $usuarios[$i]->usr_id) ? "selected" : ""; ?>><?= $usuarios[$i]->usr_nombre . ' ' . $usuarios[$i]->usr_apellidos; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Entrevistado:</label>
                                <input class="form-control" placeholder="Nombre del entrevistado" id="input_entrevistado" name="input_entrevistado" value="<?= isset($contacto) ? $contacto : ''; ?>">
                            </div>
                            <div class="form-group">
                                <label>Área del entrevistado</label>
                                <select class="form-control" id="area_entrevistado" name="area_entrevistado">
                                    <?php for ($i = 0; $i < sizeof($areas); $i++): ?>
                                        <option value="<?= $areas[$i]->area_id; ?>" <?= (isset($area_id) && $area_id == $areas[$i]->area_id) ? "selected" : ""; ?>><?= $areas[$i]->area_nombre_area; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            
                            <button type="submit" class="btn btn-default">
                                <?= ($this->router->method == "crear_evento") ? "Crear Evento" : "Editar Evento"; ?>
                            </button>
                            <button type="reset" class="btn btn-default">reiniciar</button>
                            <?= form_close(); ?>
                        </div>           
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>