<?php
/* @var $this roles */
/* @var $model role_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Roles</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="<?= site_url("roles/create"); ?>">
                <button type="button" class="btn btn-info">Crear Nuevo Rol</button>
            </a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="roles-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($get_all as $formulario) {
                        echo '<tr>';
                        echo '<td>' . $formulario->role_id . '</td>';
                        echo '<td>' . $formulario->role_nombre_rol . '</td>';
                        echo '<td>
                                <a href="' . site_url("roles/view/" . $formulario->role_id) . '" title="Ver"><i class="fa fa-search fa-fw"></i></a>
                                <a href="' . site_url("roles/update/" . $formulario->role_id) . '" title="Editar"><i class="fa fa-edit fa-fw"></i></a>
                                <a href="' . site_url("roles/delete/" . $formulario->role_id) . '" title="Eliminar" class="deleteRow"><i class="fa fa-times fa-fw"></i></a>
                              </td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th><input type="text" name="search_id" value="Buscar..." class="search_init"/></th>
                        <th><input type="text" name="search_nombre" value="Buscar..." class="search_init" /></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(document).ready(function() {

        $(".deleteRow").click(function(event) {
            var href = $(this).attr("href")
            var btn = this;

            $.ajax({
                type: "GET",
                url: href,
                beforeSend: function() {
                    if (!confirm("Está a punto de eliminar un registro ; ¿Desea continuar?")) {
                        return false;
                    }
                },
                success: function(msgd) {
                    $(btn).closest('tr').fadeOut("slow");
                }
            });
            event.preventDefault();
        })
    });
</script>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    $('#roles-table').dataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
        },
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "http://cdnjs.cloudflare.com/ajax/libs/datatables-tabletools/2.1.5/swf/copy_csv_xls_pdf.swf",
            "aButtons":[
                {"sExtends": "xls","sButtonText": "Exportar a Excel", "sFileName": "usuarios.xls", "mColumns": [0, 1]},
                {"sExtends": "pdf","sButtonText": "Exportar a PDF", "sFileName": "usuarios.pdf", "mColumns": [0, 1]},
                {"sExtends": "print","sButtonText": "Imprimir",  "mColumns": [0, 1]}
            ]
        }
    });
} );
</script>