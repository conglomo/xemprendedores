<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body style="font: 14px 'Arial', sans-serif; color: #333333">
        <div>
            <div style="background-color: rgb(31,91,97); text-align: right">
                <!-- <img src="cid:1001" alt="aa"/> -->
            </div>
            <div style="margin-left: 10px">
                <p style="text-align: left; margin-top: 50px;"><strong>Estimado <?php echo $entrevistado; ?>,</strong></p>
                <!-- <h1 style="font-size: 24px; font-family: 'Arial', sans-serif;margin-top: 0px;">Nuevo evento creado</h1> -->
                <p>
                    <?php echo $creador; ?> lo(a) ha incluido en la siguiente actividad:
                    <br/>
                    Fecha: <?php echo $fecha; ?>
                    <br/>
                    Hora de inicio: <?php echo $hora_inicio; ?>
                    <br/>
                    Hora de término: <?php echo $hora_termino; ?>
                    <br/>
                    Área: <?php echo $area; ?>
                    <br/>
                    Descripción: <?php echo $descripcion; ?>
                    <br/>
                    <!--<br/>Tú código de verifación es:<br/><br/>
                    <strong></strong> -->
                </p>
            </div>
            <div style="font-size: 11px; margin: 20px 0px; text-align: center; background-color: gray;">
                Este correo ha sido enviado de forma automatica. Por favor, no respondas a este mensaje. Si tienes alguna duda, favor de comunicarse con <?php echo $creador; ?>.
            </div>
        </div>
    </body>
</html>