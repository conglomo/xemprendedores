<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Actividad #<?= $id; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Fecha</th>
                                    <td><?= $fecha; ?></td>
                                </tr>
                                <tr>
                                    <th>Hora Inicio</th>
                                    <td><?= $inicio; ?></td>
                                </tr>
                                <tr>
                                    <th>Hora Término</th>
                                    <td><?= $fin; ?></td>
                                </tr>
                                <tr>
                                    <th>Tipo de actividad</th>
                                    <td><?= !empty($tipo_actividad) ? $tipo_actividad : 'N/A'; ?></td>
                                </tr>
                                <tr>
                                    <th>Estado</th>
                                    <td><?= $estado; ?></td>
                                </tr>
                                <tr>
                                    <th>Entrevistado</th>
                                    <td><?= $contacto; ?></td>
                                </tr>
                                <tr>
                                    <th>Correo entrevistado</th>
                                    <td><?= isset($contacto_correo) ? $contacto_correo : "-" ; ?></td>
                                </tr>
                                <tr>
                                    <th>Descripción</th>
                                    <td><?= $descripcion; ?></td>
                                </tr>
                                <tr>
                                    <th>Usuario Asignado</th>
                                    <td><?= $usuario; ?></td>
                                </tr>
                                <tr>
                                    <th>Área</th>
                                    <td><?= $area; ?></td>
                                </tr>
                                <tr>
                                    <th>Comentarios</th>
                                    <td><?= $comentarios; ?></td>
                                </tr>
                                <tr>
                                    <th>Actividad en terreno</th>
                                    <td><?= !empty($actividad_terreno) ? 'Sí' : 'No'; ?></td>
                                </tr>
                                <tr>
                                    <th>ID Padre</th>
                                    <td><?= !empty($padre) ? $padre : 'N/A'; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php if (!empty($entrevistados)){
                        echo "<p><b>Entrevistados</b></p>";
                    } ?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <?php if (empty($entrevistados)) {
                                        echo '<p><b>No hay entrevistados para esta actividad.</b></p>';
                                    }
                                else {
                                    echo '<thead>';
                                    echo '<tr>';
                                    echo '<th>Nombre</th>';
                                    echo '<th>Correo</th>';
                                    echo '</tr>';
                                    echo '</thead>';
                                    echo '<tbody>';
                                
                                    foreach ($entrevistados as $entrevistado) {
                                        echo '<tr>';
                                        echo '<td><b>' . $entrevistado->nombre . '</b></td>';
                                        echo '<td><b>' . $entrevistado->email . '</b></td>';
                                        echo '</tr>';
                                     }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>