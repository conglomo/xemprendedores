<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Agenda</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="<?= site_url("agenda/crear_evento/"); ?>">
                <button type="button" class="btn btn-info">Crear Actividad</button>
            </a>
        </div>
    </div>
    <br>
    <div class="row">
        <table border="0" cellspacing="5" cellpadding="5">
        <tbody><tr>
            <td>Desde:</td>
            <td>
                <div class='input-group date' id='fechaPickerDesde'>
                    <input type="text" id="date_min" name="date_min">
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </span>
                </div>
            </td>
        </tr>
        <tr>
            <td>Hasta:</td>
            <td>
                <div class='input-group date' id='fechaPickerHasta'>
                    <input type="text" id="date_max" name="date_max">
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </span>
                </div>
            </td>
        </tr>
        <!-- <tr>
            <td>Estado:</td>
            <td>
                <select class="form-control" id="input_estado" name="input_estado">
                    <option value="Todos" selected>Todos</option>
                    <option value="Pendiente">Pendiente</option>
                    <option value="Realizada">Realizada</option>
                    <option value="No Realizada">No Realizada</option>
                    <option value="Suprimida">Suprimida</option>
                </select>
                </td>
        </tr> -->
    </tbody></table>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="agenda-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Fecha</th>
                        <th>Inicio</th>
                        <th>Fin</th>
                        <th>Descripción</th>
                        <?php
                            if ($this->session->userdata('usrrole') != 2) {
                                echo '<th>U. Asignado</th>';
                            }
                        ?>
                        <!-- <th>U. Asignado</th> -->
                        <th>Entrevistado</th>
                        <!-- <th>Correo entrevistado</th> -->
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php
                    foreach ($eventos as $evento) {
                        echo '<tr>';
                        echo '<td>' . $evento->agen_id . '</td>';
                        echo '<td>' . $evento->agen_fecha . '</td>';
                        echo '<td>' . $evento->agen_hora_inicio . '</td>';
                        echo '<td>' . $evento->agen_hora_fin . '</td>';
                        echo '<td>' . $evento->agen_descripcion . '</td>';
                        if ($this->session->userdata('usrrole') != 2) {
                            echo '<td>' . $evento->usr_nombre . '</td>';
                        }
                        echo '<td>' . $evento->agen_contacto_area . '</td>';
//                        if (isset($evento->agen_contacto_correo)){
//                            echo '<td>' . $evento->agen_contacto_correo . '</td>';
//                        }
//                        else {
//                            echo '<td>' . "-". '</td>';
//                        }
//                        if ($evento->agen_comentarios != NULL || $evento->agen_comentarios != '') {
//                            echo '<td>Si</td>';
//                        } else {
//                            echo '<td>No</td>';
//                        }
                        echo '<td>' .  $evento->agen_estado  .'</td>';
                        echo '<td>';
                        echo '<a href="' . site_url("agenda/ver_evento/" . $evento->agen_id) . '" title="Ver"><i class="fa fa-search fa-fw"></i></a>';
                        if ($evento->agen_creador != NULL){
                            if ($this->session->userdata('usrid') == $evento->agen_creador) {
                                echo "<a href='".site_url("agenda/editar_evento/" . $evento->agen_id)."' title='Editar'><i class='fa fa-edit fa-fw'></i></a>";
                                echo "<a href='".site_url("agenda/eliminar_evento/" . $evento->agen_id)."' title='Eliminar' class='deleteRow'><i class='fa fa-times fa-fw'></i></a>";
                            }
                            else if ($this->session->userdata('usrrole') != 2 &&  $this->session->userdata('usrid') != $evento->agen_creador){
                                echo "<a href='".site_url("agenda/editar_evento/" . $evento->agen_id)."' title='Editar'><i class='fa fa-edit fa-fw'></i></a>";
                                echo "<a href='".site_url("agenda/eliminar_evento/" . $evento->agen_id)."' title='Eliminar' class='deleteRow'><i class='fa fa-times fa-fw'></i></a>";
                            }
                        }
                        else if ($this->session->userdata('usrrole') != 2){
                            echo "<a href='".site_url("agenda/editar_evento/" . $evento->agen_id)."' title='Editar'><i class='fa fa-edit fa-fw'></i></a>";
                            echo "<a href='".site_url("agenda/eliminar_evento/" . $evento->agen_id)."' title='Eliminar' class='deleteRow'><i class='fa fa-times fa-fw'></i></a>";
                        }
                        //echo ($evento->agen_fecha < date("Y-M-d")) ? "<a href='".site_url("agenda/editar_evento/" . $evento->agen_id)."' title='Editar'><i class='fa fa-edit fa-fw'></i></a>" : "";
                        //echo ($evento->agen_fecha < date("Y-M-d")) ? "<a href='".site_url("agenda/eliminar_evento/" . $evento->agen_id)."' title='Eliminar' class='deleteRow'><i class='fa fa-times fa-fw'></i></a>" : "";
                        echo '<a href="' . site_url("agenda/generar_pdf/" . $evento->agen_id) . '" title="Reporte PDF" target="_blank"><i class="fa fa-file-o fa-fw"></i></a>';
                        echo '</td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
                <!--<tfoot>
                    <tr>
                        <th><input type="text" name="search_fecha" value="Buscar..." class="search_init" style="width: 90px;" /></th>
                        <th><input type="text" name="search_hora_inicio" value="Buscar..." class="search_init" style="width: 90px;" /></th>
                        <th><input type="text" name="search_hora_fin" value="Buscar..." class="search_init" style="width: 90px;" /></th>
                        <th><input type="text" name="search_descripcion" value="Buscar..." class="search_init" /></th>
                        <th><input type="text" name="search_usuario" value="Buscar..." class="search_init" style="width: 100px;" /></th>
                        <th><input type="text" name="search_entrevistado" value="Buscar..." class="search_init" style="width: 100px;" /></th>
                    </tr>
                </tfoot> -->
            </table>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(document).ready(function() {

        $(".deleteRow").click(function(event) {
            var href = $(this).attr("href")
            var btn = this;

            $.ajax({
                type: "GET",
                url: href,
                beforeSend: function() {
                    if (!confirm("Está a punto de Eliminar un Instrumento inmediatamente al aceptar ; ¿Desea continuar?")) {
                        return false;
                    }
                },
                success: function(msgd) {
                        $(btn).closest('tr').fadeOut("slow");
                }
            });
            event.preventDefault();
        })
    });
</script>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    //$.datepicker.regional[""].dateFormat = 'dd/mm/yy';
    //$.datepicker.setDefaults($.datepicker.regional['']);
    $('#agenda-table').dataTable({
        "order": [[ 0, "desc" ]],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
        },
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "http://cdnjs.cloudflare.com/ajax/libs/datatables-tabletools/2.1.5/swf/copy_csv_xls_pdf.swf",
            "aButtons":[
                {"sExtends": "xls","sButtonText": "Exportar a Excel", "sFileName": "Listado_agenda.xls", "mColumns": [0, 1, 2, 3, 4, 5, 6, 7]},
                {"sExtends": "pdf","sButtonText": "Exportar a PDF", "sFileName": "Listado_agenda.pdf", "mColumns": [0, 1, 2, 3, 4, 5, 6, 7]},
                {"sExtends": "print","sButtonText": "Imprimir",  "mColumns": [0, 1, 2, 3, 4, 5, 6, 7]}
            ]
        }
        
    });
                
    /*$('#agenda-table').dataTable().columnFilter({ sPlaceHolder: "head:before",
                    aoColumns: [ { type: "date-range" }]});*/
} );

/* Custom filtering function which will search data in column four between two values */
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        
//        var min = parseInt( $('#date_min').val(), 10 );
//        var max = parseInt( $('#date_max').val(), 10 );
//        var age = parseFloat( data[0] ) || 0; // use data for the age column
// 
//        if ( ( isNaN( min ) && isNaN( max ) ) ||
//             ( isNaN( min ) && age <= max ) ||
//             ( min <= age   && isNaN( max ) ) ||
//             ( min <= age   && age <= max ) )
//        {
//            return true;
//        }
//        return false;
        // "date-range" is the id for my input
        //var dateRange = $('#date-range').attr("value");
 
        // parse the range from a single field into min and max, remove " - "
        //dateMin = dateRange.substring(0,4) + dateRange.substring(5,7)  + dateRange.substring(8,10);
        //dateMax = dateRange.substring(13,17) + dateRange.substring(18,20) + dateRange.substring(21,23);
        var dateMin = $('#date_min').val();
        var dateMax = $('#date_max').val();
        var estado_selected = $('#input_estado').val();
        console.log(estado_selected);
        
        //console.log(dateMin);
        //console.log(dateMax);
        
        if ( dateMin == '' || dateMax == '' ){
            return true;
        }
        else {
            dateMin = dateMin.substring(6,10) + '-' + dateMin.substring(3,5) + '-' + dateMin.substring(0,2);
            dateMax = dateMax.substring(6,10) + '-' + dateMax.substring(3,5) + '-' + dateMax.substring(0,2);


            dateMin = new Date(dateMin);
            dateMax = new Date(dateMax);



            // 4 here is the column where my dates are.
            var date = data[1];

            // remove the time stamp out of my date
            // 2010-04-11 20:48:22 -> 2010-04-11
            //date = date.substring(0,10);
            // remove the "-" characters
            // 2010-04-11 -> 20100411
            date = date.substring(6,10) + '-' + date.substring(3,5) + '-' + date.substring(0,2);
            date = new Date(date);

            //date = date.substring(0,4) + date.substring(5,7) + date.substring( 8,10 )

            // run through cases
            if ( dateMin == "" && date <= dateMax){
                return true;
            }
            else if ( dateMin == "" && date <= dateMax ){
                return true;
            }
            else if ( dateMin <= date && "" == dateMax ){
                return true;
            }
            else if ( dateMin <= date && date <= dateMax ){
                return true;
            }
            // all failed
            return false;
        }
        if (estado_selected != 'Todos'){
            var estado = data[7];
            return false;
        }
        
        
    }
);

$(document).ready(function() {
    var table = $('#agenda-table').DataTable();
     
    // Event listener to the two range filtering inputs to redraw on input
    $('#date_min, #date_max').keyup(function() {
        table.draw();
    });
    
    $('#input_estado').on('change',function() {
        console.log("Estado cambiado");
        table.draw();
    });
    
} );

//function filterByDate(){     
//    //$('#yourInputID').val(value);
//    var table = $('#agenda-table').DataTable();
//    table.draw();
//}
</script>
<script type="text/javascript">
        $(function() {
            $('#fechaPickerDesde').datetimepicker({
                pickTime: false,
                format: 'DD-MM-YYYY',
                autoclose: true,
                language: 'es'
            });
        });
</script>
<script type="text/javascript">
        $(function() {
            $('#fechaPickerHasta').datetimepicker({
                pickTime: false,
                format: 'DD-MM-YYYY',
                autoclose: true,
                language: 'es'
            });
        });
</script>