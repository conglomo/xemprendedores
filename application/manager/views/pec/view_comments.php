<?php
/* @var $this roles */
/* @var $model role_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= 'Comentarios de ' . $name . " #" . $id_pec; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <?php if (empty($comments)) {
                                        echo '<p><b>No hay comentarios para este documento.</b></p>';
                                    }
                                else {
                                    echo '<thead>';
                                    echo '<tr>';
                                    echo '<th>Título</th>';
                                    echo '<th>Comentario</th>';
                                    echo '<th>Fecha</th>';
                                    echo '<th>Acciones</th>';
                                    echo '</tr>';
                                    echo '</thead>';
                                    echo '<tbody>';
                                
                                    foreach ($comments as $comment) {
                                        echo '<tr>';
                                        echo '<td align="justify"><b>' . nl2br($comment->titulo) . '</b></td>';
                                        echo '<td align="justify">' . nl2br($comment->comentario) . '</td>';
                                        echo '<td>' . $comment->fecha . '</td>';
                                        echo '<td>
                                             <a href="' . site_url("pec/update_comment/" . $comment->id_comentario) . '" title="Editar"><i class="fa fa-edit fa-fw"></i></a>
                                             <a href="' . site_url("pec/delete_comment/" . $comment->id_comentario) . '" title="Eliminar" class="deleteRow"><i class="fa fa-times fa-fw"></i></a>
                                            </td>';
                                        echo '</tr>';
                                     }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($this->session->flashdata('result_ok') || $this->session->flashdata('result_failed')){
    echo '<div class="container">';
     echo '<div class="row">';
      echo '<div class="col-md-6">';
        if ($this->session->flashdata('result_ok')){
            echo '<div class="alert alert-success"><strong><span class="glyphicon glyphicon-ok"></span> ' . $this->session->flashdata('result_ok') . '</strong>';
        } else if ($this->session->flashdata('result_failed')) {
            echo '<div class="alert alert-danger"><span class="glyphicon glyphicon-alert"></span><strong> ' . $this->session->flashdata('result_failed') . '</strong></div>';
        }
      echo '</div>'; 
    echo '</div>';
    echo '</div>';
    } ?>   
  <div class="container">
  <br>
  <div class="row">
    <?= form_open("pec/ver_comentarios/" . $id_pec); ?>
    <div class="col-lg-6">
        <?php if (validation_errors() != FALSE) { ?>
            <div class="alert alert-danger">
                <?= validation_errors(); ?>
            </div>  
        <?php } ?>
        <div class="row">
            <label>Agregar Comentario</label>
        </div>
        <br>
        <div class="form-group">
        <label>Título:</label>
            <input class="form-control" placeholder="Ingrese título..." id="input_titulo" name="input_titulo" 
               value="" required>
        </div>
        <div class="form-group">
            <label>Comentario:</label>
            <textarea class="form-control" placeholder="Ingrese comentario..." id="input_comentario" name="input_comentario" 
                      maxlength="2000" rows="4" required></textarea>
        </div>
        
        <button type="submit" class="btn btn-default">Enviar
        </button>
    </div>
    <?= form_close(); ?>
</div>
  <br>
</div>
</div>
<script type='text/javascript'>
    $(document).ready(function() {

        $(".deleteRow").click(function(event) {
            var href = $(this).attr("href")
            var btn = this;

            $.ajax({
                type: "GET",
                url: href,
                beforeSend: function() {
                    if (!confirm("¡Está a punto de eliminar este comentario! ¿Desea continuar?")) {
                        return false;
                    }
                },
                success: function(msgd) {
                        $(btn).closest('tr').fadeOut("slow");
                }
            });
            event.preventDefault();
        })
    });
</script>
