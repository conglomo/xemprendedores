<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Evento #<?= $id; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Nombre</th>
                                    <td><?= $fecha; ?></td>
                                </tr>
                                <tr>
                                    <th>Descripción</th>
                                    <td><?= $inicio; ?></td>
                                </tr>
                                <tr>
                                    <th>Hora Término</th>
                                    <td><?= $fin; ?></td>
                                </tr>
                                <tr>
                                    <th>Entrevistado</th>
                                    <td><?= $contacto; ?></td>
                                </tr>
                                <tr>
                                    <th>Correo entrevistado</th>
                                    <td><?= isset($contacto_correo) ? $contacto_correo : "-" ; ?></td>
                                </tr>
                                <tr>
                                    <th>Descripción</th>
                                    <td><?= $descripcion; ?></td>
                                </tr>
                                <tr>
                                    <th>Usuario Asignado</th>
                                    <td><?= $usuario; ?></td>
                                </tr>
                                <tr>
                                    <th>Área</th>
                                    <td><?= $area; ?></td>
                                </tr>
                                <tr>
                                    <th>Comentarios</th>
                                    <td><?= $comentarios; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
