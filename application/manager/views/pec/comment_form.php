﻿<?php
/* @var $this documentos */
/* @var $model documento_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar Comentario</h1>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Editar comentario</div>
                <div class="panel-body">
                    <div class="row">
                        <?= form_open("pec/update_comment/" . $id_comentario); ?>
                        <div class="col-lg-6">
                            <?php if (validation_errors() != FALSE) { ?>
                                <div class="alert alert-danger">
                                    <?= validation_errors(); ?>
                                </div>  
                            <?php } ?>
                            <div class="form-group">
                                <label>Título:</label>
                                <input class="form-control" placeholder="Ingrese título..." id="input_titulo" name="input_titulo" 
                                       value="<?= isset($titulo) ? $titulo : ''; ?>" required>
                            </div>
                            <div class="form-group">
                                <label>Comentario:</label>
                                <textarea class="form-control" placeholder="Ingrese comentario..." id="input_comentario" name="input_comentario" 
                                    maxlength="2000" rows="4" required><?= isset($comentario) ? $comentario : ''; ?></textarea>
                            </div>
                            <input type="hidden" name="input_id_comentario" id ="input_id_comentario" value="<?= isset($id_comentario) ? $id_comentario : ''; ?>">
                            <input type="hidden" name="input_id_pec" id ="input_id_pec" value="<?= isset($id_pec) ? $id_pec : ''; ?>">
                            <button type="submit" class="btn btn-default">Editar</button>
                            <button type="reset" class="btn btn-default">Limpiar Formulario</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>