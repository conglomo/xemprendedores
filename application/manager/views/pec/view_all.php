<?php
/* @var $this documentos */
/* @var $model documentos_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Documentos PEC</h1>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="example">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($get_all as $formulario) {
                        echo '<tr>';
                        echo '<td>' . $formulario->nombre . '</td>';
                        echo '<td>' . $formulario->descripcion . '</td>';
                        echo '<td>
                                <a href="' . $formulario->doc_path . '" title="Ver archivo" target="_blank"><i class="fa fa-file fa-fw"></i></a>
                                <a href="' . site_url("pec/ver_comentarios/" . $formulario->id_pec) . '" title="Ver/agregar comentarios"><i class="fa fa-comment fa-fw"></i></a>
                                <a href="' . site_url("pec/update/" . $formulario->id_pec) . '" title="Editar"><i class="fa fa-edit fa-fw"></i></a>
                                <a href="' . site_url("pec/delete/" . $formulario->id_pec) . '" title="Eliminar" class="deleteRow"><i class="fa fa-times fa-fw"></i></a>
                              </td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
                <!-- <tfoot>
                    <tr>
                        <th><input type="text" name="search_id" value="Buscar..." class="search_init"/></th>
                        <th><input type="text" name="search_nombre" value="Buscar..." class="search_init" /></th>
                    </tr>
                </tfoot> -->
            </table>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(document).ready(function() {

        $(".deleteRow").click(function(event) {
            var href = $(this).attr("href")
            var btn = this;

            $.ajax({
                type: "GET",
                url: href,
                beforeSend: function() {
                    if (!confirm("¡Está a punto de eliminar este documento! ¿Desea continuar?")) {
                        return false;
                    }
                },
                success: function(msgd) {
                        $(btn).closest('tr').fadeOut("slow");
                }
            });
            event.preventDefault();
        })
    });
</script>
