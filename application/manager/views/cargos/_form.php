<?php
/* @var $this cargos */
/* @var $model cargo_model */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <?= ($this->router->method == "create") ? '<h1 class="page-header">Crear</h1>' : '<h1 class="page-header">Editar</h1>'; ?>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= ($this->router->method == "create") ? 'Registrar nuevo cargo en el sistema' : 'Editar cargo del sistema'; ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?= ($this->router->method == "create") ? form_open("cargos/create") : form_open("cargos/update"); ?>
                        <div class="col-lg-6">
                            <?php if (validation_errors() != FALSE) { ?>
                                <div class="alert alert-danger">
                                    <?= validation_errors(); ?>
                                </div>  
                            <?php } ?>

                            <input type='text' hidden id="input_id" name="input_id" value="<?= isset($id) ? $id : ''; ?>"/>
                            
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input class="form-control" placeholder="Nombre del Cargo" id="input_nombre" name="input_nombre" 
                                       value="<?= isset($nombre) ? $nombre : ''; ?>">
                            </div>

                            <button type="submit" class="btn btn-default">
                                <?= ($this->router->method == "create") ? "Crear" : "Editar"; ?>
                            </button>
                            <button type="reset" class="btn btn-default">Limpiar Formulario</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>