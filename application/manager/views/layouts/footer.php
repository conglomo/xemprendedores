<!-- Core Scripts - Include with every page -->
<script src="<?= base_url('js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('js/plugins/metisMenu/jquery.metisMenu.js') ?>"></script>
<script src="<?= base_url('js/bootstrap-datetimepicker.min.js') ?>"></script>
<script src="<?= base_url('js/moment.min.js') ?>"></script>

<!-- SB Admin Scripts - Include with every page -->
<script src="<?= base_url('js/sb-admin.js') ?>"></script>
<script type="text/javascript" charset="utf-8">
    var asInitVals = new Array();

    $(document).ready(function() {
        var oTable = $('#example').dataTable({
            "order": [[1, "desc"]],
            "oLanguage": {
                "sSearch": "Buscar:",
                "zeroRecords": "Lo sentimos, no hemos encontrado registros",
                "lengthMenu": "Mostrando _MENU_ registros por página",
                "info": "Mostrando _PAGE_ páginas de _PAGES_"
            }
        });

        $("tfoot input").keyup(function() {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter(this.value, $("tfoot input").index(this));
        });



        /*
         * Support functions to provide a little bit of 'user friendlyness' to the textboxes in 
         * the footer
         */
        $("tfoot input").each(function(i) {
            asInitVals[i] = this.value;
        });

        $("tfoot input").focus(function() {
            if (this.className == "search_init")
            {
                this.className = "";
                this.value = "";
            }
        });

        $("tfoot input").blur(function(i) {
            if (this.value == "")
            {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });
</script>
</body>
</html>