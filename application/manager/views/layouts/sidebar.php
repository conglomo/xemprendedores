<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <?php if($this->session->userdata('usrrole') == 3){ ?>
            <li>
                <a href="#"><i class="fa fa-users fa-fw"></i> General<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?= site_url("usuarios/view_all"); ?>">Gestión de usuarios</a>
                    </li>
                    <li>
                        <a href="<?= site_url("cargos/view_all"); ?>">Gestión de cargos</a>
                    </li>
                    <li>
                        <a href="<?= site_url("roles/view_all"); ?>">Gestión de roles</a>
                    </li>
                    <li>
                        <a href="<?= site_url("areas/view_all"); ?>">Gestión de Áreas</a>
                    </li>
                </ul>
            </li>
            <?php } ?>
            
            <?php if($this->session->userdata('usrrole') == 3 || $this->session->userdata('usrrole') == 2 || $this->session->userdata('usrrole') == 1){ ?>
            <li>
                <a href="<?= site_url(); ?>/agenda/ver_eventos/"><i class="fa fa-calendar fa-fw"></i> Agenda</a>
            </li>
            <?php } ?>
            
            <?php if($this->session->userdata('usrrole') == 3){ ?>
            <li>
                <a href="#"><i class="fa fa-edit fa-fw"></i> Formularios<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?= site_url("formularios/view_all/"); ?>">Ver Formularios</a>
                    </li>
                    <!-- <li>
                        <a href="#">Consolidado</a>
                    </li> -->
                </ul>
            </li>
            <?php } ?>
            
            <?php if($this->session->userdata('usrrole') == 3){ ?>
            <li>
                <a href="#"><i class="fa fa-file-text-o fa-fw"></i> Biblioteca<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?= site_url("documentos/create/"); ?>">Subir documento</a>
                    </li>
                    <li>
                        <a href="<?= site_url("documentos/view_all/"); ?>">Ver Documentos</a>
                    </li>
                </ul>
            </li>
            <?php } ?>
            <?php if($this->session->userdata('usrrole') == 3 || $this->session->userdata('usrrole') == 1){ ?>
            <li>
                <a href="#"><i class="fa fa-file-text-o fa-fw"></i> PEC<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?= site_url("pec/create/"); ?>">Subir documento</a>
                    </li>
                    <li>
                        <a href="<?= site_url("pec/view_all/"); ?>">Ver Documentos</a>
                    </li>
                </ul>
            </li>
            <?php } ?>
        </ul>
    </div>
</nav>
