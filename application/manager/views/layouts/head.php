<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title></title>
        <!-- Core CSS - Include with every page -->
        <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('font-awesome/css/font-awesome.css') ?>" rel="stylesheet">

        <!-- SB Admin CSS - Include with every page -->
        <link href="<?= base_url('css/sb-admin.css') ?>" rel="stylesheet">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.9.1.js"></script>
        <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

        <script src="<?= base_url('js/plugins/dataTables/jquery.dataTables.js'); ?>"></script>
        <script src="<?= base_url('js/plugins/dataTables/dataTables.bootstrap.js'); ?>"></script>
        
        <link href="<?= base_url('css/plugins/dataTables/dataTables.tableTools.css') ?>" rel="stylesheet">
        <script type="text/javascript" charset="utf-8" src="<?= base_url('js/plugins/dataTables/dataTables.tableTools.js'); ?>"></script>

        <script src="<?= base_url('js/bootstrap-datetimepicker.js') ?>"></script>
        <script src="<?= base_url('js/moment.min.js') ?>"></script>
        <link href="<?= base_url('css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">
        
        <script src="<?= base_url('js/plugins/dataTables/jquery.dataTables.columnFilter.js'); ?>"></script>
        <script src="//cdn.datatables.net/plug-ins/380cb78f450/sorting/date-euro.js"></script> 
        
        <script type="text/javascript">
            $(function() {
                $('#fechaPicker').datetimepicker({
                    pickTime: false,
                    format: 'DD-MM-YYYY',
                    autoclose: true,
                    language: 'es'
                });
            });
        </script>

        <script type="text/javascript">
            $(function() {
                $('#inicioPicker, #finPicker').datetimepicker({
                    format: 'HH:mm',
                    autoclose: true,
                    pickDate: false,
                    pickSeconds: false,
                    pick12HourFormat: false
                });
            });
        </script>
        

<!--<script src="<?php // base_url('js/bootstrap-timepicker.min.js')   ?>"></script>
<link href="<?php // base_url('css/bootstrap-timepicker.min.css')   ?>" rel="stylesheet">
<script>
    $(function() {
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd"
        });
    });
</script>-->
    </head>
    <body>