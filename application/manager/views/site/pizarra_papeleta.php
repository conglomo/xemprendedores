<html>
  <head>
    <meta http-equiv="refresh" content="300">
    <style type="text/css">
      /* Reset the stylesheet */
      html, body, div, span, applet, object, iframe,
      h1, h2, h3, h4, h5, h6, p, blockquote, pre,
      a, abbr, acronym, address, big, cite, code,
      del, dfn, em, font, img, ins, kbd, q, s, samp,
      small, strike, strong, sub, sup, tt, var,
      dl, dt, dd, ol, ul, li,
      fieldset, textarea, form, label, legend,
      table, caption, tbody, tfoot, thead, tr, th, td,
      address, article, aside, audio, canvas, command, datalist, 
      details, dialog, figure, figcaption, footer, header, hgroup,
      keygen, mark, meter, menu, nav, progress,
      ruby, section, time, video {
        margin: 0%;
        border: 0px;
        padding: 0%;
        outline: 0px;
        font-size: 100%;
        font-style: inherit;
        font-weight: inherit;
        font-family: inherit;
        vertical-align: baseline;
        box-sizing: border-box;
      }
      /* HTML5 elements that are blocks */
      article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section, ul li, ol li {
          display: block;
          margin: 0;
      }
      strong{font-weight: bold;}

      /* Normal HTML */
      ol, ul {list-style: none;}
      a{text-decoration: none;}

      /*---*/

      html, body{
        font-size: 0.95em;
        width: 100%;
      }
      
      ul{
        height: 128px;
        width: 384px;
      }
      ul li{
        padding: 5px 5px 0px 5px;
        width: 100%;
      }
      ul li span{
        display: block;
        font-family: 'Platform-Bold';
        font-size: 0.8em;
        letter-spacing:1.2px;
        font-weight: 600;
        margin-bottom: 2.5px;
        text-transform: uppercase;
      }
      ul li p{
        border-radius: 5px;
        font-family: 'Platform-Regular';
        letter-spacing:1.2px;
        font-size: 0.7em;
        padding: 5px;
      }
      ul li.pregunta{
        float:left;
        margin-bottom: -5px;
        padding-right: 30%;
      }
      ul li.pregunta div{
        float: left;
        text-align: left;
      }
      ul li.pregunta img{
        margin-left: 5%;
      }
      ul li.respuesta img{
        margin-right: 5%;
      }
      ul li.respuesta{
        float:right;
        padding-left: 30%; 
        text-align: right;
      }
      ul li.respuesta div{
        float: right;
        text-align: right;
      }
      @font-face {
        font-family: 'Platform-Bold'; /*a name to be used later*/
        src: url('http://hola-hola.cl/font/Platform-Bold.otf'); /*URL to font*/
      }
      @font-face {
        font-family: 'Platform-Regular'; /*a name to be used later*/
        src: url('http://hola-hola.cl/font/Platform-Regular.otf'); /*URL to font*/
      }
    </style>
    
    <meta charset='UTF-8' />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
  </head>
  <body>
     <?php foreach ($ciudades as $city) {

        if ($city->id_ciudad == $mensajes[1]->origen) {
          $nombre_origen = $city->nombre;
        }

        if ($city->id_ciudad == $mensajes[0]->origen) {
          $nombre_destino = $city->nombre;
        }

        }?>
        <?php $mensajes2 = array_reverse($mensajes);  ?>
    <ul style='background-color: <?php echo $background; ?>; color: <?php echo $color; ?>'>
      <?php $aux=0; foreach ($mensajes as $ver_mensaje){  if ($ver_mensaje->tipo==1 && $ver_mensaje->estado==1 && $aux<2) { ?>
      <li <?php  if ($ver_mensaje->origen==$origen) {?> style="margin-left:2px;" class='pregunta'  <?php }else{ ?>style="margin-right:2px;" class='respuesta' <?php } ?> >
        <div>
          <span><?php foreach ($ciudades as $city) {

                                                if ($city->id_ciudad == $ver_mensaje->origen) {
                                                    $nombre_origen = $city->nombre;
                                                    
                                                }
                                            }?>
                <?php if ($ver_mensaje->pregunta=="¿Cómo te imaginas el futuro de tu comunidad?"){ echo "El futuro de " . $nombre_origen . " será:";  ?> 
                <?php }elseif($ver_mensaje->pregunta=="¿Cómo ha sido la historia de tu comunidad?"){ echo "La historia de " . $nombre_origen . " ha sido:";  ?>
                <?php }elseif($ver_mensaje->pregunta=="¿Qué le dirías a tu vecino?"){ echo "¿Qué le dirías a tu vecino de " . $nombre_origen . "?:";  ?>
                <?php }else{ echo $ver_mensaje->pregunta; } ?>

          </span>
          <p style='border: <?php echo $border; ?>'>
            <?php $nombre_mensaje[$aux]=$ver_mensaje->mensaje;    echo $ver_mensaje->mensaje; ?>
          </p>
          <img src='<?php echo 'http://www.hola-hola.cl/images/triangulo-pre-'.$imagen.'.png'; ?>' />
        </div>
      </li>
      <?php $aux++; } } ?>

      
    </ul>
  </body>
</html>