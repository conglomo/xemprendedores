    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Hola Hola</h1>
            </div>
            <div id="crear_ac" class="row">
            <div class="col-lg-12">
                <a href="<?= site_url("site/inicio"); ?>">
                        <button type="button" class="btn btn-info">Ingresar Mensaje</button>
                </a>
                <a href="<?= site_url("site/papeleta/"); ?>">
                        <button type="button" class="btn btn-info">Ingresar Papeleta</button>
                </a>
                <a href="<?= site_url("site/pizarra/"); ?>">
                        <button type="button" class="btn btn-info">Ingresar Pizarra</button>
                </a>
            </div>
            <br>
        </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-comment fa-fw"></i> Buscar chat
                        <div class="pull-right">
                            
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-area-chart">
                            <form action='<?php echo site_url('site/ver_mensajes'); ?>' method='POST' target="_blank">

                           
                           

                            <div class="form-group">
                                <label>*Origen: </label>
                                
                               
                                <select class="form-control" id="origen" required name="origen"> 
                                    <option value='0'>Todos</option>
                                <?php foreach ($ciudades as $origen) { ?>
                                    <option value='<?php echo $origen->{'id_ciudad'}; ?>'><?php echo $origen->{'nombre'}; ?></option>
                                <?php } ?>
                                </select>     
                            </div>

                            <div class="form-group">
                                <label>*Destino: </label>
                                
                                <select class="form-control" id="destino" required name="destino"> 
                                <option value='0'>Todos</option>                                
                                <?php foreach ($ciudades as $destino) { ?>
                                    <option value='<?php echo $destino->{'id_ciudad'}; ?>'><?php echo $destino->{'nombre'}; ?></option>
                                <?php } ?>
                                </select>     
                            </div>

                            <div class="form-group">
                                <label>*Tipo: </label>
                                
                                <select class="form-control" id="tipo_vista" required name="tipo_vista"> 
                                     <option value='5'>Moderación</option>
                                     <option value='1'>Chat completo</option>
                                     <!-- <option value='2'>Vista LED 1</option>-->
                                     <option value='3'>Vista Día</option>
                                     <option value='4'>Vista Noche</option>
                                     
                                     <option value='6'>Vista Papeleta/Pizarra Día</option>
                                     <option value='7'>Vista Papeleta/Pizarra Noche</option>
                                </select>     
                            </div>

                            

                            <label>*Todos los campos son obligatorios.</label>
                            <br>
                            <button type="submit" class="btn btn-default">
                                 Ir
                            </button>
                            <button type="reset" class="btn btn-default">Limpiar Formulario</button>
                            </form>


                        </div>
                    </div>

                    <!-- /.panel-body -->
                </div>
                
                <!-- /.panel -->
                
            <!-- /.col-lg-8 -->
            <div style="float:left;" class="col-lg-4">
                
                <!-- /.panel -->
                
                <!-- /.panel -->
                
                <!-- /.panel .chat-panel -->
            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>



