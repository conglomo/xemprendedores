    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Hola Hola</h1>
            </div>
        </div>
        <div id="crear_ac" class="row">
            <div class="col-lg-12">
                <a href="<?= site_url("site/administracion/"); ?>">
                    <button type="button" class="btn btn-info">Buscar Chat</button>
                </a>
                <a href="<?= site_url("site/inicio/"); ?>">
                        <button type="button" class="btn btn-info">Ingresar Mensaje</button>
                </a>
                <a href="<?= site_url("site/pizarra/"); ?>">
                        <button type="button" class="btn btn-info">Ingresar Pizarra</button>
                </a>
            </div>
        </div>
        
    <br>
        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-comment fa-fw"></i> Enviar Papeleta
                        <div class="pull-right">
                            
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-area-chart">
                            <form action='<?php echo site_url('site/crear_mensaje'); ?>' method='POST'>

                            
                            <div  name="input_pregunta1" id="input_pregunta1" class="form-group">
                                <label>Tipo: </label>
                                <select class="form-control" id="pregunta1" name="pregunta1">
                                    <option value="¿Cómo te imaginas el futuro de tu comunidad?">¿Cómo te imaginas el futuro de tu comunidad?</option>
                                    <option value="¿Cómo ha sido la historia de tu comunidad?">¿Cómo ha sido la historia de tu comunidad?</option>
                                    <option value="¿Qué le dirías a tu vecino?">¿Qué le dirías a tu vecino?</option>
                                    
                                </select>
                            </div>

                            

                            <div class="form-group">
                                <label>*Origen: </label>
                                
                               
                                <select class="form-control" id="origen" required name="origen"> 
                                    <option value="" <?php if(validation_errors()){ echo "selected"; }else{ echo set_select('sel_local', '', TRUE); } ?>>Seleccione localidad</option>
                                <?php foreach ($ciudades as $origen) { ?>
                                    <option value='<?php echo $origen->{'id_ciudad'}; ?>'><?php echo $origen->{'nombre'}; ?></option>
                                <?php } ?>
                                </select>     
                            </div>

                            <div style="display:none;" class="form-group">
                                <label>*Origen: </label>
                                
                               
                                <select class="form-control" id="destino" name="destino"> 
                                    <option value="" <?php if(validation_errors()){ echo "selected"; }else{ echo set_select('sel_local', '', TRUE); } ?>>Seleccione localidad</option>
                                
                                </select>     
                            </div>

                            

                            <div class="form-group">
                                <label>*Mensaje (máximo 255 caracteres):</label>
                                <!--<input class="form-control" placeholder="Descripción de la entrevista" id="input_descripcion" name="input_descripcion" 
                                       value="<?//= isset($descripcion) ? $descripcion : ''; ?>"> -->
                                <textarea class="form-control" name="mensaje" id="mensaje" placeholder="Escriba un mensaje" rows="2" maxlength="255" required></textarea>
                            </div>

                            <label>*Todos los campos son obligatorios.</label>
                            <br>
                            <button type="submit" class="btn btn-default">
                                 Enviar Papeleta
                            </button>
                            <button type="reset" class="btn btn-default">Limpiar Formulario</button>
                            </form>


                        </div>
                    </div>

                    <!-- /.panel-body -->
                </div>
                
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>


<script type="text/javascript">

    

    $("#origen").change(function(){


            $("#destino option:selected").text() = $("#origen option:selected").text();
            
            
       
    });

</script>
