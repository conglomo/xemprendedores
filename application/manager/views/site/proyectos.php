<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PorEmprendedores.org</title>
    
    <link rel="stylesheet" href="http://qa.ibex.cl/emprendedores/css/sitio/bootstrap.min.css">
    <link rel="stylesheet" href="http://qa.ibex.cl/emprendedores/css/sitio/idangerous.swiper.css">
    <link rel="stylesheet" href="http://qa.ibex.cl/emprendedores/css/sitio/flexslider.css">
    <link rel="stylesheet" href="http://qa.ibex.cl/emprendedores/css/sitio/animate.css">
    <link rel="stylesheet" type="text/css" href="http://qa.ibex.cl/emprendedores/css/sitio/menu_elastic.css" />
    <link rel="stylesheet" href="http://qa.ibex.cl/emprendedores/css/sitio/main.css">
    <link rel="stylesheet" href="http://qa.ibex.cl/emprendedores/css/sitio/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
    
    <!--sidebar menu-->
    <script src="http://qa.ibex.cl/emprendedores/js/snap.svg-min.js"></script>
</head>

<body id="internal">
    <div id="preloader">
        <div id="status">
            <div id="loader"></div><!--loader-->
        </div><!--status-->
    </div><!--preloader-->
    
    <div class="menu-container hidden-lg">
        <div class="menu-wrap">
            <nav class="menu hidden-lg" id="slide-menu">
                <div class="icon-list">
                    <a class="navbar-brand sidebar-brand" href=""></a>
                    <a href=""><span class="active-link icon-list-item" id="0">Inicio</span></a>
                    <a href=""><span class="icon-list-item" id="1">Proyecto</span></a>
                    <a href=""><span class="icon-list-item" id="2">Noticias</span></a>
                    <a href=""><span class="icon-list-item" id="5">Contacto</span></a>
                </div><!--icon-list-->
            </nav>
            
            <button class="close-button hidden-lg" id="close-button">Close Menu</button>
            <div class="morph-shape" id="morph-shape" data-morph-open="M-1,0h101c0,0,0-1,0,395c0,404,0,405,0,405H-1V0z">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
                    <path d="M-1,0h101c0,0-97.833,153.603-97.833,396.167C2.167,627.579,100,800,100,800H-1V0z"/>
                </svg>
            </div><!--morph-shape-->
        </div><!--menu-wrap-->
    </div><!-- menu-container -->

    <nav id="staticBar" class="navbar">
        <div class="container">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header nav-transparent">
                    <button type="button" class="navbar-toggle collapsed menu-button" data-toggle="collapse" id="open-button" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                    <a class="navbar-brand" href="" id="0"></a>
                </div><!--navbar-header-->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right visible-lg" id="nav">
                        <li><a href="" id="0" class="text-white">Inicio</a></li>
                        <li><a href="" id="1" class="active-link text-white">Proyecto</a></li>
                        <li><a href="" id="2" class="text-white">Noticias</a></li>
                        <li><a href="" id="5" class="text-white">Contacto</a></li>
                    </ul>
                </div><!--navbar-collapse -->
            </div><!--container-fluid -->
        </div><!--container-->
    </nav>  
    
    <div id="internalContent">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div id="titleProject">
                        <div id="contImgProjects"></div>
                        <h1>Boulevard Chañaral</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div id="leftCol" class="col-sm-8">
                    <div id="contVideo">
                        <iframe src="https://player.vimeo.com/video/124129703?color=ff9933" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>

                    <div id="panelInformation" role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#info" aria-controls="home" role="tab" data-toggle="tab">Campañas</a></li>
                            <li role="presentation"><a href="#updates" aria-controls="profile" role="tab" data-toggle="tab">Actualizaciones</a></li>
                            <li role="presentation"><a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">Comentarios</a></li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="info"></div>
                            <div role="tabpanel" class="tab-pane" id="updates"></div>
                            <div role="tabpanel" class="tab-pane" id="comments"></div>
                        </div>
                    </div>
                </div>

                <div id="rightCol" class="col-sm-4">
                    <h3 class="titleSection">Selecciona una opción:</h3>

                    <section class="contOption">
                        <div class="contRadio">
                            <input type="checkbox" id="opDinero" name="opDinero" class="optionDonation" />
                        </div>

                        <div class="contInfo">
                            <h3>Donación en dinero</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>

                            <input type="number" name="monto" id="monto" class="form-control inputInfo" placeholder="Ingresa monto a donar" />
                        </div>
                    </section>

                    <section class="contOption">
                        <div class="contRadio">
                            <input type="checkbox" id="opMaterial" name="opMaterial" class="optionDonation" />
                        </div>
                        
                        <div class="contInfo">
                            <h3>Donación en materiales</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>

                            <textarea name="materiales" id="materiales" class="form-control inputInfo" placeholder="Escribe los materiales que vas a donar"></textarea>
                        </div>
                    </section>

                    <section class="contOption">
                        <div class="contRadio">
                            <input type="checkbox" id="opTrabajo" name="opTrabajo" class="optionDonation" />
                        </div>
                        
                        <div class="contInfo">
                            <h3>Donar horas de trabajo</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>

                            <input type="number" name="horas" id="horas" class="form-control inputInfo" placeholder="Ingresa cantidad de horas de trabajo" />
                            <input type="text" name="profesion" id="profesion" class="form-control inputInfo" placeholder="¿Cual es tu profesión?" />
                        </div>
                    </section>

                    <section class="contOption">
                        <div class="contRadio">
                            <input type="checkbox" id="opIdea" name="opIdea" class="optionDonation" />
                        </div>
                        
                        <div class="contInfo">
                            <h3>Donar una idea</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>

                            <textarea name="idea" id="idea" class="form-control inputInfo" placeholder="Redacta tu idea de innovación"></textarea>
                        </div>
                    </section>

                    <button id="donate" class="btn donateBtn" data-toggle="modal" data-target="#infoUser"><i class="fa fa-heart"></i> Donar</button>
                </div>
            </div>
        </div>
    </div><!--swiper-container-->

    <div class="modal fade" id="infoUser" tabindex="-1" role="dialog" aria-labelledby="infoUserLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Formulario Donación</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="nombres" class="control-label">Nombres:</label>
                            <input type="text" class="form-control" id="nombres">
                        </div>
                        <div class="form-group">
                            <label for="apellidos" class="control-label">Apellidos:</label>
                            <input type="text" class="form-control" id="apellidos">
                        </div>
                        <div class="form-group">
                            <label for="rut" class="control-label">Rut:</label>
                            <input type="text" class="form-control" id="rut">
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">Email:</label>
                            <input type="email" class="form-control" id="email">
                        </div>
                        <div class="form-group">
                            <label for="telefono" class="control-label">Telefono:</label>
                            <input type="text" class="form-control" id="telefono">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="donateForm" class="btn donateBtn"><i class="fa fa-heart"></i> Donar</button>
                </div>
            </div>
        </div>
    </div>

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>if (!window.jQuery) { document.write('<script src="../js/jquery-2.1.3.min.js"><\/script>'); }</script>
    <script src="http://qa.ibex.cl/emprendedores/js/bootstrap.min.js"></script>
    <script src="http://qa.ibex.cl/emprendedores/js/classie.js"></script>
    <script src="http://qa.ibex.cl/emprendedores/js/main3.js"></script>
    <script src="http://qa.ibex.cl/emprendedores/js/parallax.js"></script>
    <script src="http://qa.ibex.cl/emprendedores/js/donation.js"></script>
    <script src="http://qa.ibex.cl/emprendedores/js/main.js"></script>
</body>
</html>