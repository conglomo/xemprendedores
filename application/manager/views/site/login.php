<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Por favor, inicie sesión</h3>
                </div>
                <div class="panel-body">
                    <?php if (!empty($this->session->flashdata)) { ?>
                        <div class="alert alert-danger">
                            <?= $this->session->flashdata('error_validate'); ?>
                        </div>
                    <?php } ?>

                    <?php if (validation_errors() != FALSE) { ?>
                        <div class="alert alert-danger">
                            <?= validation_errors(); ?>
                        </div>  
                    <?php } ?>

                    <? $attributes = array('id' => 'form_login'); ?>
                    <?= form_open('site/login', $attributes); ?>
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="Usuario" name="username" type="username" autofocus>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Contraseña" name="password" type="password" value="">
                        </div>
                        <a class="btn btn-lg btn-success btn-block" onclick="document.getElementById('form_login').submit();">Login</a>
                    </fieldset>

                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
