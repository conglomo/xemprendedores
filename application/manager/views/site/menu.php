    <style type="text/css" media="print">
    @media print {
    #parte1 {display:none;}
    #parte2 {display:none;}
    #parte_id {display:none;}
    #parte_id2 {display:none;}
    #parte_acciones {display:none;}
    #parte_acciones2 {display:none;}
    #parte_entre {display:none;}
    #ver1 {display:none;}
    #footer {display:none;}
    #imprimir {display:none;}
    #ver {display:none;}
    #datos_cont {display:none;}
    #grafico_titulo {display:none;}
    #piechart {margin-left: -3cm;}
    #agenda-table {font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
        font-size: 0.8em;
        color: #333333;
        
        margin-left: -0.8cm;}
    #generado_por {
        
        margin-top: 3cm;}
    }
    </style>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">X Emprendedores</h1>
            </div>
        </div>
        <!-- <div id="crear_ac" class="row">
            <div class="col-lg-12">
                <a href="<?= site_url("site/administracion/"); ?>">
                    <button type="button" class="btn btn-info">Buscar Chat</button>
                </a>
                <a href="<?= site_url("site/papeleta/"); ?>">
                        <button type="button" class="btn btn-info">Ingresar Papeleta</button>
                </a>
                <a href="<?= site_url("site/pizarra/"); ?>">
                        <button type="button" class="btn btn-info">Ingresar Pizarra</button>
                </a>
            </div>
        </div> -->
        
    <br>
        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class=""></i> Formularios
                        <div class="pull-right">
                            
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                         <div id="imprimir" class="row" align="left">
                            <div class="col-lg-12">
                                <a href="javascript:if(window.print)window.print()">
                                    <button type="button" class="btn btn-info">Imprimir</button>
                                </a>
                            </div>
                        </div>
                        <br>
                        <div id="morris-area-chart">
                            
                               <table class="table table-striped table-bordered table-hover" id="agenda-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Rut</th>
                                        <th>Email</th>
                                        <th>Telefono</th>
                                        <th style="display:none;">Monto Donacion</th>
                                        <th style="display:none;">Materiales Donacion</th>
                                        <th style="display:none;">Horas de Trabajo</th>
                                        <th style="display:none;">Profecion</th>
                                        <th style="display:none;">Idea Innovadora</th>
                                        <th id="ver1">Ver</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($formularios as $formulario) {  ?>
                                    <tr>
                                        <td><?php echo $formulario->id; ?></td>
                                        <td><?php echo $formulario->nombre; ?> <?php echo $formulario->apellido; ?></td>
                                        <td><?php echo $formulario->rut; ?></td>
                                        <td><?php echo $formulario->email; ?></td>
                                        <td><?= !empty($formulario->telefono) ? $formulario->telefono : 'N/A' ; ?></td>
                                        <td style="display:none;"><?= !empty($formulario->monto_donacion) ? $formulario->monto_donacion : 'N/A' ; ?></td>
                                        <td style="display:none;"><?= !empty($formulario->materiales_donacion) ? $formulario->materiales_donacion : 'N/A' ; ?></td>
                                        <td style="display:none;"><?= !empty($formulario->horas_donacion) ? $formulario->horas_donacion : 'N/A' ; ?></td>
                                        <td style="display:none;"><?= !empty($formulario->profesion) ? $formulario->profesion : 'N/A' ; ?></td>
                                        <td style="display:none;"><?= !empty($formulario->idea_innovacion) ? $formulario->idea_innovacion : 'N/A' ; ?></td>
                                        <td id="ver"><?php echo '<a href="' . site_url("site/ver_formulario/" . $formulario->id) . '" title="Ver"><i class="fa fa-search fa-fw"></i></a>'; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                
                               </table>
                            
                            <form action='<?php echo site_url('site/ver_formularios'); ?>' method='POST'>

                            </form>


                        </div>
                    </div>

                    <!-- /.panel-body -->
                </div>
                
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>



<script type="text/javascript">

    

    $("#tipo").change(function(){
        if($("#tipo option:selected").text()=="Papeleta"){
            $("#input_pregunta1").css('visibility','visible');
            $("#input_pregunta1").focus();
            $("#input_pregunta2").css('visibility','hidden');
            $("#input_pregunta2").val("");
            
        }else if($("#tipo option:selected").text()=="Pizarra"){
            $("#input_pregunta2").css('visibility','visible');
            $("#input_pregunta2").focus();
            
            
            $("#input_pregunta1").css('visibility','hidden');
            $("#input_pregunta1").val("");
            
        }else{
            $("#input_pregunta1").css('visibility','hidden');
            $("#input_pregunta1").val("");
            $("#input_pregunta2").css('visibility','hidden');
            $("#input_pregunta2").val("");
            
           
            
        }
    });

</script>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    //$.datepicker.regional[""].dateFormat = 'dd/mm/yy';
    //$.datepicker.setDefaults($.datepicker.regional['']);
    $('#agenda-table').dataTable({
        "order": [[ 0, "desc" ]],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
        },
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "http://cdnjs.cloudflare.com/ajax/libs/datatables-tabletools/2.1.5/swf/copy_csv_xls_pdf.swf",
            "aButtons":[
                {"sExtends": "xls","sButtonText": "Exportar a Excel", "sFileName": "Listado_formularios.xls", "mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]},
               
            ]
        }
        
    });
                
    /*$('#agenda-table').dataTable().columnFilter({ sPlaceHolder: "head:before",
                    aoColumns: [ { type: "date-range" }]});*/
} );

/* Custom filtering function which will search data in column four between two values */


$(document).ready(function() {
    var table = $('#agenda-table').DataTable();
     
    // Event listener to the two range filtering inputs to redraw on input
    
    
    $('#input_estado').on('change',function() {
        console.log("Estado cambiado");
        table.draw();
    });
    
} );

//function filterByDate(){     
//    //$('#yourInputID').val(value);
//    var table = $('#agenda-table').DataTable();
//    table.draw();
//}
</script>