    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Moderación de Mensajes</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
              
                <!-- /.panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Ver mensajes 
                        <div class="pull-right">
                            
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="agenda-table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Tipo</th>
                                                <th>Origen</th>
                                                <th>Destino</th>
                                                <th>Mensaje</th>
                                                <th>Estado</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($mensajes as $mensaje) { ?>
                                            <tr>
                                                <td><?php echo $mensaje->id; ?></td>

                                                <td><?php if($mensaje->pregunta=="0" || $mensaje->pregunta==null) $tipo="Mensaje"; else $tipo="Pizarra / Papeleta"; echo $tipo;?> </td>
                                                
                                                <td><?php foreach ($ciudades as $city) {

                                                if ($city->id_ciudad == $mensaje->origen) {
                                                    $nombre_origen = $city->nombre;
                                                    echo $nombre_origen;
                                                }
                                            }?></td>
                                                <td><?php foreach ($ciudades as $city) {

                                                if ($city->id_ciudad == $mensaje->destino) {
                                                    $nombre_destino = $city->nombre;
                                                    echo $nombre_destino;
                                                }
                                            }?></td>

                                                <?php if ($mensaje->estado==0) {
                                                   $nombre_estado = "No publicado";
                                                }  ?>
                                                <?php if ($mensaje->estado==1) {
                                                   $nombre_estado = "Publicado";
                                                }  ?>
                                                <?php if ($mensaje->estado==2) {
                                                   $nombre_estado = "Rechazado";
                                                }  ?>

                                                <td><?php if($mensaje->pregunta!="0" || $mensaje->pregunta==null){ echo $mensaje->pregunta; }?> <?php echo $mensaje->mensaje; ?></td>
                                                <th><?php echo $nombre_estado; ?></th>
                                                <?php
                                                echo '<td id="parte_acciones2">';
                                                if ($mensaje->estado==0){
                                                        echo "<a href='".site_url("site/publicar_mensaje/" . $mensaje->id . "/1")."' title='Publicar'><i class='fa fa-share fa-fw'></i></a>";
                                                        echo "<a href='".site_url("site/publicar_mensaje/" . $mensaje->id . "/2")."' title='No Publicar'><i class='fa fa-minus-circle fa-fw'></i></a>";
                                                    
                                                    
                                                }
                                                else if ($mensaje->estado==1){
                                                    echo "<a href='".site_url("site/republicar/" . $mensaje->origen . "/" . $mensaje->destino . "/" . $mensaje->mensaje . "/" . $mensaje->pregunta)."' title='Republicar'><i class='fa fa-retweet fa-fw'></i></a>";
                                                }
                                                echo '</td>';
                                                ?>
                                            </tr>
                                            <?php  } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.col-lg-4 (nested) -->
                            <div class="col-lg-8">
                                <div id="morris-bar-chart"></div>
                            </div>
                            <!-- /.col-lg-8 (nested) -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
                
            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    //$.datepicker.regional[""].dateFormat = 'dd/mm/yy';
    //$.datepicker.setDefaults($.datepicker.regional['']);
    $('#agenda-table').dataTable({
        "order": [[ 0, "desc" ]],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
        },
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "http://cdnjs.cloudflare.com/ajax/libs/datatables-tabletools/2.1.5/swf/copy_csv_xls_pdf.swf",
            "aButtons":[
                
            ]
        }
        
    });
                
    /*$('#agenda-table').dataTable().columnFilter({ sPlaceHolder: "head:before",
                    aoColumns: [ { type: "date-range" }]});*/
} );

/* Custom filtering function which will search data in column four between two values */


//function filterByDate(){     
//    //$('#yourInputID').val(value);
//    var table = $('#agenda-table').DataTable();
//    table.draw();
//}
</script>