<?php
/**
 * Description of area_model
 *
 * @author nicolas
 */
class area_model extends CI_Model{
    
    public function view_all($table){
        $query = $this->db->get($table);
        
        return $query->result();
    }
    
    public function view($table, $id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('area_id', $id);
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function getAreaName($id){
        $this->db->select('area_nombre_area');
        $this->db->from('area');
        $this->db->where('area_id', $id);
        
        $query = $this->db->get();
        
        return $query->row();
    }
    
    public function create($table, $nombre){
        $data = array(
            'area_nombre_area' => $nombre,
        );
        
        $this->db->insert($table, $data);
        
        if($this->db->affected_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function update($table, $id, $nombre){
        $data = array(
            'area_nombre_area' => $nombre,
        );
        
        $this->db->where('area_id',$id);
        $this->db->update($table, $data);
        
        if($this->db->affected_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function delete($table, $id){
        $this->db->delete($table, array('area_id' => $id));
    }
}

/* End of file area_model.php */
/* Location: ./application/controllers/area_model.php */