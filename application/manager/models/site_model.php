<?php
/**
 * Description of site_model
 *
 * @author nicolas
 */
class site_model extends CI_Model{
    
    function validate_user($username, $password){
        $this->db->where("usr_username", $username);
        $this->db->where("usr_password", sha1($password));
        //$this->db->where("usr_role", "1");
        //$this->db->or_where('usr_role', "3"); 
        $query = $this->db->get("usuarios");
        
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }

    function ver_formularios(){

        $this->db->select("*");
        //$this->db->from("formulario");
        
        $query = $this->db->get("formulario");
        
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }

    function verFormulario($id){

        $this->db->select("*");
        //$this->db->from("formulario");
        $this->db->where("id", $id);
        
        $query = $this->db->get("formulario");
        
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    
}

/* End of file site_mdel.php */
/* Location: ./application/models/site_model.php */