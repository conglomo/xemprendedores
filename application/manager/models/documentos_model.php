<?php
/**
 * Description of documento_model
 *
 * @author nicolas
 */
class Documentos_model extends CI_Model {
    
    public function view($table, $id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('doc_id', $id);
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function view_all($table){
        $query = $this->db->get($table);
        
        return $query->result();
    }
    
    public function create($table, $nombre, $descripcion, $path){
        $data = array(
            "doc_nombre" => $nombre,
            "doc_descripcion" => $descripcion,
            "doc_path" => $path,
        );
        
        $this->db->insert($table, $data);
        
        if($this->db->affected_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function update($table, $id, $nombre, $descripcion, $path){
        if ($path != null){
            $data = array(
                'doc_nombre' => $nombre,
                'doc_descripcion' => $descripcion,
                'doc_path' => $path
            );
            
        }
        else {
            $data = array(
                'doc_nombre' => $nombre,
                'doc_descripcion' => $descripcion
            );
        }
        $this->db->where('doc_id',$id);
        $this->db->update($table, $data);
        
        if($this->db->affected_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function delete($table, $id){
        $this->db->delete($table, array('doc_id' => $id));
    }
}

/* End of file area_model.php */
/* Location: ./application/controllers/area_model.php */
