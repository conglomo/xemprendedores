<?php
/**
 * Description of usuario_model
 *
 * @author nicolas
 */
class Usuario_model extends CI_Model{
    
    public function view_all($table){
        $this->db->select('usr_id, usr_nombre, usr_apellidos, usr_empresa, cargo_nombre_cargo, area_nombre_area, role_nombre_rol, usr_username');
        $this->db->from($table);
        $this->db->join('cargos', 'cargos.cargo_id ='.$table.'.usr_cargo');
        $this->db->join('area', 'area.area_id ='.$table.'.usr_area');
        $this->db->join('roles', 'roles.role_id ='.$table.'.usr_role');
        $this->db->order_by('usr_nombre', 'asc');
        $this->db->order_by('usr_apellidos', 'asc');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function view($table, $id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->join('cargos', 'cargos.cargo_id ='.$table.'.usr_cargo');
        $this->db->join('area', 'area.area_id ='.$table.'.usr_area');
        $this->db->join('roles', 'roles.role_id ='.$table.'.usr_role');
        $this->db->where('usr_id', $id);
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function create($table, $nombre, $apellidos, $empresa, $cargo, $area, $role, $username, $password, $email){
        $data = array(
            'usr_nombre' => $nombre,
            'usr_apellidos' => $apellidos,
            'usr_empresa' => $empresa,
            'usr_cargo' => $cargo,
            'usr_area' => $area,
            'usr_username' => $username,
            'usr_password' => sha1($password),
            'usr_role' => $role,
            'usr_email' => $email
        );
        
        $this->db->insert($table, $data);
        
        if($this->db->affected_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function update($table, $id, $nombre, $apellidos, $empresa, $cargo, $area,
            $password, $username, $role, $email){
        
        if(!empty($password)){
            $data = array(
                'usr_nombre' => $nombre,
                'usr_apellidos' => $apellidos,
                'usr_empresa' => $empresa,
                'usr_cargo' => $cargo,
                'usr_area' => $area,
                'usr_password' => sha1($password),
                'usr_username' => $username,
                'usr_role' => $role,
                'usr_email' => $email
            );
        }elseif(empty($password)){
            $data = array(
                'usr_nombre' => $nombre,
                'usr_apellidos' => $apellidos,
                'usr_empresa' => $empresa,
                'usr_cargo' => $cargo,
                'usr_area' => $area,
                'usr_username' => $username,
                'usr_role' => $role,
                'usr_email' => $email
            );
        }
        
        $this->db->where('usr_id',$id);
        $this->db->update($table, $data);
        
        if($this->db->affected_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function delete($table, $id){
        $this->db->delete($table, array('usr_id' => $id));
    }
    
    public function validate_username($table, $username){
        $this->db->where("usr_username", $username);
        
        $query = $query = $this->db->get($table);
        
        if($query->num_rows() > 0){
            return 1;
        }else{
            return 0;
        }
    }
    
    public function getUserEmail($id_user){
        $this->db->select('usr_email');
        $this->db->from('usuarios');
        $this->db->where('usr_id', $id_user);
        $query = $this->db->get();
        return $query->row();
    }
    
    public function getUserInfo($id_user){
        $this->db->select('usr_nombre, usr_apellidos');
        $this->db->from('usuarios');
        $this->db->where('usr_id', $id_user);
        $query = $this->db->get();
        return $query->row();
    }
    
}

?>
