<?php

class Agenda_model extends CI_Model{

    public function getLocalidades(){
        $this->db->select('*');
        $this->db->from('ciudad');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function insertarEvento($fecha, $inicio, $fin, $desc, $user, $entrevistado, $entrevistado_correo, $area, $creador, $actividad_terreno, $tipo_actividad){
        $data = array(
            'agen_fecha' => $fecha,
            'agen_hora_inicio' => $inicio,
            'agen_hora_fin' => $fin,
            'agen_contacto_area' => $entrevistado,
            'agen_descripcion' => $desc,
            'agen_usuario' => $user,
            'agen_area' => $area,
            'agen_contacto_correo' => isset($entrevistado_correo) ? $entrevistado_correo : 'NULL',
            'agen_creador' => $creador,
            'agen_actividad_terreno' => $actividad_terreno,
            'id_tipo_actividad' => $tipo_actividad
        );
        
        $this->db->insert('usuario_agenda', $data);
        
        if($this->db->affected_rows() > 0){
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function editarEvento($id, $fecha, $inicio, $fin, $desc, $user, $entrevistado, $entrevistado_correo, $area, $estado, $actividad_terreno, $tipo_actividad){
        $data = array(
            'agen_fecha' => $fecha,
            'agen_hora_inicio' => $inicio,
            'agen_hora_fin' => $fin,
            'agen_contacto_area' => $entrevistado,
            'agen_descripcion' => $desc,
            'agen_usuario' => $user,
            'agen_contacto_correo' => isset($entrevistado_correo) ? $entrevistado_correo : 'NULL',
            'agen_area' => $area,
            'agen_estado' => $estado,
            'agen_actividad_terreno' => $actividad_terreno,
            'id_tipo_actividad' => $tipo_actividad
        );
        
        $this->db->where('agen_id', $id);
        $this->db->update('usuario_agenda', $data);
        
        if($this->db->affected_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function verTodosEventos(){
        $this->db->select("agen_id, DATE_FORMAT(agen_fecha, '%d-%m-%Y') AS agen_fecha, DATE_FORMAT(agen_hora_inicio, '%H:%i') AS agen_hora_inicio,"
                . "DATE_FORMAT(agen_hora_fin, '%H:%i') AS agen_hora_fin, agen_estado, agen_descripcion, CONCAT(usr_nombre,' ', usr_apellidos) as usr_nombre, agen_contacto_area,"
                . " agen_contacto_correo, agen_comentarios, agen_creador", false);
        $this->db->from('usuario_agenda');
        $this->db->join('usuarios', 'agen_usuario = usr_id');
        $this->db->join('area', 'agen_area = area_id');
        $this->db->order_by('agen_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function verEventosEmpleado($user_id){
        $this->db->select('agen_id, agen_fecha, agen_hora_inicio, agen_hora_fin, agen_descripcion, agen_usuario, agen_contacto_area, agen_comentarios, agen_creador, agen_estado');
        $this->db->from('usuario_agenda, usuarios');
        $this->db->where('agen_usuario', $user_id);
        $this->db->where('agen_usuario = usr_id');
        $this->db->order_by('agen_fecha', 'desc');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function verEvento($id){
        $this->db->select("agen_id, DATE_FORMAT(agen_fecha, '%d-%m-%Y') AS agen_fecha, DATE_FORMAT(agen_hora_inicio, '%H:%i') AS agen_hora_inicio,"
                . "DATE_FORMAT(agen_hora_fin, '%H:%i') AS agen_hora_fin, agen_estado, agen_descripcion, CONCAT(usr_nombre,' ', usr_apellidos) as usr_nombre, agen_contacto_area,"
                . " agen_contacto_correo, agen_comentarios, agen_creador, agen_padre, area_nombre_area, agen_usuario, agen_area, agen_actividad_terreno, nombre_actividad", false);
        $this->db->from('usuario_agenda');
        $this->db->join('area','area_id = agen_area');
        $this->db->join('usuarios','usr_id = agen_usuario');
        $this->db->join('tipos_actividad','tipos_actividad.id_tipo_actividad = usuario_agenda.id_tipo_actividad');
        $this->db->where('agen_id', $id);
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function eliminarEvento($id){
        $this->db->delete('usuario_agenda', array('agen_id' => $id));
    }
    
    public function comentarioByID($id){
        $this->db->select('agen_comentarios');
        $this->db->from('usuario_agenda');
        $this->db->where('agen_id',$id);
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function getTiposActividad(){
        $this->db->select('*');
        $this->db->from('tipos_actividad');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getEntrevistados($id_agenda){
        $this->db->select('id_entrevistado, nombre, email');
        $this->db->from('entrevistados');
        $this->db->where('id_agenda',$id_agenda);
        $query = $this->db->get();
        return $query->result();
    }
    
}
/* End of file agenda_model.php */
/* Location: ./application/models/agenda_model.php */
