<?php
/**
 * Description of cargo_model
 *
 * @author nicolas
 */
class cargo_model extends CI_Model{
    
    public function view_all($table){
        $query = $this->db->get($table);
        
        return $query->result();
    }
    
    public function view($table, $id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('cargo_id', $id);
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function create($table, $nombre){
        $data = array(
            'cargo_nombre_cargo' => $nombre,
        );
        
        $this->db->insert($table, $data);
        
        if($this->db->affected_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function update($table, $id, $nombre){
        $data = array(
            'cargo_nombre_cargo' => $nombre,
        );
        
        $this->db->where('cargo_id',$id);
        $this->db->update($table, $data);
        
        if($this->db->affected_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function delete($table, $id){
        $this->db->delete($table, array('cargo_id' => $id));
    }
}

?>
