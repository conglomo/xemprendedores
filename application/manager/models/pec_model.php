<?php

/**
 * Description of pec_model
 *
 * @author Tavor
 */

class pec_model extends CI_Model {
    
    public function getAll(){
        $this->db->where('enabled', '1');
        $query = $this->db->get('documentos_pec');
        return $query->result();
    }
    
    public function create($nombre, $descripcion, $path){
        $data = array(
            "nombre" => $nombre,
            "descripcion" => $descripcion,
            "doc_path" => $path,
        );
        
        $this->db->insert('documentos_pec', $data);
        
        if ($this->db->affected_rows() > 0){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    public function delete($id){
        $data = array(
            'enabled' => '0',
        );
        $this->db->where('id_pec', $id);
        $this->db->update('documentos_pec', $data);
        if($this->db->affected_rows() > 0){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    public function getComments($id){
        $this->db->select("id_comentario, titulo, comentario, DATE_FORMAT(fecha, '%d-%m-%Y %H:%i') AS fecha", false);
        $this->db->from('comentarios_pec');
        $this->db->where('id_pec', $id);
        $this->db->where('enabled', '1');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getComment($id){
        $this->db->select("id_comentario, id_pec, titulo, comentario");
        $this->db->from('comentarios_pec');
        $this->db->where('id_comentario', $id);
        $this->db->where('enabled', '1');
        $query = $this->db->get();
        return $query->row();
    }
    
    public function addComment($id_pec, $titulo, $comentario){
        date_default_timezone_set('America/Santiago');
        $fecha = date("Y-m-d H:i:s");
        $data = array(
            "id_pec" => $id_pec,
            "titulo" => $titulo,
            "comentario" => $comentario,
            "fecha" => $fecha,
        );
        
        $this->db->insert('comentarios_pec', $data);
        
        if ($this->db->affected_rows() > 0){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    public function view($id){
        $this->db->select('*');
        $this->db->from('documentos_pec');
        $this->db->where('id_pec', $id);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function update($id, $nombre, $descripcion, $path){
        if ($path != null && !empty($path)){
            $data = array(
                'nombre' => $nombre,
                'descripcion' => $descripcion,
                "doc_path" => $path,
            );
        }
        else {
            $data = array(
                'nombre' => $nombre,
                'descripcion' => $descripcion,
            );
        }
        $this->db->where('id_pec', $id);
        $this->db->update('documentos_pec', $data);
        if($this->db->affected_rows() > 0){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    public function update_comment($id_comentario, $titulo, $comentario){
        date_default_timezone_set('America/Santiago');
        $fecha = date("Y-m-d H:i:s");
        $data = array(
            'titulo' => $titulo,
            'comentario' => $comentario,
            'fecha' => $fecha
        );
        $this->db->where('id_comentario', $id_comentario);
        $this->db->update('comentarios_pec', $data);
        if($this->db->affected_rows() > 0){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    public function delete_comment($id_comentario){
        $data = array(
            'enabled' => '0',
        );
        $this->db->where('id_comentario', $id_comentario);
        $this->db->update('comentarios_pec', $data);
        if($this->db->affected_rows() > 0){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    
    
}
