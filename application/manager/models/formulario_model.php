<?php

/**
 * Description of formulario_model
 *
 * @author nicolas
 */
class formulario_model extends CI_Model {

    public function view_all() {
        $this->db->select("DATE_FORMAT(rir_fecha_informe, '%d-%m-%Y %H:%i') AS rir_fecha_informe, rig_tipo_evento, CONCAT(usr_nombre, ' ', usr_apellidos) as usr_nombre, "
                . "DATE_FORMAT(rig_fecha_evento, '%d-%m-%Y') AS rig_fecha_evento, DATE_FORMAT(rig_hora_evento, '%H:%i') AS rig_hora_evento,"
                . "rip_nombre_completo, rir_id" , false);
        $this->db->from('repin___hecho_reporte');
        $this->db->join('repin_condiciones_entorno', 'repin_condiciones_entorno.rice_id = repin___hecho_reporte.rir_condiciones_entorno');
        $this->db->join('repin___especifico', 'repin___especifico.rie_id = repin___hecho_reporte.rir_especifico');
        $this->db->join('repin___general', 'repin___general.rig_id = repin___hecho_reporte.rir_general');
        $this->db->join('repin___personas', 'repin___personas.rip_id = repin___hecho_reporte.rir_personas');
        $this->db->join('usuarios', 'usuarios.usr_id = repin___hecho_reporte.rir_usuarios');
        $this->db->order_by('DATE(rir_fecha_informe)', 'desc');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function view_all_excel() {
        $this->db->select("rir_id, CONCAT(usr_nombre, ' ', usr_apellidos) as usr_nombre, DATE_FORMAT(rir_fecha_informe, '%d-%m-%Y %H:%i') AS rir_fecha_informe, "
                . "rig_tipo_evento, rig_tipo_incidente,  "
                . "DATE_FORMAT(rig_fecha_evento, '%d-%m-%Y') AS rig_fecha_evento, "
                . "DATE_FORMAT(rig_hora_evento, '%H:%i') AS rig_hora_evento, descripcion_incidente, nombre_centro_trabajo, nombre_area, "
                . "rig_lugar_especifico, rig_descripcion, rig_testigo_1, rig_testigo_2, rig_testigo_3, rip_nombre_completo, rip_edad, rip_sexo, "
                . "rip_bp, (SELECT nombre_razon_social FROM razones_sociales WHERE repin___personas.id_razon_social = razones_sociales.id_razon_social) as razon_social_persona, "
                . "(SELECT descripcion_lesion FROM lesiones_aparentes WHERE repin___personas.id_lesion_aparente = lesiones_aparentes.id_lesion_aparente) as lesion_persona, "
                . "(SELECT descripcion_parte FROM partes_lesionadas WHERE repin___personas.id_parte_lesionada = partes_lesionadas.id_parte_lesionada) as parte_lesionada_persona, "
                . "(SELECT nombre_area FROM areas_reporte WHERE repin___personas.id_area_reporte = areas_reporte.id_area_reporte) as area_persona,"
                . "rip_jefe_directo, rip_responsable_area, (SELECT descripcion_turno FROM turnos WHERE repin___personas.id_turno = turnos.id_turno) as turno_persona, "
                . "rip_otros, rie_proceso, rie_actividad_generica, "
                . "rie_tarea_especifica, rie_frecuencia_tarea, rie_estandar, rie_maquinarias, rie_equipos, rie_herramientas, rie_materiales, "
                . "rip_rut, (SELECT descripcion_condicion_climatica FROM condiciones_climaticas WHERE repin_condiciones_entorno.id_condicion_climatica = condiciones_climaticas.id_condicion_climatica) as descripcion_condicion_climatica, "
                . "(SELECT nombre_centro_trabajo FROM centros_trabajo WHERE repin_condiciones_entorno.id_centro_trabajo = centros_trabajo.id_centro_trabajo) as centro_trabajo_clima, rig_latitude, actos, condiciones, "
                . "rig_longitude, rig_direccion", false);
        $this->db->from('repin___hecho_reporte');
        $this->db->join('repin_condiciones_entorno', 'repin_condiciones_entorno.rice_id = repin___hecho_reporte.rir_condiciones_entorno');
        $this->db->join('repin___especifico', 'repin___especifico.rie_id = repin___hecho_reporte.rir_especifico');
        $this->db->join('repin___general', 'repin___general.rig_id = repin___hecho_reporte.rir_general');
        $this->db->join('repin___personas', 'repin___personas.rip_id = repin___hecho_reporte.rir_personas');
        $this->db->join('usuarios', 'usuarios.usr_id = repin___hecho_reporte.rir_usuarios');
        $this->db->join('tipos_incidente', 'repin___general.id_tipo_incidente = tipos_incidente.id_tipo_incidente');
        $this->db->join('centros_trabajo', 'repin___general.id_centro_trabajo = centros_trabajo.id_centro_trabajo');
        $this->db->join('areas_reporte', 'repin___general.id_area_reporte = areas_reporte.id_area_reporte');
        $query = $this->db->get();
        return $query->result();
    }

    public function view($id) {
        $this->db->select("rir_id, rig_tipo_evento, rig_tipo_incidente, DATE_FORMAT(rir_fecha_informe, '%d-%m-%Y %H:%i') AS rir_fecha_informe, "
                . "DATE_FORMAT(rig_fecha_evento, '%d-%m-%Y') AS rig_fecha_evento, "
                . "DATE_FORMAT(rig_hora_evento, '%H:%i') AS rig_hora_evento, descripcion_incidente, nombre_centro_trabajo, nombre_area, "
                . "rig_lugar_especifico, rig_descripcion, rig_testigo_1, rig_testigo_2, rig_testigo_3, rie_proceso, rie_actividad_generica,  "
                . "rie_tarea_especifica, rie_frecuencia_tarea, rie_estandar, rie_maquinarias, rie_equipos, rie_herramientas, rie_materiales, "
                . "CONCAT(usr_nombre, ' ', usr_apellidos) as usr_nombre, rip_rut, rip_nombre_completo, rip_edad, rip_sexo, "
                . "rip_bp, (SELECT nombre_razon_social FROM razones_sociales WHERE repin___personas.id_razon_social = razones_sociales.id_razon_social) as razon_social_persona, "
                . "(SELECT descripcion_lesion FROM lesiones_aparentes WHERE repin___personas.id_lesion_aparente = lesiones_aparentes.id_lesion_aparente) as lesion_persona, "
                . "(SELECT descripcion_parte FROM partes_lesionadas WHERE repin___personas.id_parte_lesionada = partes_lesionadas.id_parte_lesionada) as parte_lesionada_persona, "
                . "(SELECT nombre_area FROM areas_reporte WHERE repin___personas.id_area_reporte = areas_reporte.id_area_reporte) as area_persona,"
                . "rip_jefe_directo, rip_responsable_area, (SELECT descripcion_turno FROM turnos WHERE repin___personas.id_turno = turnos.id_turno) as turno_persona, "
                . "rip_otros, (SELECT descripcion_condicion_climatica FROM condiciones_climaticas WHERE repin_condiciones_entorno.id_condicion_climatica = condiciones_climaticas.id_condicion_climatica) as descripcion_condicion_climatica, "
                . "(SELECT nombre_centro_trabajo FROM centros_trabajo WHERE repin_condiciones_entorno.id_centro_trabajo = centros_trabajo.id_centro_trabajo) as centro_trabajo_clima, rig_latitude, actos, condiciones, "
                . "rig_longitude, rig_direccion", false);
        $this->db->from('repin___hecho_reporte');
        $this->db->join('repin_condiciones_entorno', 'repin_condiciones_entorno.rice_id = repin___hecho_reporte.rir_condiciones_entorno');
        $this->db->join('repin___especifico', 'repin___especifico.rie_id = repin___hecho_reporte.rir_especifico');
        $this->db->join('repin___general', 'repin___general.rig_id = repin___hecho_reporte.rir_general');
        $this->db->join('repin___personas', 'repin___personas.rip_id = repin___hecho_reporte.rir_personas');
        $this->db->join('usuarios', 'usuarios.usr_id = repin___hecho_reporte.rir_usuarios');
        $this->db->join('tipos_incidente', 'repin___general.id_tipo_incidente = tipos_incidente.id_tipo_incidente');
        $this->db->join('centros_trabajo', 'repin___general.id_centro_trabajo = centros_trabajo.id_centro_trabajo');
        $this->db->join('areas_reporte', 'repin___general.id_area_reporte = areas_reporte.id_area_reporte');
        $this->db->where('rir_id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    public function getFiles($id){
        $this->db->select('multi_id, multi_tipo, multi_path');
        $this->db->from('repin___multimedia');
        $this->db->where('multi_hecho_reporte', $id);
        $query = $this->db->get();
        return $query->result();
    }

}

/* End of file formulario_model.php */
/* Location: ./application/controllers/formulario_model.php */