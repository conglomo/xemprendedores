$(document).on('ready', function(){
    $('.optionDonation').on('click', function(){
        if($(this).prop('checked')) {
            $(this).parent().parent().find('.inputInfo').fadeIn(300);
        } else {
            $(this).parent().parent().find('.inputInfo').fadeOut(300);
        }
    }); 


    $('#infoUser #donateForm').on('click', function(){
        var monto = $('#monto').val();
        var materiales = $('#materiales').val();
        var horas = $('#horas').val();
        var profesion = $('#profesion').val();
        var idea = $('#idea').val();

        var nombres = $('#nombres').val();
        var apellidos = $('#apellidos').val();
        var rut = $('#rut').val();
        var email = $('#email').val();
        var telefono = $('#telefono').val();

        if(nombres == "" || apellidos == "" || rut == "" || email == "" || telefono == ""){
            alert('Debe completar todos los campos');

        } else {
            $.ajax({ 
                url:'http://xemprendedores.org/index.php/main/crear_mensaje',
                type : 'POST',
                format : 'json',
                data: {
                    'monto': monto,
                    'materiales': materiales,
                    'horas': horas,
                    'profesion': profesion,
                    'idea': idea,
                    'nombres': nombres,
                    'apellidos': apellidos,
                    'rut': rut,
                    'email': email,
                    'telefono': telefono
                },
                success: function(response){    
                    alert('La informacion ha sido recibida, nos pondremos en contacto a la brevedad.');
                    $('#infoUser').modal('hide');
                },
                error: function(xhr, ajaxOptions, thrownError){ 
                    if(ajaxOptions === "timeout") {
                        alert('Hubo un problema en la conexion. Por favor intente de nuevo.');

                    } else {
                        if(thrownError === "") {
                            alert('Hay un problema en la conexion. Por favor revise si esta conectado a internet.');

                        } else {
                            alert(thrownError);
                        }
                    }
                } 
            });
        }
    });
});